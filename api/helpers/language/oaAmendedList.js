exports.oa_amend_list = {
	'Specification': [
		[ {text: 'Amendments to the Specification', decoration: {bold: true, underline: 'single'} } ],
		[ {text: 'Please delete paragraph [0001], and insert the following amended paragraph:'} ],
		[ {text: '[0001]'}, {text: 'Text ', tab: true}, {text: 'text', decoration: {bold: true, underline: 'single'}}, {text: ' '}, {text: 'text', decoration: {bold: true, strike: true} } ],
		[ {text: 'Please insert the following paragraph after paragraph [0001]:'} ],
		[ {text: '[0001a]'}, {text: 'Text text text', tab: true} ],

	],
	'Drawings': [
		[ {text: 'Amendments to the Drawings', decoration: {bold: true, underline: 'single'} } ],
		[ {text: 'The drawing sheet for FIG. __ has been amended such that ____. No other changes are made herein to FIG. __. No new matter has been added.'} ],
		[ {text: 'Appendix:', decoration: {bold: true} } ],
		[ {text: 'Annotated Sheet Showing Changes'} ],
		[ {text: 'Replacement Sheet'} ],
	],
	'Claims': [
		[ {text: 'Amendments to the Claims', decoration: {bold: true, underline: 'single'} } ],
	],
}
