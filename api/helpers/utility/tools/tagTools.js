// return true if the component has a tag with tag_name
// else return false
exports.has = function (component, tag_name, tag_value) {
    return component.component_tags.some(tag =>
        tag.tag_name === tag_name &&
        (typeof tag_value !== 'undefined' ? tag[tag_name] === tag_value : true)
    );
};

// return tag_description if component has a tag with tag_name
// else return undefined
exports.description = function (component, tag_name) {
    var desc;
    component.component_tags.some((tag) => {
        if (tag.tag_name === tag_name) {
            desc = tag.tag_description;
            return true;
        } else {
            return false;
        }
    });
    return desc;
};

exports.findComponent = function (components, tag_name) {
    var cmp;
    (components || []).map((c) => {
        (c.component_tags || []).some((t) => {
            if (t.tag_name === tag_name) {
                cmp = c;
                return true;
            } else {
                if (!cmp && c.tchildren && c.tchildren.length > 0) {
                    cmp = exports.findComponent(c.tchildren, tag_name);
                    return typeof cmp !== 'undefined';
                } else {
                    return false;
                }
            }
        });
    });
    return cmp;
};

exports.getIndependentClaimComponents = function (device) {
    return device.components.filter(cmp => 
        cmp.component_tags.some(tag => 
            tag.tag_name === 'feature' && typeof tag.feature_claim_type !== 'undefined' && 
                tag.feature_claim_type === true
        )
    );
};
