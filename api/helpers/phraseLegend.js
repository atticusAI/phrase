///////////////////////////////// Imports //////////////////////////////////////

//// Keep this file concise.
// Any logic should be moved to another file and imported here

const CLAIM_BUILDER = require("./utility/claimBuilder.js");
const REF_BUILDER = require("./utility/referenceBuilder.js");
const REJ_BUILDER = require("./utility/rejectionBuilder.js");
const CLAIM_GEN = require("./utility/claimGenerators.js");
const FIG_DESCRIPTION_BUILDER = require("./utility/descriptionBuilder.js");
const S_TOOLS = require("./utility/tools/stringTools.js");
const CONFIG = require('./config');

// const DATA_BREACH_BUILDER = require("./utility/dataBreachBuilder.js");

const FIGURE_BUILDER = require("./utility/figureBuilder.js");

///////////////////////////////// Router ///////////////////////////////////////

//// This list tells Atticus what function exist and what type of data to send it.
// ex: the function CLAIM_FORMAT will be passed a PatentOfficeAction object
// The full list of possible data types can be found in SCHEMA.js

exports.list = [

  // DOCX
  ["HELLO", "()"],
  ["VISIOTEST", "()"],
  ["APP_CLAIM_FORMAT", "[Numbered CurClaim]"],
  ["APP_CLAIM_SUMMARY", "[Numbered CurClaim]"],
  ["CLAIM_SUMMARY_FOR_SPEC", "[Numbered CurClaim]"],
  ["APP_CLAIMS_ONLY", "[Numbered CurClaim]"],
  ["CORRESPONDENCE_TABLE", "[Numbered CurClaim]"],
  ["CLAIM_FORMAT", "[Numbered CurClaim]"],
  ["MICRON_MPF", "[Numbered CurClaim]"],
  ["CLAIM_SUMMARY", "PatentOfficeAction"],
  ["FORMAT_PARAGRAPHS", "Text"],
  ["RESTRICTION_REQUIREMENT", "PatentOfficeAction"],
  ["ARG_103_IND", "Task"],
  ["ARG_103_DEP", "Task"],
  ["ARG_102", "Task"],

  // VISIO Descriptions
  ["BACKGROUND_DESCRIPTION", "PatentApplication"],
  ["NARRATIVE_DESCRIPTION", "Text"],
  ["NARRATIVE_DESCRIPTION_STD", "Text"],

  // ["FLOWCHART_DESCRIPTION", "(Text, Text, [(Feature :<>: FeatureData) :<>: Text], [(Text, Text)])"],
  ["FLOWCHART_DESCRIPTION", "((Text, Text), Text, [(Feature :<>: FeatureData) :<>: Text], [(Text, Text)])"],
  ["FLOWCHART_DESCRIPTION_MPF", "((Text, Text), Text, [Claim ((Feature :<>: FeatureData) :<>: Text)], [(Text, Text)])"],
  ["BLOCK_MA_DESCRIPTION", "([(Text, LabeledDeviceTemplate)], [(Text, Text)])"],
  ["BLOCK_MB_DESCRIPTION", "([(Text, LabeledDeviceTemplate)], [(Text, Text)])"],
  ["BLOCK_MC_DESCRIPTION", "([(Text, LabeledDeviceTemplate)], [(Text, Text)])"],
  ["BLOCK_HA_DESCRIPTION", "([(Text, LabeledDeviceTemplate)], [(Text, Text)])"],
  ["TECH_PARAGRAPHS", "[Technology]"],
  ["DYNAMIC_FIGURE_DESCRIPTION", "LabeledDeviceTemplate"],

  // VISIO
  // ["FC_FIG",        "(Text, Text, [(Feature :<>: FeatureData) :<>: Text], [(Text, Text)])"],
  ["FC_FIG",        "((Text, Text), Text, [(Feature :<>: FeatureData) :<>: Text], [(Text, Text)])"],
  ["NARRATIVE_FIG", "Text"],
  ["NARR_PHOLDER",  "Text"],
  ["BLK_MA", "(LabeledDeviceTemplate, [(Text, Text)])"],
  ["BLK_MB", "(LabeledDeviceTemplate, [(Text, Text)])"],
  ["BLK_MC", "(LabeledDeviceTemplate, [(Text, Text)])"],
  ["BLK_HA", "(LabeledDeviceTemplate, [(Text, Text)])"],
  ["JBLK_A", "([(Text, LabeledDeviceTemplate)], [(Text, Text)])"],
  ["JBLK_B", "([(Text, LabeledDeviceTemplate)], [(Text, Text)])"],
  ["JBLK_C", "([(Text, LabeledDeviceTemplate)], [(Text, Text)])"],
  ["JBLK_H", "([(Text, LabeledDeviceTemplate)], [(Text, Text)])"],
  ["DYNAMIC_FIGURE", "LabeledDeviceTemplate"],

  // Brief Descriptions
  ["FIG1_BR_DES",       "Text"],
  ["NARRATIVE_BR_DES",  "Text"],
  ["FLOWCHART_BR_DES",  "[(Text, Text)]"],
  ["BLOCK_ABCH_BR_DES", "(Text, [(Text, Text)]"],
  ["BLOCK_BCH_BR_DES",  "(Text, [(Text, Text)]"],
  ["BLOCK_CH_BR_DES",   "(Text, [(Text, Text)]"],
  ["BLOCK_C_BR_DES",   "(Text, [(Text, Text)]"],
  ["DYNAMIC_FIGURE_BR_DES", "LabeledDeviceTemplate"]

  // Data Breach
  /*
  ["BREACH_BY_STATE",  "[(DataBreachInfo, Text, Text, SubstitutionReq)]"],
  ["BREACH_SUMMARY_TABLE",  "[DataBreachInfo]"],
  ["TIMELINE", "[NotificationReq]"],
  ["BREACH_MAP", "Text"],
  ["LIST_TO_PARS", "[Text]"]
  */
]

/////////////////////////////// Endpoints //////////////////////////////////////
// GET method to return version
exports.GET_VERSION = function(data, send){
  send(CONFIG.app.version);
};


//// Each exported function will be avalible as a document template variable
// The functions name will determine the template varible's name
// Atticus will replace each template varible with whatever the function returns
// ex: if the template contains "{HELLO}" it will be replaced by the text "Success"

// used as an example and for connection testing
exports.HELLO = function(data, send){
  send("Success");
};


// returns a paragraph
exports.APP_CLAIM_FORMAT = function(claim_pair, send){
  send(CLAIM_BUILDER.appClaimFormat(claim_pair, true));
};

// returns a paragraph
exports.APP_CLAIM_SUMMARY = function(claim_pair, send){
  send(CLAIM_BUILDER.appClaimSummary(claim_pair, false, ['method', 'app', 'mpf', 'crm']));
};

exports.CLAIM_SUMMARY_FOR_SPEC = function(claim_pair, send){
  send(CLAIM_BUILDER.appClaimSummary(claim_pair, true, ['method', 'app', 'mpf', 'crm']));
};

exports.MICRON_MPF = function(claim_pair, send){
  send(CLAIM_BUILDER.appClaimSummary(claim_pair, true, ['mpf']));
};

// returns application claims (not summarized)
exports.APP_CLAIMS_ONLY = function(claim_pair, send){
  send(CLAIM_BUILDER.appClaimFormat(claim_pair, false));
}

// send(a Claim Correspondence Table)
exports.CORRESPONDENCE_TABLE = function(claims, send){
  send(CLAIM_BUILDER.corrTable(claims));
}

// returns a paragraph
exports.CLAIM_FORMAT = function(claim_pair, send){
  send(CLAIM_BUILDER.oaClaimFormat(claim_pair));
};

// returns a runs
exports.CLAIM_SUMMARY = function(patent_office_action, send){
  send(CLAIM_BUILDER.oaClaimSummary(patent_office_action));
};

// returns restriction requirement text
exports.RESTRICTION_REQUIREMENT = function(patent_office_action, send){
  send(CLAIM_BUILDER.restrictionClaimSummary(patent_office_action));
};

// returns a paragraph
exports.ARG_102 = function(task, send){
  send(REJ_BUILDER.rejectionMarkup(task, '102'));
}

// returns a paragraph
exports.ARG_103 = function(task, send){
  send(REJ_BUILDER.rejectionMarkup(task, '103'));
}

// returns a paragraph
exports.ARG_103_IND = function(task, send){
  send(REJ_BUILDER.rejectionMarkup(task, '103'));
}

// returns a paragraph
exports.ARG_103_DEP = function(task, send){
  send(REJ_BUILDER.rejectionMarkup(task, '103'));
}

// Returns printout of technology text
exports.TECH_PARAGRAPHS = function(techs, send){
  send(FIG_DESCRIPTION_BUILDER.writeTechnologies(techs));
}

// Take raw text and format it into paragraphs
exports.FORMAT_PARAGRAPHS = function(text, send){
  send(FIG_DESCRIPTION_BUILDER.formatParagraphs(text));
}

/////////////////////////// Figure Desciptions /////////////////////////////////

exports.NARR_PHOLDER = function(fig_hash, send){
  console.log("Labelling Narrative Figure")
  send("");
}

exports.BACKGROUND_DESCRIPTION = function(patent_application, send){
  send(FIG_DESCRIPTION_BUILDER.backgroundDescription(patent_application));
}

exports.NARRATIVE_DESCRIPTION = function(fig_label, send, client){
  send(FIG_DESCRIPTION_BUILDER.narrativeDescription(fig_label, client));
}

exports.NARRATIVE_DESCRIPTION_STD = function(fig_label, send, client){
  send(FIG_DESCRIPTION_BUILDER.narrativeDescriptionSTD(fig_label, client));
}

exports.FLOWCHART_DESCRIPTION = function(flow_obj, send, client){
  send(FIG_DESCRIPTION_BUILDER.flowDescription(flow_obj, client));
}

exports.FLOWCHART_DESCRIPTION_MPF = function(flow_obj, send, client){
  send(FIG_DESCRIPTION_BUILDER.flowDescMPF(flow_obj, client));
}

exports.BLOCK_MA_DESCRIPTION = function(device_obj, send, client){
  send(FIG_DESCRIPTION_BUILDER.blockDescription("MA", device_obj[0], device_obj[1], "{TITLE}", client));
};

exports.BLOCK_MB_DESCRIPTION = function(device_obj, send, client){
  send(FIG_DESCRIPTION_BUILDER.blockDescription("MB", device_obj[0], device_obj[1], "{TITLE}", client));
};

exports.BLOCK_MC_DESCRIPTION = function(device_obj, send, client){
  send(FIG_DESCRIPTION_BUILDER.blockDescription("MC", device_obj[0], device_obj[1], "{TITLE}", client));
};

exports.BLOCK_HA_DESCRIPTION = function(device_obj, send, client){
  send(FIG_DESCRIPTION_BUILDER.blockDescription("HA", device_obj[0], device_obj[1], "{TITLE}", client));
};

exports.DYNAMIC_FIGURE_DESCRIPTION = function(device_obj, send) {
  send(FIG_DESCRIPTION_BUILDER.dynamicFigureDescription(device_obj));
};

//////////////////////////// BRIEF DESCRIPTIONS ////////////////////////////////

exports.FIG1_BR_DES = function(obj, send, client){
  send(FIG_DESCRIPTION_BUILDER.figure1BriefDescription(obj, client));
}

exports.NARRATIVE_BR_DES = function(obj, send, client){
  send(FIG_DESCRIPTION_BUILDER.narrativeBriefDescription(obj, client));
}

exports.FLOWCHART_BR_DES = function(obj, send, client){
  send(FIG_DESCRIPTION_BUILDER.flowBriefDescription(obj, client));
}

exports.BLOCK_ABCH_BR_DES = function(obj, send){
  send(FIG_DESCRIPTION_BUILDER.abchBriefDescription(obj, use_a=true, use_b=true, use_h=true));
}

exports.BLOCK_BCH_BR_DES = function(obj, send){
  send(FIG_DESCRIPTION_BUILDER.abchBriefDescription(obj, use_a=false, use_b=true, use_h=true));
}

exports.BLOCK_CH_BR_DES = function(obj, send){
  send(FIG_DESCRIPTION_BUILDER.abchBriefDescription(obj, use_a=false, use_b=false, use_h=true));
}

exports.BLOCK_C_BR_DES = function(obj, send){
  send(FIG_DESCRIPTION_BUILDER.abchBriefDescription(obj, use_a=false, use_b=false, use_h=false));
}

exports.DYNAMIC_FIGURE_BR_DES = function(device_obj, send) { 
  send(FIG_DESCRIPTION_BUILDER.dynamicFigureBriefDescription(device_obj));
};


///////////////////////////////// VISIO ////////////////////////////////////////

//// These generate vsdx specific xml

// returns a vsdx page
exports.NARRATIVE_FIG = function(fig_hash, send){
  send(FIGURE_BUILDER.getNarrativeFigure(fig_hash));
}

exports.BLK_MA = function(labeled_device, send, client){
  send(FIGURE_BUILDER.getBlockFigures(labeled_device, "MA", client));
}

exports.BLK_MB = function(labeled_device, send, client){
  send(FIGURE_BUILDER.getBlockFigures(labeled_device, "MB", client));
}

exports.BLK_MC = function(labeled_device, send, client){
  send(FIGURE_BUILDER.getBlockFigures(labeled_device, "MC", client));
}

exports.BLK_HA = function(labeled_device, send, client){
  send(FIGURE_BUILDER.getBlockFigures(labeled_device, "HA", client));
}

exports.FC_FIG = function(features, send){
  send(FIGURE_BUILDER.getFlowFigures(features.slice(1,3)));
}

exports.JBLK_A = function(labeled_devs, send){
  send(FIGURE_BUILDER.getBlockFigures([labeled_devs[0][0][1], labeled_devs[1]], "MA"));
}

exports.JBLK_B = function(labeled_devs, send){
  send(FIGURE_BUILDER.getBlockFigures([labeled_devs[0][1][1], labeled_devs[1]], "MB"));
}

exports.JBLK_C = function(labeled_devs, send){
  send(FIGURE_BUILDER.getBlockFigures([labeled_devs[0][2][1], labeled_devs[1]], "MC"));
}

exports.JBLK_H = function(labeled_devs, send){
  send(FIGURE_BUILDER.getBlockFigures([labeled_devs[0][3][1], labeled_devs[1]], "HA"));
}

exports.DYNAMIC_FIGURE = function(labeled_devs, send) { 
  send(FIGURE_BUILDER.getDynamicFigure(labeled_devs));
};

///////////////////////////// Atticus Portal //////////////////////////////////

//// These methods are called directly from the atticus portal.
// Also they should not be included in the list of function sent to atticus rest.

exports.GENERATE_CLAIMS = function(method_pack, send){
  // console.log(JSON.stringify(method_pack))
  // console.log('------------------')
  send(CLAIM_GEN.generateClaimCollection(method_pack.claims, method_pack.types, method_pack.preamble));
}
///////////////////////////// Data Breach Tool //////////////////////////////////
/*
exports.BREACH_SUMMARY_TABLE = function(breach_infos, send) {
  console.log(JSON.stringify(breach_infos))
  console.log('------------------')
  send(DATA_BREACH_BUILDER.breachSummaryTable(breach_infos));
}
exports.BREACH_MAP = function(svg_buffer, send){
  DATA_BREACH_BUILDER.generateStateMap(svg_buffer, send);
}
exports.BREACH_BY_STATE = function(breach_infos,send){
  console.log(JSON.stringify(breach_infos))
  console.log('------------------')
  send(DATA_BREACH_BUILDER.breachByState(breach_infos));
}

exports.TIMELINE = function(notifications, send){
  console.log(JSON.stringify(notifications))
  console.log('------------------')
  send(DATA_BREACH_BUILDER.timeline(notifications));
}
exports.LIST_TO_PARS = function (texts, send) {
  console.log(JSON.stringify(texts))
  console.log('------------------')
  send(DATA_BREACH_BUILDER.listToPars(texts));
}
*/
