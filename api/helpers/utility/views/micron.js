const  W_TOOLS = require('../tools/wordTools');

// for use by W_TOOLS.ParagraphBuilder
// each function must return an array
// the this context will always be a device
// each element in that array must be a string or an object based on W_TOOLS.WordObject
// strings will be implemented as W_TOOLS.Text objects

exports.dynamic_figure_description = function () {
    const wt = new W_TOOLS.WordTools();
    return [
        'FIG. ', new wt.RefSource(this.text, 'Figure'),
        ' shows a block diagram ', new wt.RefTarget(this.text, 'Figure'), 
        this.text + ' of a {PRIMARY_MODULE} ', new wt.ComponentRefTarget(this, 'primary'), 
        ' that supports {TITLE.LCASE} in accordance with aspects' +
        ' of the present disclosure. The {PRIMARY_MODULE} ', new wt.RefTarget(this.text, 'Figure'), 
        this.text + ' may be an example of aspects of an external memory controller 105, a device memory controller 155, a' + 
        ' local memory controller 165, a local memory controller 265, or a combination thereof as described herein.' + 
        ' The  {PRIMARY_MODULE} ', new wt.RefTarget(this.text, 'Figure'), 
        this.text + ' may include ', new wt.ComponentList(this),
        ' Each of these modules may communicate, directly or indirectly, with one another.'
    ];
};

exports.dynamic_figure_brief_description = function () {
    const wt = new W_TOOLS.WordTools();
    return [
        'FIG. ', new wt.RefTarget(this.text, 'Figure'), 
        ' is a block diagram of a {PRIMARY_MODULE}' +
        ' that supports {TITLE.LCASE} in accordance with aspects' + 
        ' of the present disclosure.'
    ];
};
