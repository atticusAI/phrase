'use strict';

var util = require('util');
var legend = require('../helpers/phraseLegend');

module.exports = {
  HELLO: hello, 
  VERSION: version, 
  "/": keywords
};

function hello(req, res) {
  console.log('HELLO');
  res.json("HELLO");
}

function version(req, res) {
  res.json("0.0.1");
}

function keywords(req, res) {
  res.json(legend.list);
}
