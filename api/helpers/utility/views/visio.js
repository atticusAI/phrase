exports.Page = function() {
    return {
        "@xmlns": "http://schemas.microsoft.com/office/visio/2012/main",
        "@xmlns:r": "http://schemas.openxmlformats.org/officeDocument/2006/relationships",
        "@xml:space": "preserve"
    };
};

exports.Label = function() {
    return { 
        "@Type": "Shape",
        "@Name": this.name,
        "@NameU": this.name,
        "@ID": this.id,
        "Cell": [{ "@N": "PinX", "@V": this.x },
            { "@N": "PinY", "@V": this.y },
            { "@N": "Width", "@V": this.w },
            { "@N": "Height", "@V": this.h },
            { "@N": "LocPinX", "@V": this.halfWidth, "@F": "Width*0.5" }, 
            { "@N": "LocPinY", "@V": this.halfHeight, "@F": "Height*0.5" }, 
            { "@N": "LinePattern", "@V": 0}, // no boxes around labels
            { "@N": "FillPattern", "@V": 0} // no background for labels
            ],
        "Section": [
            { "@IX": 0, 
                "@N": "Field", 
                "Row": {
                    "@IX": 0,
                    "Cell": [
                        {"@N": "Value", "@V": 0, "@F": "PAGENUMBER()"}, 
                        {"@N": "Format", "@V": "esc(0)", "@U": "STR", "@F": "FIELDPICTURE(0)" }
                    ]
                }
            }, 
            { "@IX": 1,
                "@N": "Geometry",
                "Row": [{ "@T": "RelMoveTo", "@IX": 1,
                        "Cell": [{ "@N": "X", "@V": 0}, { "@N": "Y", "@V": 0}] },
                    { "@T": "RelLineTo", "@IX": 2,
                        "Cell": [{ "@N": "X", "@V": 0}, { "@N": "Y", "@V": 1}] },
                    { "@T": "RelLineTo", "@IX": 3,
                        "Cell": [{ "@N": "X", "@V": 1}, { "@N": "Y", "@V": 1}] },
                    { "@T": "RelLineTo", "@IX": 4,
                        "Cell": [{ "@N": "X", "@V": 1}, { "@N": "Y", "@V": 0}] },
                    { "@T": "RelLineTo", "@IX": 5,
                        "Cell": [{ "@N": "X", "@V": 0}, { "@N": "Y", "@V": 0}] }
                ]
            }
        ], 
        "Text": {
            "cp": {"@IX": "0"}, 
            "fld": {"@IX": "0", "#text": "#"}, 
            "#text": this.text
        } 
    };
};

exports.FigureLabel = function () {
    return {
        "@ID": this.id, 
        "@Type": "Shape", 
        "@Name": this.name, 
        "Cell": [
            {"@N": "Width", "@V": this.width}, // 0.95}, 
            {"@N": "Height", "@V": this.height}, // 0.25}, 
            {"@N": "PinX", "@V": this.x},
            {"@N": "PinY", "@V": this.y}
        ], 
        "Section": [{
            "@N": "Field", 
            "Row": {
                "@IX": 0, 
                "Cell": [
                    {"@N": "Value", "@V": 0, "@F": "PAGENUMBER()"}, 
                    {"@N": "Format", "@V": "esc(0)", "@U": "STR", "@F": "FIELDPICTURE(0)"}
                ]
            }
        }, 
        {
            "@N": "Character", 
            "Row": {
                "@IX": 0,
                "Cell": [
                    {"@N": "Size", "@V": this.charSize, "@U": "PT"}
                ]
            }
        }], 
        "Text": {
            "cp": {"@IX": 0}, 
            "#text": "FIG. ",
            "fld":{"@IX": 0, "#text": "#"}
        }
    };
};

exports.Group = function() {
    return {
        "@Type": "Group",
        "@LineStyle": "3",
        "@FillStyle": "3",
        "@TextStyle": "3",
        "@ID": this.id,
        "Cell": [{ "@N": "PinX", "@V": this.x },
            { "@N": "PinY", "@V": this.y },
            { "@N": "Width", "@V": this.w },
            { "@N": "Height", "@V": this.h },
            { "@N": "LocPinX", "@V": 0},  // , "@F": "Width*0.5" },
            { "@N": "LocPinY", "@V": 0}, // , "@F": "Height*0.5" },
            { "@N": "QuickStyleVariation", "@V": "2" }
        ]
    };
};

exports.Rectangle = function () {
    return { 
        "Text": this.text,
        "@Type": "Shape",
        "@Name": this.name,
        "@NameU": this.name,
        "@ID": this.id,
        "Cell": [{ "@N": "PinX", "@V": this.x },
            { "@N": "PinY", "@V": this.y },
            { "@N": "Width", "@V": this.w },
            { "@N": "Height", "@V": this.h },
            { "@N": "LocPinX", "@V": this.halfWidth, "@F": "Width*0.5" }, 
            { "@N": "LocPinY", "@V": this.halfHeight, "@F": "Height*0.5" }, 
            { "@N": "LinePattern", "@V": this.linePattern},
            { "@N": "VerticalAlign", "@V": this.verticalAlign}],
        "Section": [
            { "@IX": 0,
                "@N": "Geometry",
                "Row": [{ "@T": "RelMoveTo", "@IX": 1,
                        "Cell": [{ "@N": "X", "@V": 0}, { "@N": "Y", "@V": 0}] },
                    { "@T": "RelLineTo", "@IX": 2,
                        "Cell": [{ "@N": "X", "@V": 0}, { "@N": "Y", "@V": 1}] },
                    { "@T": "RelLineTo", "@IX": 3,
                        "Cell": [{ "@N": "X", "@V": 1}, { "@N": "Y", "@V": 1}] },
                    { "@T": "RelLineTo", "@IX": 4,
                        "Cell": [{ "@N": "X", "@V": 1}, { "@N": "Y", "@V": 0}] },
                    { "@T": "RelLineTo", "@IX": 5,
                        "Cell": [{ "@N": "X", "@V": 0}, { "@N": "Y", "@V": 0}] }
                ]
            }
        ]
    };
};

exports.LabelConnector = function () {
    return {
        "@ID": this.id, 
        "@Type": "Shape", 
        "@LineStyle": "3", 
        "@FillStyle": "3", 
        "@TextStyle": "3",
        "Cell": [
            {"@N": "PinX", "@V": 0.72, "@F": "(BeginX+EndX)/2"}, 
            {"@N": "PinY", "@V": 0.32, "@F": "(BeginY+EndY)/2"}, 
            {"@N": "Width", "@V": 0.16, "@F": "SQRT((EndX-BeginX)^2+(EndY-BeginY)^2)"},
            {"@N": "Height", "@V": 0.033}, 
            {"@N": "LocPinX", "@V": 0.08, "@F": "Width*0.5"}, 
            {"@N": "LocPinY", "@V": 0, "@F": "Height*0"}, 
            {"@N": "Angle", "@V": -1.6, "@F": "ATAN2(EndY-BeginY,EndX-BeginX)"}, 
            {"@N": "BeginX", "@V": 0.82, "@F": "Sheet." + this.parentId + "!Width*0.48"},
            {"@N": "BeginY", "@V": 0.37, "@F": "Sheet." + this.parentId + "!Height*0.21"},
            {"@N": "EndX", "@V": 0.72, "@F": "Sheet." + this.parentId + "!Width*0.48"},
            {"@N": "EndY", "@V": 0.106, "@F": "Sheet." + this.parentId + "!Height*0.08"}, 
            {"@N": "QuickStyleLineMatrix", "@V": 1},
            {"@N": "QuickStyleFillMatrix", "@V": 1},
            {"@N": "QuickStyleEffectsMatrix", "@V": 1},
            {"@N": "QuickStyleFontMatrix", "@V": 1}
        ], 
        "Section": {"@N": "Geometry", "@IX": 0,
            "Cell": [
                {"@N": "NoFill", "@V": 1},
                {"@N": "NoLine", "@V": 0},
                {"@N": "NoShow", "@V": 0},
                {"@N": "NoSnap", "@V": 0},
                {"@N": "NoQuickDrag", "@V": 0}
            ], 
            "Row": [
                {"@T": "MoveTo", "@IX": 1, "Cell": [
                    {"@N": "X", "@V": 0, "@F": "Width*0"}, 
                    {"@N": "Y", "@V": 0, "@F": "Height*0"}
                ]}, 
                {"@T": "NURBSTo", "@IX": 2, "Cell": [
                    {"@N": "X", "@V": 0.16, "@F": "Width*1"},
                    {"@N": "Y", "@V": 0, "@F": "Height*0"}, 
                    {"@N": "A", "@V": 0}, 
                    {"@N": "B", "@V": 1}, 
                    {"@N": "C", "@V": 0}, 
                    {"@N": "D", "@V": 1}, 
                    {"@N": "E", "@V": "NURBS(0.45, 3, 0, 0, 0.21,1.53,0,1, 0.84,1.14,0,1)",
                        "@U": "NURBS", "@F": "NURBS(0.45, 3, 0, 0, 0.21,1.53,0,1, 0.84,1.14,0,1)"}
                ]}
            ]
        }
    };
};

exports.DeviceArrow = function() {
    return {
        "@ID": this.id, 
        "Cell": [
            {"@N": "PinX", "@V": 0.3125, "@F": "(BeginX+EndX)/2"},
            {"@N": "PinY", "@V": 0.6875, "@F": "(BeginY+EndY)/2"}, 
            {"@N": "Width", "@V": 0.625, "@F": "SQRT((EndX-BeginX)^2+(EndY-BeginY)^2)"}, 
            {"@N": "Height", "@V": 0.124}, 
            {"@N": "LocPinX", "@V": 0.3125, "@F": "Width*0.5"}, 
            {"@N": "LocPinY", "@V": 0.124, "@F": "Height*1"}, 
            {"@N": "Angle", "@V": -0.643, "@F": "ATAN2(EndY-BeginY,EndX-BeginX)"}, 
            {"@N": "FlipX", "@V": 0},
            {"@N": "FlipY", "@V": 0},
            {"@N": "ResizeMode", "@V": 0},
            {"@N": "BeginX", "@V": 0.625, "@F": "Sheet." + this.parentId + "!Width*0.041"}, 
            {"@N": "BeginY", "@V": 0.875, "@F": "Sheet." + this.parentId + "!Height*0.875"}, 
            {"@N": "EndX", "@V": 0.5625, "@F": "Sheet." + this.parentId + "!Width*0.375"}, 
            {"@N": "EndY", "@V": 0.5, "@F": "Sheet." + this.parentId + "!Height*0.5"}, 
            {"@N": "QuickStyleLineMatrix", "@V": 1}, 
            {"@N": "QuickStyleFillMatrix", "@V": 1}, 
            {"@N": "QuickStyleEffectsMatrix", "@V": 1}, 
            {"@N": "QuickStyleFontMatrix", "@V": 1}, 
            {"@N": "BeginArrow", "@V": 8}
        ], 
        "Section": [
            {"@N": "Character",
            "Row": [
                {"@IX": 0, 
                    "Cell": [
                        {"@N": "Size", "@V": "0.194", "@U": "PT"}
                    ]
                }
            ]}, 
            { "@N": "Geometry", 
                "@IX": 0, 
                "Cell": [
                    {"@N": "NoFill", "@V": 1},
                    {"@N": "NoLine", "@V": 0},
                    {"@N": "NoShow", "@V": 0},
                    {"@N": "NoSnap", "@V": 0},
                    {"@N": "NoQuickDrag", "@V": 0}
                ],
                "Row": [
                    {"@T": "MoveTo", "@IX": 1, 
                    "Cell": [
                        {"@N": "X", "@V": 0, "@F": "Width*0"}, 
                        {"@N": "Y", "@V": 0.124, "@F": "Height*1"}
                    ]}, 
                    {"@T": "EllipticalArcTo", "@IX": 2, 
                    "Cell": [
                        {"@N": "X", "@V": 0.625, "@F": "Width*1"}, 
                        {"@N": "Y", "@V": 0.124, "@F": "Height*1"}, 
                        {"@N": "A", "@V": 0.276, "@U": "DL", "@F": "Width*0.402"}, 
                        {"@N": "B", "@V": 0, "@U": "DL", "@F": "Height*0"}, 
                        {"@N": "C", "@V": -2.498, "@U": "DA", "@F": "_ELLIPSE_THETA(0.643,1.333,0.625,0.124,Width,Height)"}, 
                        {"@N": "D", "@V": 1.333, "@F": "_ELLIPSE_ECC(0.643,1.333,0.625,0.124,Width,Height,Geometry1.C2)"} 
                    ]}
                ]
            }] 
    };
};
