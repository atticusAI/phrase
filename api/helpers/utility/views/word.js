exports.Paragraph = function () {
    return {
        "w:p": {
            "w:pPr": {
                "w:pStyle": { "@w:val": "ListParagraph" },
                "w:jc": { "@w:val": "left" },
                "w:ind": { "@w:firstLine": 0, "@w:right": 0, "@w:left": 0 },
                "w:spacing": { "@w:before": 160, "@w:after": 0, "@w:line": 360, "@w:lineRule": "auto" },
                "w:contextualSpacing": { "@w:val": 0 },
                "w:tabs": {
                    "w:tab": { "@w:val": "left", "@w:pos": 1080 }
                },
                "w:numPr": {
                    "w:ilvl": { "@w:val": 0 },
                    "w:numId": { "@w:val": 1 }
                }
            } 
        }
    };
};

exports.Text = function() {
    return { 
        "w:r": {
            "w:t": {
                "@xml:space": "preserve", 
                "#text": this.text
            }
        }
    };
};

exports.RefSource = function() {
    return {
        "w:fldSimple": {
            "@w:instr": `SEQ ${this.type} \\\\* MERGEFORMAT`, 
            "w:bookmarkStart": {"@w:id": this.id, "@w:name": `_${this.type}${this.id}` }, 
            "w:r" : {
                "w:rPr": {
                    "w:sz": {"@w:val": 24}, 
                    "w:b":{}
                },
                "w:t": {"@xml:space": "preserve", "#text": '#'}
            }, 
            "w:bookmarkEnd": {"@w:id": this.id}
        }
    };
};

exports.RefTarget = function() {
    return {
        "w:fldSimple": {
            "@w:instr": `REF _${this.type}${this.srcid} \\\\* CHARFORMAT`,
            "w:r": { 
                "w:rPr": {
                    "w:sz" : {"@w:val": 24}
                }, 
                "w:t": {"@xml:space": "preserve", "#text": '#'}
            }
        }
    };
};

exports.ComponentList = function() {
    return null;
};
