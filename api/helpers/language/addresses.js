exports.address_book = {
	'micron': {header: 'HOLLAND {AMP} HART LLP', address: 'P.O. Box 11583', region: 'Salt Lake City, Utah 84147', tel: '208-342-5000', fax: '208-343-8869'},
	'vivint': {header: 'HOLLAND {AMP} HART LLP', address: '222 South Main Street, Suite 2200', region: 'Salt Lake City, Utah 84101', tel: '(801) 799-5800', fax: '(801) 799-5700', email: "jskarren@hollandhart.com"},
	'default': {header: 'HOLLAND {AMP} HART LLP', address: 'P.O. Box 11583', region: 'Salt Lake City, Utah 84147', tel: '303-473-2700', fax: '303-473-2720'}
}
