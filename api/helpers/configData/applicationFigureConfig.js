exports.template_list = {
  'DEFAULT': {
    input_module: 'receiver',
		primary_module: 'wireless communications manager',
		output_module: 'transmitter',
		hardware_modules: ['processor','memory','software','transceiver'],
		blocks: ['MA','MB','MC','HA'],
		// hardware: ['A'],
		// method_types: ['method', 'mpf', 'app', 'crm'],
  },
	'LTE': {
		input_module: 'receiver',
		primary_module: 'wireless communications manager',
		output_module: 'transmitter',
		hardware_modules: ['processor','memory','software','transceiver'],
		blocks: ['MA','MB','MC','HA'],
		// hardware: ['A'],
		// method_types: ['method', 'mpf', 'app', 'crm'],
	},
	'WLAN': {
		input_module: 'receiver',
		primary_module: 'communications manager',
		output_module: 'transmitter',
		hardware_modules: ['processor','memory','software','transceiver',],
		blocks: ['MA','MB','MC','HA'],
		// hardware: ['A'],
		// method_types: ['method', 'mpf', 'app', 'crm'],
	},
	'BLAM': {
		input_module: 'receiver',
		primary_module: 'communications manager',
		output_module: 'transmitter',
		hardware_modules: 	['processor','memory','software','transceiver',],
		blocks: ['HB','HC'],
		// hardware: ['B','C'],
		// method_types: ['method', 'mpf', 'app', 'crm'],
	},
	'VIV': {
		input_module: 'receiver',
		primary_module: 'communications manager',
		output_module: 'transmitter',
		hardware_modules: ['processor','memory','software','transceiver','I/O controller','user interface',],
		blocks: ['MA','MB','MC','HA'],
		// hardware: ['A'],
		// method_types: ['method', 'mpf', 'app', 'crm'],
	},
  'AKONIA': {
    input_module: 'input',
    primary_module: 'display manager',
    output_module: 'output',
    hardware_modules: ['processor','memory','software','I/O controller','user interface'],
    blocks: [],
    // hardware: [],
    // method_types: ['method', 'mpf', 'crm'],
  },
	'RMS': {
		input_module: 'input',
		primary_module: 'patient monitoring manager',
		output_module: 'output',
		hardware_modules: ['processor','memory','software','transceiver','I/O controller','user interface'],
		blocks: ['MA','MB','MC','HA'],
		// hardware: ['A'],
		// method_types: ['method','app'],
	},
	'MICRON': {
		primary_module: 'memory controller',
    primary_submodules: ['biasing component','timing component'],
		hardware_modules: 	['memory cells','BIOS component','processor','I/O controller','peripheral components'],
		blocks: ['MM','MC','HA'],
		// hardware: ['A'],
		// method_types: ['method', 'ctr'],
	},
  'VIASAT': {
    primary_module: 'scheduler',
    primary_submodules: ['uplink scheduler','downlink scheduler'],
    hardware_modules: ['modem','BIOS component','processor','I/O controller','peripheral components'],
    blocks: ['MC', 'HA'],
    // hardware: ['A'],
    // method_types: ['method', 'mpf', 'crm'],
  },
	'ICON': {
		input_module: 'input module',
		primary_module: 'media system manager',
		output_module: 'output module',
		hardware_modules: 	['processor','I/O controller','memory',],
		blocks: ['HC'],
		// hardware: ['C'],
		// method_types: ['method', 'app'],
	},
  'CORNING': {
    input_module: 'input module',
    primary_module: 'manufacturing controller',
    output_module: 'output module',
    hardware_modules: ['processor','I/O controller','peripheral components'],
    blocks: ['MB', 'HA'],
    // hardware: ['A'],
    // method_types: ['method', 'mpf'],
  },
  'SALESFORCE': {
    input_module: 'input module',
    primary_module: 'data processing component',
    output_module: 'output module',
    hardware_modules:   ['processor','memory','database controller','database','I/O controller',],
    blocks: ['MB','MC','HA'],
    module_summary: 'In some cases, {DEVICE} {FIG}05 may be an example of a user terminal, a database server, or a system containing multiple computing devices.',
    // hardware: ['A'],
    // method_types: ['method', 'app', 'crm'],
  },
}

exports.device_list = {
  'UE': {
    hardware_modules: ['antenna', 'I/O controller']
  },
  'BASE STATION': {
    hardware_modules: ['antenna', 'network communications manager', 'inter-station communications manager']
  },
  'AP': {
    hardware_modules: ['antenna', 'I/O controller']
  },
  'STA': {
    hardware_modules: ['antenna', 'I/O controller']
  },
  'WIRELESS DEVICE': {
    hardware_modules: ['antenna', 'I/O controller']
  },
  'DEFAULT': {
    hardware_modules: ['I/O controller']
  }
}
