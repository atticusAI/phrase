const FIG_TOOLS = require("./tools/figureTools.js");
const B_TOOLS = require("./tools/blockDescriptionTools.js");
const S_TOOLS = require('./tools/stringTools.js');
const TAGS = require('./tools/tagTools');
const config = require('../config');
const V_TOOLS = require('./tools/visioTools');
const C_TOOLS = require('./tools/componentTools');

var masters = {llr: '13', lll: '89', llu: '23', lld: '24', ue: '4', bs: '22', sv: '45', tda: '12', ant: '0', crv: '26', lnk: '6', arr: '9', dar: '49', mem: '78', smc: '43', ap: '7', db: '93', proc: '92'};
var page_content = "<PageContents xml:space='preserve' xmlns='http://schemas.microsoft.com/office/visio/2012/main' xmlns:r='http://schemas.openxmlformats.org/officeDocument/2006/relationships'>"
var page_ref = "<Section N='Field'><Row IX='0'><Cell N='Value' V='0' F='PAGENUMBER()'/><Cell N='Format' V='esc(0)' U='STR' F='FIELDPICTURE(0)'/></Row></Section>"
var page_label = mark('Shape', shapePosition(6.75, 1) + mark('Shapes', mark('Shape', '', {ID: 996, Type: 'Shape', MasterShape: 6} ) + mark('Shape', page_ref + "<Section N='Character'><Row IX='0'><Cell N='Size' V='0.19444444444' U='PT'/></Row></Section><Section N='Geometry' IX='0'><Cell N='NoFill' V='1'/><Cell N='NoLine' V='1'/></Section>", {ID: 997, Type: 'Shape'} ) ), {Name: 'label', ID: 995, Type: 'Group', Master: masters.crv} )
+ mark('Shape', shapeSize(.95, .25) + shapePosition(3.7375, .55) + page_ref + "<Section N='Character'><Row IX='0'><Cell N='Size' V='0.25' U='PT'/></Row></Section>" + shapeText("FIG. <fld IX='0'>#</fld>"), {Name: 'label', ID: 998, Type: 'Shape',})

var SF_DEV_NAMES = config.salesForce.deviceNames; // ['database server', 'data store', 'application server', 'user device', 'source database', 'target database', 'operational data store', 'analytical data store'];
var MICRON_DEV_NAMES = config.micron.deviceNames;

exports.getNarrativeFigure = function(fig_hash){
  var narrative_xml = ""

  var text_tree = [{text: 'Narrative figure', label: 'X'}];
  narrative_xml = exports.flowchartMarkup(text_tree, fig_hash, true);

  return narrative_xml;
}

exports.getBlockFigures = function(labeled_device, block_type, client){
  var figure_labels = labeled_device[1];
  labeled_device = labeled_device[0];
  labeled_device.primary_component =  B_TOOLS.getComponentByTag(labeled_device, 'primary');
  labeled_device.input_component =  B_TOOLS.getComponentByTag(labeled_device, 'input');
  labeled_device.output_component =  B_TOOLS.getComponentByTag(labeled_device, 'output');
  // labeled_device.static_modules = labeled_device.static_modules ||
  labeled_device.static_components = B_TOOLS.getAllComponentsByTag(labeled_device, 'static');

  var canonical_component = {"body_text" : "",
  "label" : "",
  "submodules" : [],
  "component_class" : 0};
  var markup = '';
  var pm_name = labeled_device.primary_component.component_name;

  switch(block_type){
    case "MA":
      var text_tree = JSON.parse(JSON.stringify(canonical_component));
      text_tree.label = labeled_device.text; // List of labels, MA is first, label is second element
      text_tree.submodules = [JSON.parse(JSON.stringify(canonical_component)),
      JSON.parse(JSON.stringify(canonical_component)),
      JSON.parse(JSON.stringify(canonical_component))];
      text_tree.submodules[0].body_text = labeled_device.input_component.component_name;
      text_tree.submodules[1].body_text = S_TOOLS.capitalize(pm_name);
      text_tree.submodules[2].body_text = labeled_device.output_component.component_name;

      text_tree.submodules[0].label = labeled_device.input_component.text;
      text_tree.submodules[1].label = labeled_device.primary_component.text;
      text_tree.submodules[2].label = labeled_device.output_component.text;

      text_tree.submodules[1].component_class = '1';

      markup = exports.blockMarkup(text_tree, figure_labels[0][1]);
      break;

    case "MB":
      var text_tree = JSON.parse(JSON.stringify(canonical_component));
      text_tree.label = labeled_device.text; // List of labels, MA is first, label is second element
      text_tree.submodules = [JSON.parse(JSON.stringify(canonical_component)),
      JSON.parse(JSON.stringify(canonical_component)),
      JSON.parse(JSON.stringify(canonical_component))];
      text_tree.submodules[0].body_text = labeled_device.input_component.component_name;
      text_tree.submodules[1].body_text = S_TOOLS.capitalize(pm_name);
      text_tree.submodules[2].body_text = labeled_device.output_component.component_name;

      text_tree.submodules[0].label = labeled_device.input_component.text;
      text_tree.submodules[1].label = labeled_device.primary_component.text;
      text_tree.submodules[2].label = labeled_device.output_component.text;

      text_tree.submodules[1].component_class = '1';

      var submodules = [];
      var seen_names = [];

      TAGS.getIndependentClaimComponents(labeled_device).forEach(function(c) {
          var this_subm = JSON.parse(JSON.stringify(canonical_component));
          this_subm.body_text = S_TOOLS.capitalize(c.component_name);
          this_subm.label = c.text;

          if (c.component_name !== "Bus" && seen_names.indexOf(c.component_name) < 0) { // && c.component_type == "independent"){
            submodules.push(this_subm);
            seen_names.push(c.component_name);
          }
        }
      );

      text_tree.submodules[1].submodules = submodules;
      return exports.blockMarkup(text_tree, figure_labels[1][1]);
      break;

    case "MC":
      var text_pairs = [];
      var seen_names = [];
      if (labeled_device.components != null){
        labeled_device.components.forEach(function(c){
          feature = false;
          c.component_tags.forEach(function(t){
            if(t.tag_name == 'feature'){
              feature = true;
            }
          })
          if(seen_names.indexOf(c.component_name) < 0 && feature == true){
           text_pairs.push({text : S_TOOLS.capitalize(c.component_name), label: c.text});
           seen_names.push(c.component_name);
          }
        })
      }

      return exports.busMarkup(text_pairs, labeled_device.text, figure_labels[2][1])
      break;

    case "HA":
      var input = {"claim_component" : [S_TOOLS.capitalize(pm_name),
      labeled_device.primary_component.text],
      "type" : labeled_device.device_name,
      "components" : [],
      "bus_label" : "400",
      "device_label" : labeled_device.text,
      "system_style" : "",
      "sw_component" : C_TOOLS.softwareComponent(labeled_device.components)};


      if (Array.isArray(labeled_device.static_components)) {
        labeled_device.static_components.forEach(function(c) {
          var static_component = [c.component_name, c.text]
          if (c.component_name !== "Bus") {
            // unique only
            if (!input.components.find(ic => ic[0] === c.component_name)) { 
              input.components.push(static_component);
            }
          } else {
            input.bus_label = c.text;
          }
          // if (c.component_name === "Software") { //} && ["UE", "base station"].indexOf(labeled_device.device_name) >= 0){
              // input.sw_label = m.tchildren[0].text;
          //     input.sw_label = true;
          // }
        });
      }

      if (['Micron', 'Salesforce'].indexOf(client) >= 0){
        input.system_style = 'IO';
      }

      markup = exports.hardwareMarkup(input, figure_labels[3][1]);

      break;
    default:

    }
  return markup;
}

exports.getFlowFigures = function(features){
  var figure_label = features[0]
  var text_pairs = [];

  features[1].forEach(function(f){
    text_pairs.push({text: S_TOOLS.capitalize(S_TOOLS.simplify(f.feature_text)), label: f.text})
  })

  var flow_xml = exports.flowchartMarkup(text_pairs, figure_label, false);
  return flow_xml
}

//////////////////////////////////////////////////////////////////////////////// 

exports.flowchartMarkup1 = function(text_pairs, figure_label, narrative){
  var steps = text_pairs || [];
  var step_width = 3;
  var step_x = 4.2 - step_width/2;
  var step_y = 8 + steps.length/2;
  
  var label_markup_list = [];
  var box_markup_list = [];
  var arrow_markup_list = [];

  text_pairs.forEach(function(step, index){
    var arrow_index = index + steps.length;
    var box_index = index + steps.length*2;
    var text = S_TOOLS.removeListSeparators( S_TOOLS.capitalize( S_TOOLS.infinitiveSentence(step.text) ) );
    var char_threshold = 160;
    var step_height = .65;

    step_height = (text.length > char_threshold*step_height) ? (text.length/char_threshold) + .1 : step_height;

    step_y = step_y - step_height - .5;
    var step_markup = mark('Shape', shapePosition(step_x, step_y) + shapeSize(step_width, step_height) + shapeSquare() + shapeText(text), {ID: (box_index*10 + 1), Type: 'Shape', LineStyle: '3', FillStyle: '3', TextStyle: '3'});

    var label_markup = labelMarkup('llr', step_x + step_width/2 + 1.97, step_y + step_height/2 + 0.1, index, step.label);
    var arrow_markup = arrowMarkup(step_x + step_width/2, step_y, step_x + step_width/2, step_y - .5, arrow_index);

    if (narrative == false){
      label_markup_list.push(label_markup);
      arrow_markup_list.push( index < (steps.length - 1) ? arrow_markup : '');
    }

    box_markup_list.push(step_markup);
  })

  flowchart_markup = page_content + '<Shapes>' + label_markup_list.join('') + arrow_markup_list.join('') + box_markup_list.join('') + page_label + "</Shapes>\n</PageContents>";
  
  return flowchart_markup;
}

exports.flowchartMarkup = function(text_pairs, figure_label, narrative){
  var steps = text_pairs || [];

  var est_height = estimate_fc_height(text_pairs, 0.65, 160, 0.5);
  if(est_height < 8){
    var step_width = 3;
    var step_height = 0.65;
    var char_threshold = 160;
    var step_y = 6.0 + est_height/2;
    var step_x = 4.2 - step_width/2;  
  } else {
    var step_width = 5;
    var step_height = 0.65;
    var char_threshold = 320;
    var step_y = 6.0 + estimate_fc_height(text_pairs, 0.65, 320, 0.5)/2;
    var step_x = 4.2 - step_width/2;
  }

  var label_markup_list = [];
  var box_markup_list = [];
  var arrow_markup_list = [];

  text_pairs.forEach(function(step, index){
    var arrow_index = index + steps.length;
    var box_index = index + steps.length*2;
    var text = S_TOOLS.removeListSeparators( S_TOOLS.capitalize( S_TOOLS.infinitiveSentence(step.text) ) );
    // var char_threshold = 160;
    step_height = .65;

    step_height = (text.length > char_threshold*step_height) ? (text.length/char_threshold) + .1 : step_height;

    step_y = step_y - step_height - .5;
    var step_markup = mark('Shape', shapePosition(step_x, step_y) + shapeSize(step_width, step_height) + shapeSquare() + shapeText(text), {ID: (box_index*10 + 1), Type: 'Shape', LineStyle: '3', FillStyle: '3', TextStyle: '3'});

    var label_markup = labelMarkup('llr', step_x + step_width + 0.47, step_y + step_height/2 + 0.1, index, step.label);
    var arrow_markup = arrowMarkup(step_x + step_width/2, step_y, step_x + step_width/2, step_y - .5, arrow_index);

    if (narrative == false){
      label_markup_list.push(label_markup);
      arrow_markup_list.push( index < (steps.length - 1) ? arrow_markup : '');
    }

    box_markup_list.push(step_markup);
  })

  flowchart_markup = page_content + '<Shapes>' + label_markup_list.join('') + arrow_markup_list.join('') + box_markup_list.join('') + page_label + "</Shapes>\n</PageContents>";
  
  return flowchart_markup;
}

function estimate_fc_height(text_pairs, step_height, char_threshold, separation){
  var height = 0
  // var step_height = 0.65
  // var char_threshold = 160;
  text_pairs.forEach(function(step, index){
    var text = S_TOOLS.removeListSeparators( S_TOOLS.capitalize( S_TOOLS.infinitiveSentence(step.text) ) );
    height += (text.length > char_threshold*step_height) ? (text.length/char_threshold) + .1 : step_height;
    height += separation;
  })
  height -= separation
  return height;
}

exports.blockMarkup = function(text_tree, figure_label){

  // Modules are the main blocks, submodules are the blocks within blocks.
  // Hierarchy of the incoming JSON specifies whether a module contains any submodules
  var modules = [];
  var device_width = 6.2; //** key parameter

  // var page_label = pageLabelMarkup(figure_label);

  var device_height = 2.5; // Initial outer box height
  text_tree.submodules.forEach(function(submodule){
    var submodule_height = 0.75 + submodule.submodules.length * 1.0
    device_height = Math.max(device_height, submodule_height + 1.0 )
  })
  
  // device_height = device_height + 2.0;

  // Make the big box around the smaller boxes
  var device_x = 4.3 - device_width/2;
  var device_y = 5.5 - device_height/2;
  var device_markup = moduleMarkup(device_x, device_y, device_width, device_height, text_tree.body_text, 0, text_tree.label);

  var base_module_width = 1.2; //1.6; //** key parameter
  var base_module_height = 1.2; //** key parameter
  var primary_module_width = 2.6; // 1.8; //** key parameter
  var primary_module_height = 1.5; //** key parameter
  var arrow_width = .4; //** key parameter
  
  var module_x = device_x + arrow_width/2;
  var base_module_y = device_y + (device_height - base_module_height)/2 + .1;
  var module_markup = '';
  var submodule_markup = '';

  var arrow_x = 1;
  var arrow_y = base_module_y + base_module_height/2;
  var input_arrow = arrowMarkup(arrow_x, arrow_y, arrow_x + arrow_width, arrow_y, 0);
  device_markup += input_arrow;

  var outer_markup = '';
  var inner_markup = '';

  text_tree.submodules.forEach(function(submodule, index){

    var module_name = S_TOOLS.capWords(submodule.body_text);

    var module_width = (submodule.component_class == '1') ? primary_module_width : base_module_width;
    var module_height = (submodule.component_class == '1') ? primary_module_height : base_module_height;

    submodule_height = Math.max(0.5, 0.75 + submodule.submodules.length * 1.0)

    var module_y = (module.component_class == '1') ? base_module_y - (submodule_height - base_module_height)/2 : base_module_y;
    var align_top = (submodule.submodules.length > 0) ? true : false;

    var sub_markup = '';
      module_height = submodule_height; // device_height - 1;
      module_y = base_module_y - (module_height - base_module_height)/2;
      submodule.submodules.forEach(function(_submodule, sub_index){
        var combo_index = index + sub_index + text_tree.submodules.length;
        var submodule_name = S_TOOLS.capWords(_submodule.body_text);
        var submodule_width = module_width - .2;
        var _submodule_height = .5;
        var submodule_x = module_x + .1;
        var submodule_y = module_y + module_height - 1.15 - (sub_index*(0.5+0.5));
        var next_submodule_markup = moduleMarkup(submodule_x, submodule_y, submodule_width, _submodule_height, submodule_name, combo_index, _submodule.label);          

        inner_markup += next_submodule_markup;
          // index = index + 1;
        })

      var next_module_markup = moduleMarkup(module_x, module_y, module_width, submodule_height, module_name, index+1, submodule.label, {align_top: align_top});
      var arrow_markup = (index < 2) ? arrowMarkup(module_x + module_width, arrow_y, module_x + module_width + arrow_width, arrow_y, (index + 1), 'dar') : arrowMarkup(module_x + module_width, arrow_y, module_x + module_width + arrow_width, arrow_y, index + 1);

      outer_markup += next_module_markup + arrow_markup;
      // module_markup += next_module_markup + sub_markup + arrow_markup;
      module_x += module_width + arrow_width;
      // index = index + 1;
    })

  return page_content + "<Shapes>" + device_markup + outer_markup + inner_markup + page_label + "</Shapes></PageContents>";
}

exports.busMarkup = function(text_pairs, block_label, figure_label){

  var module_width = 6.2; //** key parameter
  var module_height = ( ( (text_pairs.length + 1)/2) | 0)*1.1 + .6;
  var module_x = 4.3 - module_width/2;
  var module_y = 5.75 - module_height/2;
  var module_markup = moduleMarkup(module_x, module_y, module_width, module_height, '', 2, block_label);

  // var page_label = pageLabelMarkup(figure_label);

  var bus_thickness = .625; //** key parameter
  var bus_length = module_height - .3;
  var bus_x = module_x + module_width/2;
  var bus_top = module_y + (bus_length + module_height)/2;
  var bus = busArrowMarkup(bus_x, bus_top, bus_length, bus_thickness, text_pairs.length + 10)

  var submodule_markup = '';
  text_pairs.forEach(function(text_pair, index){
    var submodule_name = S_TOOLS.capWords(text_pair.text);
    var submodule_width = 2.25; //** key parameter
    var submodule_height = .5; //** key parameter
    var arrow_width = .5; //** key parameter
    var submodule_x = ( (index % 2) == 0 ) ? (module_x + module_width/2 - submodule_width - arrow_width - bus_thickness/6) : (module_x + module_width/2 + arrow_width + bus_thickness/6);
    var submodule_y = module_y + module_height - 1 - ( (index/2) | 0 )*1.1;
    var next_submodule_markup = moduleMarkup(submodule_x, submodule_y, submodule_width, submodule_height, submodule_name, index + 3, text_pair.label);
    
    var arrow_begin = ( (index % 2) == 0 ) ? (submodule_x + submodule_width) : (submodule_x - arrow_width);
    var arrow_markup = arrowMarkup(arrow_begin, submodule_y + submodule_height/2, arrow_begin + arrow_width, submodule_y + submodule_height/2, (index + 3), 'dar');

    submodule_markup += next_submodule_markup + arrow_markup;
  })

  return page_content + '<Shapes>' + module_markup + submodule_markup + bus + page_label + "</Shapes></PageContents>";
}

exports.hardwareMarkup = function(input, figure_label){

  var extra_width = .4
  var device_width = 5.3 //(figure_class == 'C') ? (5.3 + extra_width) : 5.3; //** key parameter
  var device_height = 6.3; //** key parameter
  var components = input.components || [];
  var antenna = false;
  components.forEach(function(component){
    if (component[0] == 'Antenna') {
      antenna = true;
      device_width = device_width + .4;
    }
  });
  
  // For the combined "UE, base station" device, filter away unneeded modules:
  if(input.type == 'UE' || input.type == 'base station'){
    these_components = [];
    for(var c in components){
      if ((input.type == 'UE' && ['Inter-station Communications Manager', 'Network Communications Manager'].indexOf(components[c][0]) < 0) ||
        (input.type == 'base station' && components[c][0] != 'I/O Controller')){
        these_components.push(components[c]);
      }
    }
    components = these_components;
  }

  // var page_label = pageLabelMarkup(figure_label);

  var device_x = 4.3 - device_width/2;
  var device_y = 5.25 - device_height/2;
  if (input.type == 'BS' || input.type == 'base station') device_y += 0.75;
  var device_markup = moduleMarkup(device_x, device_y, device_width, device_height, '', 0, input.device_label);

  var component_width = 1.5; //** key parameter
  var component_height = 1.25; //** key parameter
  var arrow_width = .5; //** key parameter

  var bus_thickness = .625; //** key parameter
  var bus_length = device_height - .5;
  var bus_x = antenna ? device_x + device_width/2 - .4 : device_x + device_width/2;
  // if (figure_class == 'C') bus_x = bus_x + extra_width/2;
  var bus_top = device_y + (bus_length + device_height)/2;
  var bus_label = labelMarkup('lll', bus_x - 0.365, bus_top + 0.3, 1, input.bus_label);
  var bus = busArrowMarkup(bus_x, bus_top, bus_length, bus_thickness, 0) + bus_label;

  var primary_component = {};
  var primary_component_markup = '';
  var primary_component_x = bus_x - bus_thickness/6 - arrow_width - component_width;
  var primary_component_y = device_y + device_height/2 - component_height/2;
  var primary_arrow_x = primary_component_x + component_width;
  var primary_arrow_y = primary_component_y + component_height/2;

  // if (figure_class == 'A'){
  var primary_component_name = input.claim_component[0] ? S_TOOLS.capWords(input.claim_component[0]) : '{PRIMARY_MODULE}';
  var primary_component_height = component_height;

  primary_component_markup += moduleMarkup(primary_component_x, primary_component_y, component_width, component_height, primary_component_name, 2, input.claim_component[1]);
  primary_component_markup += arrowMarkup(primary_arrow_x, primary_arrow_y, primary_arrow_x + arrow_width, primary_arrow_y, 2, 'dar');   
  // }

  var hardware_markup = '';
  var submodule_markup = '';
  var submodule_count = 0;
  var antenna_x = 0;
  var component_gap = .6; //** key parameter
  var starting_index = 2;
  var include_submodules = false;
  // var software = false;
  // var sw_label = false;
  var proc_label = '';
  var db_label = '';

  components.forEach(function(component){
    // if (component[0] == 'Software'){
    //   software = true;
    //   sw_label = component[1];
    // }
    if (component[0] == 'Processor'){
      proc_label = component[1];
    }
    if (component[0] == 'Database'){
      db_label = component[1];
    }
  })

  components = components.filter(c=>{
    return c[0] != "Software";
  });

  components.forEach(function(component, index){

    if (index == 0) hardware_markup += primary_component_markup;
      starting_index++;
      var hardware_component_name = component[0] ? S_TOOLS.capWords(component[0]) : '{MODULE_NAME}';

      // if (hardware_module_name == 'Software' && figure_class == 'C') return;
      var hardware_component_x;
      var alt_component_x = bus_x + arrow_width + bus_thickness/6;
      var hardware_component_y;
      var hardware_component_width = component_width;
      var hardware_component_height = component_height;
      var hardware_arrow_x = primary_arrow_x;
      var hardware_arrow_width = arrow_width;
      var mem_markup = '';
      var mem_arrow = '';

      if (hardware_component_name == 'Processor') {
        hardware_component_x = alt_component_x;
        hardware_component_y = primary_component_y - component_height - component_gap;
        hardware_arrow_x = hardware_component_x - hardware_arrow_width;
      } else if (hardware_component_name == 'Memory') {

        // Draw the main memory box component
        var mem_component_x = bus_x + bus_thickness/6 + arrow_width; // + module_width;
        var mem_component_y = device_y + device_height/2 - component_height/2;
        var mem_arrow_x = mem_component_x - hardware_arrow_width;
        var mem_arrow_y = mem_component_y + component_height/2;

        mem_markup = moduleMarkup(mem_component_x, mem_component_y, component_width, component_height, 'Memory', starting_index, component[1], {align_top : typeof input.sw_component !== 'undefined'});
        mem_arrow = arrowMarkup(mem_arrow_x, mem_arrow_y, mem_arrow_x + hardware_arrow_width, mem_arrow_y, starting_index, 'dar'); 

        if (input.sw_component) {
          starting_index += 1
          hardware_component_width = component_width - .2;
          hardware_component_height = component_height - .75;
          hardware_component_x = alt_component_x + .1;
          hardware_component_y = primary_component_y + .45;
            // Change the name to make this a HW component
            hardware_component_name = 'Code';
            component[1] = input.sw_component.text;
        }
      }
      else if (hardware_component_name == 'Transceiver') {
        hardware_component_width = antenna ? 1 : component_width; //** key parameter
        hardware_component_x = alt_component_x;
        hardware_component_y = primary_component_y + component_height + component_gap;
        hardware_arrow_x = hardware_component_x - hardware_arrow_width;
      }
      else if (hardware_component_name == 'Antenna') {
        hardware_component_width = 1; //** key parameter
        hardware_arrow_width = .3; //** key parameter
        hardware_component_x = alt_component_x + hardware_component_width + hardware_arrow_width;
        hardware_component_y = primary_component_y + component_height + component_gap;
        hardware_arrow_x = hardware_component_x - hardware_arrow_width;
        antenna_x = hardware_component_x + hardware_component_width/2;
      }
      // else if (hardware_module_name == 'I/O Controller') { // && figure_class == 'C') {
        //     hardware_module_x = alt_module_x;
        //     hardware_module_y = primary_module_y + module_height + module_gap;
        //     hardware_arrow_x = hardware_module_x - hardware_arrow_width;
        // }
      else if (hardware_component_name == 'Network Communications Manager' || hardware_component_name == 'I/O Controller') {
        hardware_component_x = primary_component_x;
        hardware_component_y = primary_component_y + component_height + component_gap;
      }
      else if (hardware_component_name == 'Inter-station Communications Manager' || hardware_component_name == 'User Interface') {
        hardware_component_x = primary_component_x;
        hardware_component_y = primary_component_y - component_height - component_gap;
      }
      else if (hardware_component_name == 'Memory Cells') {
        hardware_component_x = primary_component_x;
        hardware_component_y = primary_component_y - component_height - component_gap;
      }
      else if (hardware_component_name == 'BIOS Component') {
        hardware_component_x = alt_component_x;
        hardware_component_y = primary_component_y;
        hardware_arrow_x = hardware_component_x - hardware_arrow_width;
      }
      else if (hardware_component_name == 'Peripheral Components') {
        hardware_component_x = alt_component_x;
        hardware_component_y = primary_component_y + component_height + component_gap;
        hardware_arrow_x = hardware_component_x - hardware_arrow_width;
      }
      else if (hardware_component_name == 'Database') {
        hardware_component_x = alt_component_x;
        hardware_component_y = primary_component_y + component_height + component_gap;
        hardware_arrow_x = hardware_component_x - hardware_arrow_width;
      }
      else if (hardware_component_name == 'Database Controller') {
        hardware_component_x = primary_component_x;
        hardware_component_y = primary_component_y - component_height - component_gap;
      }
      else if (hardware_component_name == '{ADD_MODULE}') {
        hardware_component_x = primary_component_x;
        hardware_component_y = primary_component_y + component_height + component_gap;
      }
      else {
        hardware_component_x = primary_component_x;
        hardware_component_y = primary_component_y - 4 - index;
      }

      var hardware_component_markup = moduleMarkup(hardware_component_x, hardware_component_y, hardware_component_width, hardware_component_height, hardware_component_name, starting_index, component[1] );

      var hardware_arrow_y = hardware_component_y + hardware_component_height/2;
      var hardware_component_arrow_markup = (hardware_component_name == 'Code') ? '' : arrowMarkup(hardware_arrow_x, hardware_arrow_y, hardware_arrow_x + hardware_arrow_width, hardware_arrow_y, starting_index, 'dar'); 

      if (input.system_style != 'IO' || ['Database', 'Processor'].indexOf(hardware_component_name) < 0 ){
        hardware_markup += mem_markup + mem_arrow + hardware_component_markup + hardware_component_arrow_markup;
      }

    // else if ( (parseInt(module.module_class) == 1) && figure_class != 'A' ) {
      //     include_submodules = true;
      //     var submodules = module.submodules || []; 
      //     submodule_count = submodules.length;
      //     primary_module = module;
      // }
      // else if (parseInt(module.module_class) == 1) {
      //     starting_index++;
      //     //if (modules[index + 1] && modules[index + 1].module_class == '0') starting_index++;
      // }

  })

  // if (include_submodules){
  //     var submodules = primary_module.submodules || []; 
  //     submodules.forEach(function(submodule, sub_index){
  //         starting_index++;
  //         var submodule_x = (figure_class == 'C') ? (primary_module_x - extra_width/2) : primary_module_x;
  //         var submodule_y = device_height + device_y - 1.55 - 1.1*sub_index;
  //         var submodule_width = module_width;
  //         var submodule_height = .5;
  //         var submodule_arrow_x = submodule_x + submodule_width;
  //         var submodule_arrow_y = submodule_y + submodule_height/2;
  //         var submodule_arrow_width = arrow_width;
  //         var submodule_name = submodule.module_name ? T.capWords(submodule.module_name) : '{SUBMODULE_NAME}';
  //         //console.log(submodule_name)

  //         submodule_markup += moduleMarkup(submodule_x, submodule_y, submodule_width, submodule_height, submodule_name, starting_index);
  //         submodule_markup += (figure_class == 'C') ? '' : arrowMarkup(submodule_arrow_x, submodule_arrow_y, submodule_arrow_x + submodule_arrow_width, submodule_arrow_y, starting_index, 'dar'); 
  //     })
  // }

  var antenna_markup = antenna ?  masterShapeMarkup(antenna_x,primary_component_y + 3.374, (starting_index + 3), 'ant') : '';
  var system_markup = '';
  var top_row = primary_component_y + component_height*2 + component_gap + 1.75; //** key parameter
  var bottom_row = 1.75; //** key parameter

  if (input.type == 'base station' || input.type == 'BS') {
    var bs1 =   masterShapeMarkup(1.975, bottom_row, (starting_index + 4), 'bs', '105');
    var bs2 =   masterShapeMarkup(3.3, bottom_row, (starting_index + 14), 'bs', '105');
    var c1 =    arrowMarkup(2.75, bottom_row - 0.5, 2.75, primary_component_y - component_height - component_gap, (starting_index + 24), 'arr');
    var c2 =    arrowMarkup(2.35, bottom_row - 0.5, 3.15, bottom_row - .5, (starting_index + 25), 'dar');
    var c3 =    arrowMarkup(2.75, primary_component_y + component_height*2 + component_gap, 2.75, top_row - 0.5, (starting_index + 26), 'dar');
    var sv =    masterShapeMarkup(2.65, top_row - 0.25, (starting_index + 27), 'sv', '130');
    var ue1 =   masterShapeMarkup(5.5, top_row, (starting_index + 37), 'ue', '115');
    var ue2 =   masterShapeMarkup(7.1, top_row, (starting_index + 47), 'ue', '115');
    var lnk1 =  arrowMarkup(5.65, top_row - 0.1, 6.2, top_row - 1.15, (starting_index + 57), 'lnk');
    var lnk2 =  arrowMarkup(6.4, top_row -1.15, 6.95, top_row - 0.1, (starting_index + 58), 'lnk');
    system_markup = bs1 + bs2 + c1 + c2 + c3 + sv + ue1 + ue2 + lnk1 + lnk2;
  }
  else if (input.type == 'UE') {
    var bs =    masterShapeMarkup(7.1, top_row + 0.25, (starting_index + 4), 'bs', '105');
    var ue1 =   masterShapeMarkup(5.5, top_row + 0.25, (starting_index + 37), 'ue', '115');
    var lnk1 =  arrowMarkup(5.65, top_row - 0.1, 6.2, top_row - 1.15, (starting_index + 57), 'lnk');    
    var lnk =   arrowMarkup(6.4, top_row - 1.15, 6.9, top_row + 0.525, (starting_index + 14), 'lnk');
    system_markup = bs + ue1 + lnk + lnk1;
  }
  else if (input.type == 'AP') {
    var ue =    masterShapeMarkup(7.1, top_row + 0.25, (starting_index + 4), 'ue', '115');
    var lnk =   arrowMarkup(6.4, top_row - 1.15, 6.9, top_row + 0.525, (starting_index + 14), 'lnk');
    system_markup = ue + lnk;
  }
  else if (input.type == 'STA') {
    var ap =    masterShapeMarkup(7.1, top_row + 0.25, (starting_index + 4), 'ap', '105');
    var lnk =   arrowMarkup(6.4, top_row - 1.15, 6.9, top_row + 0.525, (starting_index + 14), 'lnk');
    system_markup = ap + lnk;
  }
  // else if (system_style == 'VIV') {
  //     var sv1 =   masterShapeMarkup(2.875, top_row, (starting_index + 4), 'sv', '145');
  //     var sv2 =   masterShapeMarkup(5.325, top_row, (starting_index + 14), 'sv', '140');
  //     var c1 =    arrowMarkup(3.125, top_row, 5.125, top_row, (starting_index + 24), 'dar');
  //     var lnk1 =  arrowMarkup(5.65, top_row - 0.1, 6.2, top_row - 1.15, (starting_index + 25), 'lnk');
  //     var lnk2 =  arrowMarkup(6.4, top_row -1.15, 6.95, top_row - 0.1, (starting_index + 26), 'lnk');
  //     var ue =    masterShapeMarkup(7.1, top_row, (starting_index + 27), 'ue', '115');
  //     system_markup = sv1 + sv2 + c1 + lnk1 + lnk2 + ue
  // }
  else if (input.system_style == 'IO') {
    starting_index++;
    var io_width = 1.5;
    var io_height = .75;
    var io_x = device_x - .4;
    var left_column = bus_x + 1.3*bus_thickness + arrow_width;
    var db_y = primary_component_y + component_height + 2*component_gap + 0.025
    var proc_y = primary_component_y - component_height + 0.025
    // if (figure_class == 'C') io_x += 2.85
    // io_x += 2.85;
    var io_y = device_y + device_height + 0.75;
    var input = moduleMarkup(io_x, io_y, io_width, io_height, 'Input',(starting_index), '45');
    var output = moduleMarkup(io_x + 1.7, io_y, io_width, io_height, 'Output', (starting_index + 1), '50');
    var c1 = arrowMarkup(io_x + 1.35, io_y, io_x + 1.35, primary_component_y + component_height*2 + component_gap, (starting_index + 2), 'arr');
    var c2 = arrowMarkup(io_x + 1.85, primary_component_y + component_height*2 + component_gap, io_x + 1.85, io_y, (starting_index + 3), 'arr');
    var db =  db_label ? masterShapeMarkup(left_column, db_y, starting_index+500, 'db', db_label) : '';
    var proc = masterShapeMarkup(left_column, proc_y,  starting_index+600, 'proc', proc_label);
    var db_arr = arrowMarkup(bus_x+bus_thickness/6, db_y, bus_x+arrow_width+bus_thickness/6, db_y, (starting_index + 505), 'dar');
    var pc_arr = arrowMarkup(bus_x+bus_thickness/6, proc_y, bus_x+arrow_width+bus_thickness/6, proc_y, (starting_index + 510), 'dar');
    system_markup = input + output + c1  + c2 + db + proc + db_arr + pc_arr;
  }
  // else if (system_style == 'UE') {
  //     var link_x = (figure_class == 'C') ? (6.4 + extra_width/2) : 6.4;
  //     var ue =    masterShapeMarkup(7.25, top_row + 0.25, (starting_index + 4), 'ue', '115');
  //     var lnk =   arrowMarkup(link_x, top_row - 1.15, 7.1, top_row + 0.25, (starting_index + 14), 'lnk');
  //     system_markup = ue + lnk;
  // }
  return page_content + '<Shapes>' + device_markup + bus + hardware_markup + submodule_markup + antenna_markup + system_markup + page_label + "</Shapes></PageContents>";
}

function primaryModule(device){
  var primary_module = {};
  var modules = device.submodules ? device.submodules : [];
  modules.forEach(function(module){
    if (module.module_class == '1') primary_module = module;
  })
  return primary_module;
}

//TODO! good candidate for refactor into view model, blm 2018-07-02
function moduleMarkup(x, y, width, height, text, index, label, options){
  options = options || {};
  var align = '';
  var top_margin = 0.05;
  if (options.align_top) {
    align = '<Cell N="VerticalAlign" V="0"/>';
    top_margin = 0.2;
  } else if (options.align_bottom) {
    align = '<Cell N="VerticalAlign" V="2"/>';
  }

  if (options.align_right) {
    align += '<Section N="Paragraph"><Row IX="0"><Cell N="HorzAlign" V="2"/></Row></Section>';
  }
  if (label != false) {
    var markup = mark('Shape', shapeSize(width, height) + shapePosition(x, y) + shapeSquare() + align + shapeText(text) + margins(top_margin, 0.05, 0.05, 0.05), {ID: (index*10 + 2), Name: 'Module' + (index*10 + 2), Type: 'Shape'}) + labelMarkup('lld', x + width/2, y, index, label);
  } else {
    var markup = mark('Shape', shapeSize(width, height) + shapePosition(x, y) + shapeSquare() + align + shapeText(text) + margins(top_margin, 0.05, 0.05, 0.05), {ID: (index*10 + 2), Name: 'Module' + (index*10 + 2), Type: 'Shape'});
  }
  return markup
}

function masterShapeMarkup(x, y, index, master, label, content){
  var label_y = y - 0.25;
  var label_x = x;
  if (master == 'bs') {
    label_y = y - 0.75;
    label_x = x + 0.2; 
  }
  else if (master == 'ue') {
    label_y = y - 0.22;
    label_x = x + 0.04; 
  }
  else if (master == 'sv') {
    label_x = x - 0.1; 
  }
  else if (master == 'db') {
    label_y = y - 0.72;
  }
  else if (master == 'proc'){
    label_y = y - 0.7
  }
  var label_markup = label ? labelMarkup('lld', label_x, label_y, index, label) : '';
  content = content || '';
  var type = label ? 'Shape' : 'Group';
  return label_markup + mark('Shape', shapePosition(x, y) + content, {ID: ((index + 1)*10), Type: type, Master: masters[master]} );
}


function shapePosition(x, y){
  var position_markup = "<Cell N='PinX' V='" + x + "'/><Cell N='PinY' V='" + y + "'/>";
  return position_markup;
}


function shapeSize(width, height){
  var size_markup = "<Cell N='Width' V='" + width + "'/><Cell N='Height' V='" + height + "'/>";
  return size_markup;
}

function shapeSquare(){
  var start = mark('Row', "<Cell N='X' V='0'/><Cell N='Y' V='0'/>",   {T: 'RelMoveTo', IX: 1});
  var line1 = mark('Row', "<Cell N='X' V='0'/><Cell N='Y' V='1'/>",   {T: 'RelLineTo', IX: 2}); 
  var line2 = mark('Row', "<Cell N='X' V='1'/><Cell N='Y' V='1'/>",   {T: 'RelLineTo', IX: 3}); 
  var line3 = mark('Row', "<Cell N='X' V='1'/><Cell N='Y' V='0'/>",   {T: 'RelLineTo', IX: 4}); 
  var line4 = mark('Row', "<Cell N='X' V='0'/><Cell N='Y' V='0'/>",   {T: 'RelLineTo', IX: 5}); 
  var square_markup = "<Cell N='LineWeight' V='0.01736111111111111'/>" + mark('Section', start + line1 + line2 + line3 + line4, {N: 'Geometry', IX: 0})
  return square_markup;
}


function shapeText(text){
  var text_markup = mark('Text', "<cp IX='0'/>" + text)
  return text_markup;
}


function distance(x1, y1, x2, y2) {
  return Math.sqrt ( Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
}


function angleFromPpoints(x1, y1, x2, y2){
  return Math.atan((y2 - y1)/(x2 - x1));
}


function margins(top, bottom, left, right){
  return '<Cell N="LeftMargin" U="PT" V="' + (left || 0.1) + '"/><Cell N="RightMargin" U="PT" V="' + (right || 0.1) + '"/><Cell N="TopMargin" U="PT" V="' + (top || 0.1) + '"/><Cell N="BottomMargin" U="PT" V="' + (bottom || 0.1) + '"/>';
}


function labelMarkup(type, x, y, index, label){
  var num = moduleNumber(index);
  if (['115', '130', '105'].indexOf(label) < 0){
    label = "<fld IX='0'>#</fld>" + label;
  }

  var label_id = (index*10 + 3);
  var master = masters[type] || 13;

  var inner_shapes = mark('Shapes', mark('Shape', '', {ID: (label_id + 1), Type: 'Shape'}) + mark('Shape', page_ref + "<Section N='Geometry' IX='0'><Cell N='NoFill' V='1'/><Cell N='NoLine' V='1'/></Section>" + shapeText(label), {ID: (label_id + 2), Type: 'Shape'}) );
  var label_markup = mark('Shape', shapePosition(x, y + .04) + inner_shapes, {Name: 'label', ID: label_id, Type: 'Group', Master: master});

  return label_markup;
}


function moduleNumber(index){
  return (index == 0) ? '05' : (index + 1)*5;
}

function arrowMarkup(x1, y1, x2, y2, index, type) { 
  var length = distance(x1, y1, x2, y2);
  var angle = angleFromPpoints(x1, y1, x2, y2);
  var master = ( (type && masters[type]) ? masters[type] : 9);
  var fill = (type == 'lnk') ? "<Cell N='FillForegnd' V='#000000' F='THEMEGUARD(RGB(0,0,0))'/><Cell N='FillBkgnd' V='#000000' F='THEMEGUARD(RGB(0,0,0))'/>" : '';

  var arrow_markup = mark('Shape', "<Cell N='PinX' V='" + x1 + "' F='BeginX'/><Cell N='PinY' V='" + y1 + "' F='BeginY'/><Cell N='Width' V='" + length + "' F='Inh'/><Cell N='LocPinX' V='0'/><Cell N='LocPinY' V='0'/><Cell N='Angle' V='" + angle + "' F='Inh'/><Cell N='BeginX' V='" + x1 + "'/><Cell N='BeginY' V='" + y1 + "'/><Cell N='EndX' V='" + x2 + "'/><Cell N='EndY' V='" + y2 + "'/>" + fill + "<Section N='Geometry' IX='0'><Row T='LineTo' IX='2'><Cell N='X' V='" + length + "' F='Inh'/></Row></Section>", {ID: (index*10 + 6), Name: 'Arrow', Type: 'Shape', Master: master});
  return arrow_markup;
}

function busArrowMarkup(bus_x, bus_top, bus_length, bus_thickness, index){
  return mark('Shape', "<Cell N='PinX' V='" + bus_x + "' F='(BeginX+EndX)/2'/><Cell N='PinY' V='" + (bus_top - bus_length/2) + "' F='(BeginY+EndY)/2'/><Cell N='Width' V='" + bus_length + "' F='SQRT((EndX-BeginX)^2+(EndY-BeginY)^2)'/><Cell N='Height' V='" + bus_thickness + "'/><Cell N='BeginX' V='" + bus_x + "'/><Cell N='BeginY' V='" + bus_top + "'/><Cell N='EndX' V='" + bus_x + "'/><Cell N='EndY' V='" + (bus_top - bus_length) + "'/>", {ID: (index*10 + 6), Name: 'Bus', Master: masters['tda']});
}

function pageLabelMarkup(fig_num){
  fig_num = "<fld IX='0'>#</fld>";
  var fig_arr = fig_num + '00';

  var page_label = mark('Shape', shapePosition(6.75, 1) + 
                   mark('Shapes', mark('Shape', '<Cell N="BeginArrow" V="8"/>', {ID: 996, Type: 'Shape'} ) + 
                   mark('Shape', page_ref + "<Section N='Character'><Row IX='0'><Cell N='Size' V='0.19444444444' U='PT'/></Row></Section><Section N='Geometry' IX='0'><Cell N='NoFill' V='1'/><Cell N='NoLine' V='1'/></Section>" + 
                   shapeText(fig_arr), {ID: 997, Type: 'Shape'} ) ), {Name: 'label', ID: 995, Type: 'Group', Master: masters.crv} );

  page_label += mark('Shape', shapeSize(.95, .25) + 
    shapePosition(3.7375, .55) + page_ref + "<Section N='Character'><Row IX='0'><Cell N='Size' V='0.25' U='PT'/></Row></Section>" + shapeText("FIG. " + fig_num), {Name: 'label', ID: 998, Type: 'Shape',});

  return page_label;
}

function mark(tag, content, attributes, leaf){
  var markup = '<' + tag;
  if (tag == 'input') leaf = true;
  for (var key in attributes){
    markup = markup + ' ' + key + '="' + attributes[key] + '"';
  }
  markup = (leaf) ? (markup + '/>') : (markup + '>' + content + '</' + tag + '>');
  return markup;
}

// build a dynamic visio diagram 
exports.getDynamicFigure = function(device) { 
  const vt = new V_TOOLS.VisioTools();
  const pg = new vt.Page();
  pg.layoutProvider = new vt.FlowLayout();
  for (var c in device.components) { 
    let cmp = device.components[c];
    var rec = new vt.LabeledRectangle(cmp.text, cmp.component_name);
    rec.addChildren(cmp.tchildren);
    pg.addShape(rec);
  }
  // pg.addShape(new vt.FigureLabel(pg));
  pg.addShape(new vt.DeviceLabel(device.text));
  pg.layout();
  return pg.toXML();
};
