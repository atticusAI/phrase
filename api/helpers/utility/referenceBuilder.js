const REJECTION_INFO = require("../language/rejectionInfo.js");
const G_TOOLS = require("./tools/genericTools.js");
const S_TOOLS = require("./tools/stringTools.js");

// takes a list of references and returns them as a list formated string
// if the same reference exists in previous_references then used the shortened version
exports.referenceList = function(references, previous_references){
	var ref_text;
	if(previous_references){
    ref_text = G_TOOLS.writeList(references.map(function(reference){
			var long = true;			
			previous_references.forEach(function(prev_ref){
				if (prev_ref.ref_short_name == reference.ref_short_name) long = false;
			});
			if (long){
				return reference.ref_citation + ' to ' + (reference.ref_author || '').replace(/\(.*\)/g, '') + ' (“' + S_TOOLS.cleanAuthor(reference.ref_short_name) + '”)';
			} else { 
				return S_TOOLS.cleanAuthor(reference.ref_author);
			}
		}), ',');
  }else{
		ref_text = G_TOOLS.writeList(references.map(function(reference){
			return S_TOOLS.cleanAuthor(reference.ref_short_name);
		}), ',');
  }
	return ref_text;
	// return G_TOOLS.writeList(references.map(function(reference){
	// 	return reference.ref_citation;
	// }))
}


// function referenceMarkup(reference, claims, ref_cites, author){
// 	var cites = ref_cites[author] || [];
// 	var markup  = '';
// 	cites.forEach(function(cite){
// 		var claim = cite.claim || 0;
// 		if (claims.indexOf( parseInt(claim) ) > -1) 
// 			markup += T.p( T.r(author + ' cannot be relied upon to teach “' + cite.feature + ',” as recited in claim ' + claim + '.') , oa_par_props)
// 				+ T.p( T.r('The Office Action relies specifically on ' + cite.pa_cite + ' of ' + author + ' to teach this feature.  Office Action, ' + cite.oa_cite + '. However, ' + author + ' states “'+ cite.quote + '”.  ' + author + ', ' + cite.pa_cite + '. This is different from features recited in claim ' + claim + ', because **ADD TEXT DESCRIBING KEY DIFFERENCE**.  ') , oa_par_props);
// 	});
// 	return markup;
// };
