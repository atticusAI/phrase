exports.p = function(content, prop_obj, sect_obj){
  var section = sect_obj ? exports.sectPr(sect_obj.lnNumType, sect_obj.next_page, sect_obj.header_ref, sect_obj.footer_ref, sect_obj.pgSz, sect_obj.pgMar) : '';
  var properties = prop_obj ? exports.pPr(section, prop_obj.jc, prop_obj.firstLine_ind, prop_obj.right_ind, prop_obj.left_ind, prop_obj.before, prop_obj.after, prop_obj.line, prop_obj.keep, prop_obj.tabs, prop_obj.numbering, prop_obj.num_style_id) : exports.pPr(section);
  return exports.w_node('p', properties + content);
}

exports.pPr = function(content, jc, firstLine_ind, right_ind, left_ind, before, after, line, keep_w_next, tab_list, numbering, num_style_id){
  jc = jc ? jc : 'left';
  firstLine_ind = firstLine_ind ? firstLine_ind : '0';
  right_ind = right_ind ? right_ind : '0';
  left_ind = left_ind ? left_ind : '0';
  tab_list = tab_list ? tab_list : ['1440'];
  before = before ? before : '0';
  after = after ? after : '0';
  line = line ? line : '360';
  keep = keep_w_next ? '<w:keepNext/>' : '';
  num_style_id = num_style_id ? num_style_id : '1';
  num_id = exports.w_leaf('numId',{'val':num_style_id});
  num_pr = exports.w_node('numPr',exports.w_leaf('ilvl',{'val':'0'}) + num_id);  
  numbering_markup = numbering ? num_pr : '';
  content += (numbering ? '<w:pStyle w:val="ListParagraph"/>' : '')
  	+ keep 
  	+ exports.w_leaf('jc', {'val': jc}) 
  	+ exports.w_leaf('ind', {'firstLine': firstLine_ind, 'right': right_ind, 'left': left_ind}) 
  	+ exports.w_leaf('spacing', {'before': before, 'after': after, 'line': line, 'lineRule': 'auto'}) 
    + exports.w_leaf('contextualSpacing', {'val' : 0} )
  	+ exports.tabs(tab_list) 
  	+ numbering_markup;
  return exports.w_node('pPr', content);
}

exports.injectRun = function(run){
  return '</w:t></w:r>' + run + '<w:r><w:t xml:space="preserve">';
}

exports.injectMarkup = function(markup){
  return '</w:t></w:r></w:p>' + markup + '<w:p><w:r><w:t>';
}

exports.r = function(text, prop_obj, tab, br){
  var properties = prop_obj ? exports.rPr(prop_obj.bold, prop_obj.italics, prop_obj.underline, prop_obj.caps, prop_obj.small_caps, prop_obj.color, prop_obj.size,prop_obj.strike, prop_obj.r_fonts) : '';
  var content = exports.w_node('t', text, null, true);
  tab = tab ? exports.w_leaf('tab') : '';
  return exports.w_node('r', tab + properties + content + (br ? '<w:br/>' : '') );
}

exports.rPr = function(bold, italics, underline, caps, small_caps, hex_color, size, strike, r_fonts){
  var properties = size ? exports.w_leaf('sz', {'val': size}) : exports.w_leaf('sz', {'val': '24'});
  if (italics) {properties += exports.w_leaf('i')};
  if (caps) {properties += exports.w_leaf('caps', {'val': caps})}
  if (small_caps) {properties += exports.w_leaf('b', {'val': small_caps})}
  if (bold) {properties += exports.w_leaf('b')}
  if (hex_color) {properties += exports.w_leaf('color', {'val': hex_color})}  
  if (underline) {properties += exports.w_leaf('u', {'val': 'single'})}
  if (strike) {properties += exports.w_leaf('strike', {'val': strike})}
  if (r_fonts) {properties += exports.w_leaf('rFonts',{'ascii':r_fonts,'hAnsi':r_fonts})}
  return exports.w_node('rPr', properties)
} 

exports.tabs = function(tab_list){
  var tab_content = "";
  for (var i = 0; i < tab_list.length; i++){
    tab_content += exports.w_leaf('tab', {'val': 'left', 'pos': tab_list[i]})
  }
  return exports.w_node('tabs', tab_content)
}

exports.refSource = function(type, number){
	var id = type.charCodeAt(0) ? (type.charCodeAt(0)*10 + number)  : number;
	var bold = true;
	if(type == 'Claim'){
		bold = false;
	}
  var content = exports.bookmark(id, '_' + type + number, exports.r('#', {bold: bold}));
  return exports.w_node('fldSimple', content, {'instr': 'SEQ ' + type + ' \\\\* MERGEFORMAT'})
}

exports.refTarget = function(type, number){
  var content = exports.r('#', {bold: false});
  return exports.w_node('fldSimple', content, {'instr': 'REF _' + type + number + ' \\\\* CHARFORMAT'});
}

exports.bookmark = function(id, name, content){
  return exports.w_leaf('bookmarkStart', {'id': id, 'name': name}) + content + exports.w_leaf('bookmarkEnd', {'id': id});
}

exports.w_node = function(tag_name, content, attr_obj, space_preserve){
  space_preserve = space_preserve ? ' xml:space="preserve"' : '';
  return '<w:' + tag_name + ' ' + exports.attributes(attr_obj) + space_preserve + '>' + content + '</w:' + tag_name + '>';
}

exports.w_leaf = function(tag_name, attr_obj, ref){
    var ref_id = '';
    if (ref) {ref_id = ' r:id="' + ref + '"';}
    return '<w:' + tag_name + ' ' + exports.attributes(attr_obj) + ref_id +'/>';
}

exports.attributes = function(attr_obj){
  var attr_str = '';
  for (var attr in attr_obj){
      attr_str += ' w:' + attr + '="' + attr_obj[attr] + '"';
  }
  return attr_str;
}

exports.sectPr = function(lnNumType_obj, next_page, header_ref, footer_ref, pgSz_obj, pgMar_obj){
  var section_properties = '';
  if(next_page){
    section_properties += exports.w_leaf('type', {'val': 'nextPage'});
  }else{
    section_properties += exports.w_leaf('type', {'val': 'continuous'});
  }
  if(header_ref){
    section_properties += exports.w_leaf('headerReference', {'type': 'default'}, header_ref);
  }
  if(footer_ref){
    section_properties += exports.w_leaf('footerReference', {'type': 'default'}, footer_ref);
  }
  if(pgSz_obj){
    section_properties += exports.w_leaf('pgSz', pgSz_obj);
  }
  if(pgMar_obj){
    section_properties += exports.w_leaf('pgMar', pgMar_obj);
  }
  if(lnNumType_obj){
    section_properties += exports.w_leaf('lnNumType', lnNumType_obj);
  }
  return exports.w_node('sectPr', section_properties);
}
