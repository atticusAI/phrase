 _____  _
|  __ \| |
| |__) | |__  _ __ __ _ ___  ___ 
|  ___/| '_ \| '__/ _` / __|/ _ \
| |    | | | | | | (_| \__ \  __/
|_|    |_| |_|_|  \__,_|___/\___| 
the evolution of the phrase-sever project

/////////////////////////////////// SETUP //////////////////////////////////////

Steps for running the phrase server locally

1) Install NodeJS
  https://nodejs.org/en/download/

2) Install dependencies
  In your terminal, navigate this folder and run "npm install"

3) Start your local server
  In the same terminal window run "node index.js"
