
exports.removeExtraSpaces = function(str){
  if(str && typeof str == "string"){
    return str.replace(/\s+/g,' ').trim();
  }else{
    
  }
}

// test if the obj is an array
exports.isArray = function(obj) {
  return Object.prototype.toString.apply(obj) === '[object Array]';
}

// return items formatted with list language, but preserves ":" at the end of a list item
// ex: [a,b,c] -> ["a," "b, and" c"]
exports.addListLanguage = function(list = [], separator = ',', or = false, return_list = false){
  var ret_list = [];
  if(list.length == 1){
    ret_list.push(list[0])
  }else{
    for(var i = 0; i < list.length; i++){
      if (i == list.length - 2 && list[i].indexOf('; and') < 0){
      	var join_text = (list.length > 2 ? separator : ';') + (or ? ' or ' : ' and '); 
        ret_list.push(list[i] + join_text);
      } else if(i < list.length - 1 && list[i].indexOf('; and') < 0){
      	var join_text = separator + ' ';
        ret_list.push(list[i] + join_text);
      } else {
        ret_list.push(list[i]);
      }
    }
  }
  
  return ret_list;

}

// return items combined into a single string using standard list formatting, but preserves ":" at the end of a list item
// ex: [a,b,c] -> "a, b, and c"
exports.writeList = function(list = [], separator = ',', or = false){
  var list_text = "";
  if(list.length == 1){
    list_text = list[0];
  }else{
    for(var i = 0; i < list.length; i++){
      list_text += list[i];
      if (i == list.length - 2){
        var join_text = (list.length > 2 ? separator : '') + (or ? ' or ' : ' and '); 
        list_text += join_text;
      } else if(i < list.length - 1){
        var join_text = separator + ' ';
        list_text += join_text;
      }
    }
  }
  return list_text;
}

// takes in a numer range, and returns a list of all number in range
// ex: "2-5" -> [2,3,4,5]
exports.breakDownNumberRange = function(string = ""){
	var number_list = [];
	var pieces = string.split('-');
	var first = Number(pieces[0]);
	var last = Number(pieces[pieces.length - 1]);
	
	for (var k = first; k <= last; k++){
		if (number_list.indexOf(k) < 0) number_list.push(k);
	}
	return number_list;
}

// takes an array of number ranges and returns a single list of each number that occured
// also sorts the returned list of numbers
// ex: ["5","3-5","1"] -> [1,3,4,5]
exports.breakDownNumbers = function(list){
	if (!list){
    return ['NONE'];
  }
	if (!exports.isArray(list)){
    list = [list];
  }
  var separated_list = [];
  for (var i = 0; i < list.length; i++){
  	var sublist
    if(typeof list[i] == 'string'){
      var clean_list = list[i].replace(/\[/g, '').replace(/\]/g, '').replace(/, and/g, ',').replace(/and/g, ',');
      sublist = clean_list.split(',');
    }else{
      sublist = [list[i]];
    }
  	if (!exports.isArray(sublist)){
      sublist = [sublist];
    }
	  for (var j = 0; j < sublist.length; j++)  {
  		var group = sublist[j];
    	if (typeof group == 'string' && group.indexOf('-') > -1){
    		var number_list = exports.breakDownNumberRange(group);
    		number_list.forEach(function(entry){
    			var number_entry = parseInt(entry);
    			if (separated_list.indexOf(number_entry) < 0) separated_list.push(number_entry);
    		})
    	}
    	else if (parseInt(group)){
    		var number_entry = parseInt(group);
    		if (separated_list.indexOf(number_entry) < 0) separated_list.push(number_entry);
    	}
  	}
  }
	separated_list.sort(function(a, b) {return a - b;});
	return separated_list;
}

// takes a list of number ranges and combines them into a list string format.
// ex: ["5","3-5","1","7"] -> "1, 3-5, and 7"
exports.writeNumbers = function(list = []){
  list = exports.breakDownNumbers(list);
  var text = '';
  var groups = [];
  var group_count = -1;
  for (var i = 0; i < list.length; i++) {
    var prev2 = list[i - 2];
    var prev = list[i - 1];
    var cur = list[i];
    var next = list[i + 1];
    if (!prev || prev != cur - 1 || (prev2 != cur - 2 && next != cur + 1)){
      group_count = group_count + 1;
      groups[group_count] = list[i];
    }else if (prev == cur - 1 && cur != next - 1) {
      groups[group_count] = groups[group_count] + "–" + list[i];
    }
  }
  for (var i = 0; i < groups.length; i++) {
    if (i == 0) 											text = groups[i];
    else if (i == groups.length - 1 && groups.length > 2) text += ", and " + groups[i];
    else if (i == groups.length - 1) text += " and " + groups[i];
    else text += ", " + groups[i];
  }
  if (typeof text != 'string') text = text.toString();
  return text;
}

// removes end-of-feature punctuation
exports.removeFeaturePunctuation = function(text){
  
  if (typeof text == 'undefined'){
    return text;
  }

  if (['.', ';', ':'].indexOf(text.slice(-1)) >= 0){
    return text.slice(0, -1);
  } else if (['; and', ', and'].indexOf(text.slice(-5)) >= 0){
    return text.slice(0, -5);
  } else {
    return text
  }
}