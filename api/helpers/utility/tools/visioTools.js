const XML_BUILDER = require('xmlbuilder');
const VIEWS = require('../views/visio');
const config = require('../../config');

exports.VisioTools = function () {
    let _gid = 1;

    class View {
        constructor(templateName) {
            this.templateName = templateName;
        }

        build(context) {
            return VIEWS[this.templateName].call(context);
        }
    }

    class Size {
        constructor(width = 0, height = 0) {
            this.width = width;
            this.height = height;
        }
    }

    class Point {
        constructor(x = 0, y = 0) {
            this.x = x;
            this.y = y;
        }
    }

    class Spacing {
        constructor(space) {
            this.top = space.top;
            this.bottom = space.bottom;
            this.left = space.left;
            this.right = space.right;
        }
    }

    class Margins extends Spacing {}

    class Layout {
        constructor(size, margins, spacing) {
            this.width = size.width;
            this.height = size.height;
            this.margins = margins;
            this.spacing = spacing;
        }

        containerSize() {
            return new Size(
                this.width - (this.margins.left + this.margins.right),
                this.height - (this.margins.top + this.margins.bottom)
            );
        }
    }

    class Flow extends Layout {
        constructor(size = config.visio.flow.size,
            margins = config.visio.flow.margins,
            spacing = config.visio.flow.spacing) {
            super(size, margins, spacing);
            this.rowTop = this.height - (this.margins.top + this.spacing.top);
        }

        firstPoint(shape) {
            return new Point(
                this.margins.left + this.spacing.left,
                this.height - (this.margins.top + this.spacing.top + shape.height)
            );
        }

        fitsInRow(x, shape) {
            return -this.margins.left + x + shape.width + this.spacing.right <= this.containerSize().width;
        }

        nextX(shape) {
            return shape.x + this.spacing.left + shape.width + this.spacing.right + this.spacing.left;
        }

        leastY(shapes) {
            let y = this.height; // top of page/shape everything will be below this

            // find least y > 0
            (shapes || []).map(function (s) {
                y = s.y <= y && s.y > 0 ? s.y : y;
            });

            return y;
        }

        nextPoint(shapes, index) {
            const shape1 = shapes[index - 1];
            const shape2 = shapes[index];
            let x = this.nextX(shape1);
            let y = this.rowTop - shape2.height;
            if (!this.fitsInRow(x, shape2)) {
                this.rowTop = this.leastY(shapes) - this.spacing.bottom - this.spacing.top;
                x = this.margins.left + this.spacing.left;
                y = this.rowTop - shape2.height;
            } 
            return new Point(x, y);
        }

        // fit onto the page left to right top to bottom
        shapePosition(shapes) {
            var moves = [];
            shapes.map(s => {
                if (s.reposition) moves.push(s);
            });

            for (var s in moves) {
                if (s === '0') {
                    moves[s].bottomLeft = this.firstPoint(moves[s]);
                } else {
                    moves[s].bottomLeft = this.nextPoint(moves, s);
                }
            }
        }

        // each shape is responsible for it's own internal layout
        shapeLayout(shapes) {
            for (var s in shapes) {
                if (typeof shapes[s].layout === 'function') {
                    shapes[s].layout(this);
                }
            }
        }

        layout(shapes) {
            this.shapeLayout(shapes); // 
            this.shapePosition(shapes); // this provider just puts them on the page
        }
    }

    class Shape {
        constructor(view, x, y, w, h) {
            this.x = x;
            this.y = y;
            this.w = w;
            this.h = h;
            this.viewName = view;
            this.view = new View(view);
            this.shapes = [];
            this.id = _gid++;
            this.parentId = -1;
            this.reposition = true;
        }

        get width() {
            return this.w;
        }
        set width(w) {
            this.w = w;
        }

        get height() {
            return this.h;
        }
        set height(h) {
            this.h = h;
        }

        get halfWidth() {
            return this.w * 0.5;
        }
        get halfHeight() {
            return this.h * 0.5;
        }

        get size() {
            return new Size(this.w, this.h);
        }
        set size(sz) {
            this.w = sz.width;
            this.h = sz.height;
        }

        get bottomLeft() {
            return new Point(this.x, this.y);
        }
        set bottomLeft(bl) {
            this.x = bl.x;
            this.y = bl.y;
        }

        getShapes() {
            let s = [];
            this.shapes.map(function (v) {
                s.push(v.getShape());
            });
            return s.length > 0 ? {
                "Shapes": {
                    "Shape": s
                }
            } : null;
        }

        addShape(shape) {
            shape.parentId = this.id;
            this.shapes.push(shape);
        }

        getShape() {
            return this.view ? Object.assign(this.view.build(this), this.getShapes()) : {};
        }

        fitText(text) {
            const m = -0.0220;
            const b = 0.18;
            const cw = 0.11;
            let y = m * text.length + b;
            return text.length * cw + y;
        }
    }

    class Group extends Shape {
        constructor(x = 0, y = 0, w = 2.0, h = 2.0) {
            super('Group', x, y, w, h);
        }
    }

    class LabelConnector extends Shape {
        constructor() {
            super('LabelConnector');
            this.reposition = false;
        }
    }

    class Label extends Shape {
        constructor(text = '', x = 0, y = 0) {
            super('Label', x, y);
            this.text = text;
            this.reposition = false;
        }

        layout(lo) {
            this.x = config.visio.label.position.x;
            this.y = config.visio.label.position.y;
            this.w = config.visio.label.size.width;
            this.h = config.visio.label.size.height;
        }
    }

    class Page extends Shape {
        constructor(w = 8.5, h = 11) {
            super('Page', 0, 0, w, h);
            this.layoutProvider = null;
            this.reposition = false;
        }

        getPage() {
            return {
                "PageContents": this.getShape()
            };
        }

        toXML() {
            return XML_BUILDER.create(this.getPage(),
                config.xmlbuilder.head || {
                    version: '1.0',
                    encoding: 'UTF-8'
                },
                config.xmlbuilder.options || {
                    pubID: null,
                    sysID: null,
                    headless: true
                },
                config.xmlbuilder.skips || {
                    skipNullNodes: true,
                    skipNullAttributes: true
                }
            ).end(config.xmlbuilder.end || {
                pretty: false
            });
        }

        layout() {
            if (this.layoutProvider) {
                this.layoutProvider.layout(this.shapes);
            }
        }
    }

    class Rectangle extends Shape {
        constructor(text = '', x = 0, y = 0, w = 1.5, h = 1.0) {
            super('Rectangle', x, y, w, h);
            this.text = text;
            this.linePattern = 1;
            this.verticalAlign = 1;
        }

        get name() {
            return 'Rectangle.' + this.id;
        }

        layout() {
            let w = this.fitText(this.text);
            this.width = w < config.visio.rectangle.minimum.width ? config.visio.rectangle.minimum.width : w;
        }
    }

    class DeviceArrow extends Shape {
        constructor() {
            super('DeviceArrow');
            this.reposition = false;
        }

        layout() {}
    }

    class LabeledRectangle extends Group {
        constructor(label, text) {
            super();
            this.connector = new LabelConnector();
            this.component = new Rectangle(text);
            this.label = new Label(label);
            this.addShape(this.connector);
            this.addShape(this.component);
            this.addShape(this.label);
        }

        /*
        get height() {
            return this.component.height +  this.connector.height + this.label.height;
        }
        */

        layout(lo) {
            var wr = 0;
            var hr = lo.spacing.bottom;
            var br = lo.spacing.bottom*2 + config.visio.label.size.height;
            // layout children skip first three
            for(var i = 3; i < this.shapes.length; i++) {
                this.shapes[i].layout(lo);
                wr = lo.spacing.left + this.shapes[i].width + lo.spacing.right > wr ?
                    lo.spacing.left + this.shapes[i].width + lo.spacing.right : 
                    wr;
                hr = hr + lo.spacing.bottom + this.shapes[i].height + lo.spacing.top;
                this.shapes[i].x = lo.spacing.left;
                this.shapes[i].y = br;
                br = br + lo.spacing.bottom + this.shapes[i].height + lo.spacing.top;
            }
            this.component.verticalAlign = this.shapes.length > 3 ? 0 : 1; // top if has children  else center
            this.component.layout(lo);
            this.component.width = this.component.width > wr ? this.component.width : wr;
            this.width = this.component.width;
            this.component.height = hr > config.visio.rectangle.minimum.height ? hr : config.visio.rectangle.minimum.height;
            this.height = this.component.height + config.visio.label.size.height + lo.spacing.bottom + lo.spacing.top;
            this.component.x = this.halfWidth;
            this.component.y = this.halfHeight + config.visio.label.size.height;
            this.label.layout(lo);
        }

        addChildren(children) {
            if (Array.isArray(children) && children.length > 0){
                children.map(c => {
                    var r = new LabeledRectangle(c.text, c.component_name);  
                    r.addChildren(c.tchildren);
                    this.addShape(r);
                });
            }
        }
    }

    class DeviceLabel extends Group {
        constructor(label, page) {
            super();
            this.reposition = false;
            this.label = new Label(label);
            this.arrow = new DeviceArrow();
            this.addShape(this.label);
            this.addShape(this.arrow);
            this.page = page;
        }

        layout(provider) {
            this.label.width = 1.5;
            this.label.height = 1;
            this.width = this.label.width;
            this.height = this.label.height;
            this.label.x = this.width * 0.5;
            this.label.y = this.height * 0.5;
            this.label.linePattern = 0;
            this.x = provider.width * 0.75;
            this.y = this.height * 0.5 + provider.margins.bottom;
        }
    }

    class FigureLabel extends Shape {
        constructor() {
            super("FigureLabel");
            this.reposition = false;
            this.width = 0.95;
            this.height = 0.25;
        }

        get name() {
            return 'FigureLabel.' + this.id;
        }

        get charSize() {
            return config.visio.figure_label.char_size;
        }

        layout(provider) {
            this.x = provider.width * 0.5;
            this.y = this.height * 0.5 + provider.margins.bottom;
        }
    }

    // exports
    this.Shape = Shape;
    this.Label = Label;
    this.FlowLayout = Flow;
    this.Group = Group;
    this.Page = Page;
    this.Rectangle = Rectangle;
    this.LabeledRectangle = LabeledRectangle;
    this.FigureLabel = FigureLabel;
    this.DeviceLabel = DeviceLabel;
};