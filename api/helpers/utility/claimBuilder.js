const MOMENT = require('moment');
const TZ = require('moment-timezone');

const G_TOOLS = require("./tools/genericTools.js");
const B_TOOLS = require("./tools/blockDescriptionTools.js");
const S_TOOLS = require("./tools/stringTools.js");
const X_TOOLS = require('./tools/xmlTools.js');
const CLAIM_TOOLS = require("./tools/claimTools.js");
const CLAIM_GEN = require("./claimGenerators.js");
const REJ_BUILDER = require("./rejectionBuilder.js");
const LANG = require("./../language/claimLanguage.js");

const NUM_PAR_PROP = {
  numbering: true,
  tabs: [1080],
  before: 160,
  line: 360,
}

exports.appClaimFormat = function(claim_list = [], use_old_A4){
  // console.log(JSON.stringify(claim_list));
  var markup_string = "";
  claim_list = claim_list.filter(c=>{
    return c.claim_type != undefined;
  })
  for(c in claim_list){
    claim = claim_list[c];
    //// Preamble Format
    var claim_number = X_TOOLS.refSource('Claim', claim.item_num);
    var claim_preamble = claim.claim_preamble;
    var single_feature = false;

    if(claim.claim_features != undefined){
      claim.claim_features = claim.claim_features.filter(f=>{
        return f.feature_text != undefined;
      })
    }

    // If this is an apparatus claim, break out its components
    var markedup_app_elements = '';
    if(claim.claim_type == 'app' && claim.independent == true){
      claim_preamble = claim_preamble.slice(0, claim_preamble.indexOf(',')+ 12) + ':';
      app_elements = ['a processor,', 'memory in electronic communication with the processor; and', 'instructions stored in the memory and executable by the processor to cause the apparatus to:']
      for (var ae in app_elements){
        markedup_app_elements += X_TOOLS.p(X_TOOLS.r(app_elements[ae]), {before: '0', after: '0', line: '360', firstLine_ind: '1440'});
      }
    }

    // Check to see if a "preamble-able phrase" such as "XYZ comprises:" appears in a single feature
    if(claim.claim_features.length > 0 && claim.claim_features[0].feature_text != undefined){
      var idx = claim.claim_features[0].feature_text.indexOf("comprises: ");
      if(idx > 0){
        claim_preamble = claim_preamble.replace("wherein:", "wherein");
        claim_preamble += ' ' + claim.claim_features[0].feature_text.slice(0, idx+11);
        claim.claim_features[0].feature_text = claim.claim_features[0].feature_text.slice(idx+11);
      }
    } else {
      if (claim_preamble.slice(-1) != '.'){
        claim_preamble += '.';
      }
      single_feature = true;
    }

    // Try to put our claims in a standard form, in case users have made edits on the front end.
    // This will help the generators below.
    if (claim.claim_features && claim.claim_features.length == 1 &&
        claim_preamble.indexOf("wherein") >= 0 &&
        !CLAIM_GEN.activeFeature(claim.claim_features[0].feature_text) &&
        claim_preamble.indexOf("cause") < 0 &&
        claim_preamble.indexOf("further executable") < 0){

      claim_preamble = claim_preamble.replace("wherein:", "wherein");
      claim_preamble = claim_preamble.replace("comprises:", "comprises");
      single_feature = true;
    }

    if ((claim_preamble.slice(-7) == 'wherein' || claim_preamble.slice(-9) == 'comprises') && !single_feature){
      claim_preamble += ':';
    }

    claim_preamble = X_TOOLS.r(G_TOOLS.removeExtraSpaces(claim_preamble), null, true);
    if(!claim.independent){
      var parent_marker = claim_preamble.match(/(of claim )[0-9]+/);
      if(parent_marker && parent_marker.length > 0){
        var parent_ref = X_TOOLS.refTarget('Claim', parent_marker[0].match(/[0-9]+/)[0]);
        claim_preamble = claim_preamble.replace(/(of claim )[0-9]+/, 'of claim '+X_TOOLS.injectRun(parent_ref));
      }
    }

    var preamble_markup = claim_number + X_TOOLS.r('.') + claim_preamble;

    var markup_strings = [];
    // markup_string += X_TOOLS.p(preamble_markup, {before: '240', after: '0', line: '360', firstLine_ind: '1440'})
    //// Feature Format
    for(f in claim.claim_features){
      var feature = claim.claim_features[f];
      if(feature.feature_text){
        var clean_text = G_TOOLS.removeExtraSpaces(G_TOOLS.removeFeaturePunctuation(feature.feature_text));
        // markup_string += X_TOOLS.p(X_TOOLS.r(clean_text), {before: '0', after: '0', line: '360', firstLine_ind: '1440'});
        markup_strings.push(clean_text);
      }
      for(var fc in feature.feature_components){
        if(feature.feature_components[fc].component_text){
          var clean_comp = G_TOOLS.removeExtraSpaces(G_TOOLS.removeFeaturePunctuation(feature.feature_components[fc].component_text));
          // markup_string += X_TOOLS.p(X_TOOLS.r(clean_comp), {before: '0', after: '0', line: '360', firstLine_ind: '1440'});
          markup_strings.push(clean_comp);
        }
      }
    }

    var feature_indent = '0';
    if(markedup_app_elements != ''){
      feature_indent = '720'
    }

    // Do not use too many paragraphs unless needed
    if (single_feature){
      if (markup_strings.length > 0){
        preamble_markup = preamble_markup + X_TOOLS.r(" "+ markup_strings[0]+'.');
      }
      markup_string += X_TOOLS.p(preamble_markup, {before: '240', after: '0', line: '360', firstLine_ind: '1440', contextualSpacing: {val : 0}});
      for (var ms = 1; ms < markup_strings.length; ms++){
        markup_string += X_TOOLS.p(X_TOOLS.r(markup_strings[ms].trim()), {before: '0', after: '0', line: '360', firstLine_ind: '1440'});
      }
    } else {
      markup_string += X_TOOLS.p(preamble_markup, {before: '240', after: '0', line: '360', firstLine_ind: '1440', contextualSpacing: {val : 0}});
      markup_string += markedup_app_elements;
      markup_strings = G_TOOLS.addListLanguage(markup_strings, separator = ';', or = false);
      markup_strings[markup_strings.length-1] += '.';
      for (var ms in markup_strings){
        markup_string += X_TOOLS.p(X_TOOLS.r(markup_strings[ms].trim()), {before: '0', after: '0', line: '360', firstLine_ind: '1440', left_ind: feature_indent});
      }
    }

    // Section Break; note that templates created with versions of Word prior to 2016
    // defined A4 to be a slightly different size (c'mon Microsoft)
    if(c < claim_list.length-0){
      if(use_old_A4 == true) {
        markup_string += X_TOOLS.p('', false, {lnNumType: {countBy: '1', restart: 'newSection'},
                                               pgSz: {w: "11907", h: "16839"} } )
      } else {
        markup_string += X_TOOLS.p('', false, {lnNumType: {countBy: '1', restart: 'newSection'},
                                               pgSz: {w: "11909", h: "16834"} } )
      }
    }
  }

  return markup_string;
}

exports.oaClaimFormat = function(claim_list = []){
  // console.log(JSON.stringify(claim_list));
  var markup_string = "";
  var feature_components_present = false;
  
  CLAIM_TOOLS.combineCanceledClaims(claim_list);
  for(c in claim_list){
    claim = claim_list[c];
    feature_components_present = false;
    //// Preamble Format
    var claim_number = X_TOOLS.r(claim.item_num + '.');
    var claim_preamble = "";
    if(!claim.status || claim.status.toLowerCase() != "canceled"){
      claim_preamble = X_TOOLS.r(G_TOOLS.removeExtraSpaces(claim.claim_preamble));
    }
    var claim_status = X_TOOLS.r('('+claim.status+') ', null, true);
    var preamble_markup = claim_number + claim_status + claim_preamble;
    var markup_strings = [];
    //// Feature Format
    for(f in claim.claim_features){
      var feature = claim.claim_features[f];
      if(feature.feature_text){
        var clean_text = G_TOOLS.removeExtraSpaces(feature.feature_text);
        markup_strings.push(clean_text);
      }
      for(var fc in feature.feature_components){
        if(feature.feature_components[fc].component_text){
          var clean_comp = G_TOOLS.removeExtraSpaces(feature.feature_components[fc].component_text);
          markup_strings.push(clean_comp);
          feature_components_present = true;
        }
      }
    }
    /// Use only a single .p if only one feature AND not "comprising:", otherwise one for each feature.
    if (claim.claim_features.length == 1 && !(feature_components_present) && claim.claim_preamble.indexOf(":") < 0){
      preamble_markup = preamble_markup + X_TOOLS.r(" "+ markup_strings[0]);
      markup_string += X_TOOLS.p(preamble_markup, {before: '240', after: '0', line: '360', firstLine_ind: '1440'});
    } else {
      markup_string += X_TOOLS.p(preamble_markup, {before: '240', after: '0', line: '360', firstLine_ind: '1440'});
      for (var ms in markup_strings){
        markup_string += X_TOOLS.p(X_TOOLS.r(markup_strings[ms]), {before: '0', after: '0', line: '360', firstLine_ind: '1440'});
      }
    }
  }
  return markup_string;
}


exports.appClaimSummary = function(claim_list, spec, claim_types, micronMPF){

  var summary = '';
  var summary_style = 'N-IC';
  // var claim_types = ['method', 'app', 'mpf', 'crm']
  var independent_paragraphs = '';
  
  // Generate summary for all claim types, but based only on the method claims
  claim_list = claim_list.filter(c=>{
    return c.claim_type == 'method';
  });

  claim_list.forEach(function(claim){

    if (claim.claim_device != undefined){
      device = claim.claim_device.device_name || 'generic';
      device = device.replace(',', ' or ') || 'undefined';
    } else {
      device = 'undefined'
    }

    if (claim.independent == true){

      simple_preamble = CLAIM_TOOLS.extractSimplePreamble(claim.claim_preamble);

      if (simple_preamble.length > 0){
        simple_preamble = ' for ' + simple_preamble;
      }

      claim_types.forEach(function(type){
        if (type == 'method'){
          var v = 'method';
        } else if (type == 'mpf' || type == 'app') {
          var v = 'apparatus';
        }
        var claim_info = LANG.claim_type_list[type] || {};
        var summary_intro = (summary_style == 'IC') ? claim_info.alt_summary_intro : claim_info.summary_intro;
        // if (claim_types.length == 1 && claim_types[0] == 'mpf'){
        if (micronMPF !== undefined){
          var fig_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', micronMPF))
          // This is the MPF support specific to a Micron application
          // summary_intro = 'An apparatus for performing the method is described. The apparatus may include means for'
          summary_intro = `In some examples, an apparatus as described herein may perform a method or methods, such as the method ${fig_label}00. The apparatus may include features, means, or instructions (e.g., a non-transitory computer-readable medium storing instructions executable by a processor) for`;
        }
        var claim_type = claim_info.noun || LANG.claim_type_list['default'].noun;

        var par_text = summary_intro.replace('{DEVICE}', device).replace('{PREAMBLE}', simple_preamble) + ' ';
        par_text = par_text.replace(', comprising:', '')
                           .replace('{V}', v)

        var features = claim.claim_features;
        par_text += G_TOOLS.writeList(features.map(function(feature){
          if (type == 'method' || type == 'mpf'){
            if(type == 'mpf' && micronMPF === undefined){
              means = 'means for ';
            }else{
              means = '';
            }
            return feature.feature_text ? ( means + S_TOOLS.gerundSentence(G_TOOLS.removeFeaturePunctuation(feature.feature_text)) ) : ' ';
          } else {
            return feature.feature_text ? S_TOOLS.infinitiveSentence(G_TOOLS.removeFeaturePunctuation(feature.feature_text)) : ' ';
          }
        }) ) + '.';

        summary += X_TOOLS.p( X_TOOLS.r(par_text), NUM_PAR_PROP);
      })

    } else {
      text = dependentSummaryPara(claim, claim_types, summary_style, device, micronMPF);
      summary += text;
    } 
  
  })
  summary = S_TOOLS.simplify(summary);
  
  // A weird fix required to get proper numbering format in the Claim Summary template
  // The better thing to do is update the template, though I'm not sure how (gm).
  // Note that we don't do this if the claim summary is being dumped into the specification
  if (spec == false){
    summary = summary.replace(/numId  w:val="1"/g, 'numId w:val="33"');
  }

  return summary;
}

exports.micronMPFSupport = function(claim_pair){

}

exports.oaClaimSummary = function(patent_office_action){
  // console.log(JSON.stringify(patent_office_action));
  var markup_string = "";
	if (patent_office_action.oa_type === 'Restriction'){
    return "";
	}else{
  	return X_TOOLS.p(X_TOOLS.r("In the "+S_TOOLS.actionType(patent_office_action.oa_type)+' Office Action dated '+
      MOMENT(patent_office_action.oa_date, "YYYY-MM-DD").format("MMMM D, YYYY")+' (“Office Action”): '+
      G_TOOLS.writeList(REJ_BUILDER.rejectionList(patent_office_action), ";", false)+
      ". Applicant requests reconsideration."), {firstLine_ind: [720], numbering: false, tabs: [1080], before: 160, line: 360});
  }
}

exports.restrictionClaimSummary = function(patent_office_action){
	  var groups = []
    var g_ids = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'];
    patent_office_action.oa_rejections.forEach(function(rej){
    	groups.push(G_TOOLS.writeNumbers(rej.reject_claims));
    })

    var text = X_TOOLS.p(X_TOOLS.r( "The Office Action requested restriction of the claims to the following groups: ",{},false,false))
    for (var f in groups.slice(0,-1)){
    	text += X_TOOLS.p(X_TOOLS.r( String.fromCharCode(8226) + ` Group ${g_ids[f]} 	${groups[f]}; and `,{},true,false))
    }
    f = groups.length-1;
    text += X_TOOLS.p(X_TOOLS.r( String.fromCharCode(8226) + ` Group ${g_ids[f]} 	${groups[f]}.`,{},true,false))
    text += X_TOOLS.p(X_TOOLS.r(" In response, Applicant hereby elects claims ________________ of Group _____, with/without traverse.",{},true,false))

    return text;
}


function dependentSummaryPara(claim, type_in, summary_style, device, micronMPF){

  types = type_in;
  var dep_text = '';
  var dependent_intro = {active:'Some examples may further include ', passive:'In some examples, ',};

  var type_list = [];
  types.forEach(function(t){
    var noun = LANG.claim_type_list[t].noun;
    if (type_list.indexOf(noun) < 0){
      type_list.push(noun);
    } else if (noun == 'apparatus') {
      type_list[ type_list.indexOf('apparatus') ] = 'apparatuses'
    }
  })
  type_list = G_TOOLS.writeList(type_list);
  // var type_list = G_TOOLS.writeList( types.map(function(type) {return LANG.claim_type_list[type].noun;}) );
  
  if (summary_style == 'IC' && types.length == 1 && types[0] == 'app') 
    dependent_intro = {active: 'The instructions, when executed, may cause the processor to ', passive: '',};
  else if (summary_style == 'IC' && types.length == 1 && types[0] == 'method') 
    dependent_intro = {active: 'The method may also include ', passive: '',};
  else if (types.indexOf('method') > -1) 
    dependent_intro = {active:'Some examples of the ' + type_list + ' described herein may further include operations, features, means, or instructions for ', passive:'In some examples of the ' + type_list + ' described herein, ',}
  else if (summary_style == 'IC' && types.indexOf('dev') > -1) 
    dependent_intro = {active: 'The ' + device + ' may also include ', passive: '',}
  else if (types.indexOf('dev') > -1  || types.indexOf('form') > -1 || types.indexOf('sys') > -1) 
    dependent_intro = {active: 'Some examples of the ' + device + 'or system described herein may also include ', passive: 'In some examples of the ' + device + ' described herein, ',}
  else if (micronMPF !== undefined){
    var fig_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', micronMPF))
    dependent_intro = {active: `Some examples of the method ${fig_label}00 and the apparatus described herein may further include operations, features, means, or instructions for `, passive: `In some examples of the method ${fig_label}00 and the apparatus described herein, `}
  }

  var type = types[0] || 'method';
  var features = claim.claim_features || [];
  var specifying_preamble = claim.claim_preamble.indexOf('wherein') > 0 && claim.claim_preamble.indexOf('comprises') > 0;

  for(f in features){
    feature = features[f];
  
    var value = G_TOOLS.removeFeaturePunctuation(feature.feature_text) || '';
    
    if(f == 0 && specifying_preamble == true){
      active = false;
      value = CLAIM_TOOLS.extractComplexPreamble(claim.claim_preamble).slice(0, -11) +
              ' may include operations, features, means, or instructions for ' + value;
    } else if (claim.claim_preamble.indexOf('wherein') >= 0 &&
               CLAIM_GEN.actionComprisingAction(value, false, 'summary')){
      active = false;
      value = CLAIM_GEN.actionComprisingAction(value, true, 'summary');
    } else if ( (f == 0 && specifying_preamble == false) || f > 0) {
      active = B_TOOLS.activeFeature(value, type);
    }
    
    var intro = active ? dependent_intro.active : dependent_intro.passive;
    var conjugated_value = (intro.indexOf('processor to') > -1) ? S_TOOLS.infinitiveSentence(value) : S_TOOLS.gerundSentence(value);
    if (f == 0) {
      dep_text += (active ? (intro + conjugated_value) : (intro + value) )
    } else {
      dep_text += value;
    }

    if (f == features.length-1) {
      dep_text += '. ';
    } else if (f == features.length-2) {
      dep_text += ', and ';
    } else {
      dep_text += ', '
    }

    // if (intro.length > 0) 
    //   dep_text += (active ? (intro + conjugated_value) : (intro + value) )  + '. ';
    // else 
    //   dep_text += S_TOOLS.capitalize(value) + '. ';
  }

  // This is probably a wherein-only claim (found often in Micron) summarize as such.
  if (dep_text == '' &&
      claim.features == undefined &&
      claim.claim_preamble.indexOf('wherein') > 0 &&
      claim.claim_preamble.indexOf('comprises') < 0){
    ix = claim.claim_preamble.indexOf('wherein')
    dep_text = dependent_intro.passive + claim.claim_preamble.slice(ix+8)
  }

  dep_text = dep_text
    .replace(/ is /g, ' may be ')
    .replace(/ are /g, ' may be ')
    .replace(/ have /g, ' may have ')
    .replace(/ has /g, ' may have ')
    .trim() + '.';
  var dep_par = X_TOOLS.p( X_TOOLS.r(G_TOOLS.removeFeaturePunctuation(dep_text)), NUM_PAR_PROP );

  // var dependents = claim.dep_claims || [];
  // dependents.forEach(function(dependent){dep_par = dep_par + summaryDependentParagraph(dependent, types);})
  if (dep_text == '') {
    return '';
  } else {
    return dep_par
  }
}


// Generate a table of claim numbers, description, and corresponding slides
exports.corrTable = function(claim_list){
  var column_widths = ['1200', '6500', '1200'];
  var column_headers = ['Claim', 'Description', 'Slide'];
  var table_data = [];

  claim_list.forEach(function(claim){
    var row = claimRow(claim);
    table_data = table_data.concat(row);
  })
 
  return CLAIM_TOOLS.wordTable(table_data, column_headers, column_widths);

}

function claimRow(claim){
  var features = claim.claim_features || [];
  var description = features.map(function(feature){
    var text = feature.feature_text || '';
    return  S_TOOLS.capitalize( S_TOOLS.simplify(feature.feature_text) );
  }).join('<w:br />');
  var slide = claim.claim_data.slide_num || '';
  var claim_row_data = [[claim.item_num, description, slide]];

  // var dependents = claim.dep_claims || [];
  // dependents.forEach(function(dependent){
  //   var row = claimRow(dependent, number)
  //   var dependent_data = row.data;
  //   number = row.number;
  //   claim_row_data = claim_row_data.concat(dependent_data);
  // })

  return claim_row_data;
}

