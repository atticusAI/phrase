// configurable logging
// 
const fs = require('fs');
const config = require('./config');


const levels = [ // order is significant
    'debug',
    'success',
    'info',
    'warn',
    'error',
    'none'
];

const colors = [ // order is significant
    "\x1b[34m", // blue -- debug
    "\x1b[32m", // green -- success
    "\x1b[37m", // white -- info
    "\x1b[33m", // yellow -- warn
    "\x1b[31m", // red -- error
];

const log = function (level, message, keyword) {
    if (levels.indexOf(level) >= levels.indexOf(config.log.level)) {
        if (typeof keyword === 'undefined' || config.log.keywords.length === 0) {
            write(level, message);
        }
        else if (config.log.keywords.indexOf(keyword) > -1){
            write(level, message);
        }
    }
};

const write = function(level, message){
    if (config.log.file){
        fs.appendFileSync(config.log.file, message + '\n');
    } else {
        if (config.log.colors) {
            console.log(colors[levels.indexOf(level)], message);
        } else {
            console.log(message);
        }
    }
};

if (config.log.file && config.log.truncate) {
    if (fs.existsSync(config.log.file)) {
        fs.truncateSync(config.log.file);
    }
}

exports.success = function (message, keyword) {
    log('success', message, keyword);
};

exports.debug = function (message, keyword) {
    log('debug', message, keyword);
};

exports.info = function (message, keyword) {
    log('info', message, keyword);
};

exports.warn = function (message, keyword) {
    log('warn', message, keyword);
};

exports.error = function (message, keyword) {
    log('error', message, keyword);
};