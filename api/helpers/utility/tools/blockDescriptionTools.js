const G_TOOLS = require('./genericTools.js');
const S_TOOLS = require('./stringTools.js');
const X_TOOLS = require('./xmlTools.js');
const TAGS = require('./tagTools');
const config = require('../../config');

const NUM_PAR_PROP = {
  numbering: true,
  tabs: [1080],
  before: 160,
  line: 360,
};

exports.cookDeviceGroup = function(device_group_raw) {
  var device_group = exports.tuplesToKeyValuePairs(device_group_raw);
  (device_group.blocka || {}).primary_component = exports.getComponentByTag(device_group.blocka, 'primary');
  (device_group.blockb || {}).primary_component = exports.getComponentByTag(device_group.blockb, 'primary');
  (device_group.blockc || {}).primary_component = exports.getComponentByTag(device_group.blockc, 'primary');
  (device_group.blockh || {}).primary_component = exports.getComponentByTag(device_group.blockh, 'primary');
  return device_group;
};


// get the first component where component_tags contains tag_name
exports.getComponentByTag = function(device, tag_name) {
  var cmp;
  if (device) {
    cmp = TAGS.findComponent(device.components || [], tag_name);
  }

  if (!cmp && config.defaults.components[tag_name] ) {
    cmp = config.defaults.components[tag_name];
  }

  return cmp;
};

// get the tag_description element of the for the appropriate tag name within component
exports.getTagDescription = function(component, tag_name){
  var desc;
  if (component){
    (component.component_tags || []).some(function(tag) {
      if (tag.tag_name === tag_name){
        desc = tag.tag_description;
        return true;
      } else {
        return false;
      }
    });
  }
  return desc;
};

exports.getAllTagDescriptions = function(component, tag_name){
  var descs = []
  component.component_tags.forEach(function(ct){
    if(ct.tag_name == tag_name){
      descs.push(G_TOOLS.removeFeaturePunctuation(ct.tag_description))
    }
  })
  return descs;
}

// get all components where component_tags contains tag_name
exports.getAllComponentsByTag = function(device, tag_name) {
  var cmps = [];
  var seen = [];
  if (device) {
    (device.components || []).map( (c) => {
      (c.component_tags || []).map( (t) => {
        if (t.tag_name === tag_name && seen.indexOf(c.component_name) < 0){
          cmps.push(c);
          seen.push(c.component_name);
        }
      });
    });
  }
  return cmps;
};

// Simple hashing of a string
exports.hash = function(str) {
  var hash = 0, i, chr;
  if (str.length === 0) return hash;
  for (i = 0; i < str.length; i++) {
    chr   = str.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return Math.abs(hash);
};

// converts an array of tuples into key values pairs
exports.getLabelList = function(label_group){
  var label_list = {};
  for(var l in label_group){
    label_list[label_group[l][0].toLowerCase()] = label_group[l][1];
  }
  return label_list;
};

// array of tuples to key value pairs
// same as above but refuses to process non array types
exports.tuplesToKeyValuePairs = function(tuple_list) {
  var lst = {};
  if (Array.isArray(tuple_list)) {
    tuple_list.map( (t) => {
      lst[t[0].toLowerCase()] = t[1];
    });
  }
  return lst;
};

exports.formComponentList = function(components = [], independents_only = false, include_dependents = false){
  
  if(components[0] == undefined){
    return [];
  }

  var selected_components = components.filter(c=>{
    return c.component_type == "independent";
  });

  if(!independents_only || include_dependents){ // remove dependent components that have the same name as an independent component 
    var dependent_components = components.filter(c=>{
      return c.component_type != "independent";
    });
    for(var i in selected_components){
      for(var d in dependent_components){
        if(selected_components[i].component_name == dependent_components[d].component_name){
          if(include_dependents){ // add dependent component description to independent component 
            selected_components[i].component_description = selected_components[i].component_description.concat(dependent_components[d].component_description);
          }
          dependent_components.splice(d, 1);
          d--;
        }
      }
    }
    if(!independents_only){ // merge independent and dependent components
      selected_components = selected_components.concat(dependent_components);
    }
  }

  
  selected_components.forEach(function(c){
    var update_c_text = []
    if (c.component_description){
      c.component_description.forEach(function(cd){
        update_c_text.push(G_TOOLS.removeFeaturePunctuation(cd));
      })
    }
    c.component_description = update_c_text;
  })

  return selected_components;
}


exports.getComponentsNameList = function(components = [], fls = 0, independent = false){
  if(independent){
    components = components.filter(c=>{
      has_independent = false;
      c.component_tags.forEach(function(t){
        if (t.feature_claim_type == true){
          has_independent = true;
        }
      })
      return has_independent;
    });
  }

  seen_names = [];
  unique_components = [];
  components.forEach(function(c){
    if(seen_names.indexOf(c.component_name) < 0 && c.component_name != "Bus"){
      seen_names.push(c.component_name)
      unique_components.push(c);
    }

  })

  return G_TOOLS.writeList(unique_components.map(c=>{
    if (typeof c.component_name === 'undefined') {
      return '';
    } else { 
      var article = 'a '
      if ( ['a', 'e', 'i', 'o', 'u'].indexOf(c.component_name.charAt(0).toLowerCase()) >= 0){
        article = 'an '
      }
      if (c.component_name == 'Memory' || c.component_name == 'memory'){
        article = ''
      }
      return article + S_TOOLS.unCapWords(c.component_name )+ ' ' + fls + c.text;
    }
  }), ",");
}

//// Write module descriptions

exports.writeDescriptionsList = function(components = []){
  var descriptions = components.map(val=>{
    return val.component_description;
  })
  var features = [].concat.apply([], descriptions);
  var feature_groups = exports.modualizeFeatures(features);
  return G_TOOLS.writeList(feature_groups.active_features)+'. '+feature_groups.passive_features.join(' ');
}

exports.writeDescriptionsParagraph = function(components = [], fig_label){
  var descriptions = "";
  for(var c in components){
    var ftext = '';

    var feature_groups = exports.modualizeFeatures(components[c].component_description);
    var features = feature_groups.active_features.concat(feature_groups.passive_features);

    // descriptions += X_TOOLS.p(X_TOOLS.r(`${S_TOOLS.capitalize(modules[m].module_name)} ${fig_label}${modules[m].text} may ${G_TOOLS.writeList(features)}.`), NUM_PAR_PROP);
    ftext += `The ${components[c].component_name} ${fig_label}${components[c].text} may ${G_TOOLS.writeList(feature_groups.active_features)}.`;
    if (feature_groups.passive_features.length > 0){
      feature_groups.passive_features.forEach(function(pf){
        ftext += ' ' + S_TOOLS.capitalize(pf) + '.'
      })
    }
    descriptions += X_TOOLS.p(X_TOOLS.r(ftext), NUM_PAR_PROP)
  }

  return descriptions;
}

exports.writeComponentParagraphs = function(components = [], fig_label = ''){
  var paragraphs = "";
  for (var c in components){
    var feature_groups = exports.modualizeFeatures(G_TOOLS.removeFeaturePunctuation(components[c].component_description));
    var features = feature_groups.active_features.concat(feature_groups.passive_features);
    // console.log(features)
    var ptext = `The ${components[c].component_name} ${fig_label}${components[c].text} may ` + feature_groups.active_features[0] + '.';
    paragraphs += X_TOOLS.p(X_TOOLS.r(ptext), NUM_PAR_PROP)

    var ftext = '';
    features.slice(1).forEach(function(af){
      if (af.indexOf('in some cases') >= 0){
        ftext = S_TOOLS.capitalize(af) + '.';
      }
      else if (af.indexOf('comprises') < 0) {
        ftext = `In some examples, the ${components[c].component_name} ${fig_label}${components[c].text} may ` + af + '.';
      } else {
        ftext = `In some examples, ${S_TOOLS.gerundSentence(af)}` + '.';
      }
      paragraphs += X_TOOLS.p(X_TOOLS.r(ftext), NUM_PAR_PROP);
    });
  }
  return paragraphs;
}

// categorizes and normalizes feature language
exports.modualizeFeatures = function(features = []){
  var active_features = [];
  var passive_features = [];
  for(var f in features){
    // var description = modules[m].module_description || "";
    if(features[f] && exports.activeFeature(features[f]) && features[f].length > 3){
      active_features.push(S_TOOLS.infinitiveSentence(features[f]));
    } else if (features[f] && features[f].length > 3){
      passive_features.push('in some cases, '+features[f]+'');
    }
  }
  if (active_features.length == 0){
    active_features.push('**FEATURE DETAIL**')
  }
  return {active_features, passive_features};
}

exports.activeFeature = function(text, type) {
  var words = text ? text.split(' ') : [''];
  var first_word = (typeof words[0] == 'string') ? words[0].toLowerCase() : '';
  if(type == 'dev' || type == 'form' || type == 'sys' || type == 'app' || type == 'ctr' || type == 'crm'){
    if(first_word != 'the' && first_word != 'each' && first_word != 'a' && first_word != 'an'){
      return true;
    }
  }
  if(first_word != '' && text.indexOf('es:') < 0 ){
    if(S_TOOLS.articles.indexOf(first_word) < 0 || first_word.length > 5){
      return true;
    }
  }
  return false;
}

exports.conjugateType = function(text, type){ 
  if (!exports.activeFeature(text, type)) return text;
  else if (type == 'method') return S_TOOLS.gerundSentence(text);
  else if ( type == 'mpf') return 'means for ' + S_TOOLS.gerundSentence(text);
  else if ( type == 'form') return 'forming ' + text;
  else return S_TOOLS.infinitiveSentence(text);
}

// Returns a list of components partitioned by independent claim
exports.partitionComponentsOld = function(component_list){
  component_partition = [];
  this_partition = [];
  if (component_list) {
    for(var c =0; c <= component_list.length -2; c++ ){
      if (component_list[c+1].component_type != component_list[c].component_type && component_list[c].component_type == 'dependent'){
        this_partition.push(component_list[c])
        component_partition.push(this_partition)
        this_partition = []
      } else{
        this_partition.push(component_list[c])
      }
    }
    this_partition.push(component_list[c]);
    component_partition.push(this_partition);
  }

  return component_partition;
}

exports.partitionComponents = function(component_list){
  // Establish a list of independent claim indices
  partitioned_components = [];
  if (component_list){
    ind_claims = [];
    claims_nums = [];
    component_list.forEach(function(c){
      c.component_tags.forEach(function(t){
        claims_nums.push(t.feature_claim_ref);
        if (t.feature_claim_type && ind_claims.indexOf({"ref":t.feature_claim_ref, "idx": t.feature_claim_index}) < 0){
          ind_claims.push({"ref": t.feature_claim_ref || 0, "idx": t.feature_claim_index || 0});
        }
      });
    });

    ind_claims = ind_claims.sort((a, b) => 
      (a.ref - b.ref) + (a.idx - b.idx)
    );

    ind_claims.push({"ref": Math.max(...claims_nums) + 1, "idx": 0});
    console.log(ind_claims);
    // For each independent claim, find the tags corresponding and append
    for(var i = 0; i < ind_claims.length; i++){
      this_ic_partition = [];
      component_list.forEach(function(c){
        c.component_tags.forEach(function(t){
          if (t.feature_claim_ref >= ind_claims[i].ref && t.feature_claim_ref < ind_claims[i+1].ref){
            this_ic_partition.push({component_description: [t.tag_description],
              component_name: c.component_name,
              text: c.text,
              component_type: t.feature_claim_type ? 'independent' : 'dependent'});
            }
        });
      });
      if (this_ic_partition.length > 0) {
        partitioned_components.push(this_ic_partition); // blmcgee 2018-09-11
      }
    }
  }
  // console.log(partitioned_components)
  return partitioned_components;
}

exports.aggregateComponents = function(components) {
  var agg_comps = [];

  if (components) {
      components.forEach(function(c){
        let ind_description = c.component_tags.filter(t => t.feature_claim_type === true)
          .sort((a, b) => ('' + a.feature_claim_ref + b.feature_claim_index)
            .localeCompare('' + b.feature_claim_ref + b.feature_claim_index));

        let ind_refs = new Set(ind_description.map(d => d.feature_claim_ref));
        
        let dep_description = c.component_tags.filter(t => t.feature_claim_type === false)
          .sort((a, b) => ('' + a.feature_claim_ref + b.feature_claim_index)
            .localeCompare('' + b.feature_claim_ref + b.feature_claim_index));
        
        let dep_refs = new Set(dep_description.map(d => d.feature_claim_ref));

        if (ind_description && ind_description.length > 0) {
          ind_refs.forEach((r) => {
            let desc = ind_description.filter(d => d.feature_claim_ref === r).map(d => d.tag_description);
            agg_comps.push({component_description: desc,
              component_name: c.component_name,
              text: c.text,
              component_type:'independent', 
              feature_claim_ref: r}
            );
          });
        }

        if (dep_description && dep_description.length > 0) {
          dep_refs.forEach((r) => {
            let desc = dep_description.filter(d => d.feature_claim_ref === r).map(d => d.tag_description);
            agg_comps.push({component_description: desc,
              component_name: c.component_name,
              text: c.text,
              component_type:'dependent',
              feature_claim_ref: r}
            );
          });
        }
      });
  }

  agg_comps = agg_comps.sort((a, b) => a.feature_claim_ref - b.feature_claim_ref);

  return agg_comps;
};

exports.aggregateComponentsOld = function(components){
  var agg_comps = [];

  if (components) {
      components.forEach(function(c){
      var ind_description = [];
      var dep_description = [];
      var ind_text = '';
      var dep_text = '';

      c.component_tags.forEach(function(t){
        if (t.feature_claim_type == true){
          ind_description = ind_description.concat(t.tag_description);
          ind_text = c.text;
        } else {
          dep_description = dep_description.concat(t.tag_description);
          dep_text = c.text;
        }
      })
      if(ind_description.length > 0){
        agg_comps.push({component_description: ind_description,
        component_name: c.component_name,
        text: ind_text,
        component_type:'independent'});
      }
      if(dep_description.length > 0){
        agg_comps.push({component_description: dep_description,
        component_name: c.component_name,
        text: dep_text,
        component_type:'dependent'});
      }
    });
  }
  return agg_comps;
};
