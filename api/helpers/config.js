// config.js
// all config values are here
// changes get picked up at app start

const config = {
    app: {
        port: 7575, // default is 7575
        id: '91cd4373-41d9-4867-b078-f3e7bba52f19', // used for uuid namespace, 
        version: '2.1.5' // app version http://semver.org
    },
    defaults: {
        components: {
            primary: {
                "text": "PLEASE ADD A PRIMARY COMPONENT", 
                "@": "Component",
                "component_name": "device",
                "component_tags": [{
                    "tag_name": "primary",
                    "@": "CompTagDescription"
                }]
            }, 
            input: {}, 
            output: {}
        }
    },
    log: {
        colors: true, // true/false default is true
        level: 'debug', // [debug, success, info, warn, error, none] default for prod should be success
        file: null,  // default to null for prod 
        truncate: false, // default to false for prod, 
        keywords: ['FLOWCHART_DESCRIPTION'] // add keywords that will be logged, all others will be ignored, empty array means show all
    },
    salesForce: { // salesforce specific device names
        deviceNames: [
            'database server',
            'data store',
            'application server',
            'user device',
            'source database',
            'target database',
            'operational data store',
            'analytical data store'
        ]
    },
    micron: {
        deviceNames: [
            'memory array',
            'memory cell'
        ]
    },
    xmlbuilder: {
        head: {
            version: '1.0',
            encoding: 'UTF-8'
        },
        options: {
            pubID: null,
            sysID: null,
            headless: true
        },
        skips: {
            skipNullNodes: true,
            skipNullAttributes: true
        },
        end: {
            pretty: false 
        }
    },
    visio: {
        flow: {
            margins: {
                top: 1.15,
                bottom: 0.375,
                left: 0.975,
                right: 0.625
            },
            spacing: {
                top: 0.125,
                bottom: 0.15,
                left: 0.125,
                right: 0.125
            },
            size: {
                width: 8.27,
                height: 11.69
            }
        },
        rectangle: {
            minimum: {
                width: 1.0,
                height: 1.0
            }
        },
        label: {
            position: {
                x: 0.55,
                y: 0.16
            },
            size: {
                width: 0.33,
                height: 0.16
            }
        },
        figure_label: {
            char_size: 0.25
        }
    },
    page: {
        size: {
            LETTER: {
                width: 8.5,
                height: 11.0
            },
            FOLIO: {
                width: 8.5,
                height: 13.0
            },
            TABLOID: {
                width: 11.0,
                height: 17.0
            },
            LEGAL: {
                width: 8.5,
                height: 14.0
            },
            STATEMENT: {
                width: 5.5,
                height: 8.5
            },
            EXECUTIVE: {
                width: 7.25,
                height: 10.5
            },
            A0: {
                width: 33.11,
                height: 46.81
            },
            A1: {
                width: 23.38,
                height: 33.11
            },
            A2: {
                width: 16.53,
                height: 23.38
            },
            A3: {
                width: 11.69,
                height: 16.54
            },
            A4: {
                width: 8.27,
                height: 11.69
            },
            A5: {
                width: 5.83,
                height: 8.27
            },
            B4: {
                width: 10.12,
                height: 14.33
            },
            B5: {
                width: 7.17,
                height: 10.12
            },
            ANSI_A: {
                width: 8.5,
                height: 11
            },
            ANSI_B: {
                width: 11.0,
                height: 17.0
            },
            ANSI_C: {
                width: 17.0,
                height: 22.0
            },
            ANSI_D: {
                width: 22.0,
                height: 34.0
            },
            ANSI_E: {
                width: 34.0,
                height: 44.0
            },
            ANSI_ARCH_A: {
                width: 9.0,
                height: 12.0
            },
            ANSI_ARCH_B: {
                width: 12.0,
                height: 18.0
            },
            ANSI_ARCH_C: {
                width: 18.0,
                height: 24.0
            },
            ANSI_ARCH_D: {
                width: 24.0,
                height: 36.0
            },
            ANSI_ARCH_E: {
                width: 30.0,
                height: 42.0
            }
        },
        orientation: [
            'PORTRAIT',
            'LANDSCAPE'
        ]
    }
};

module.exports = config;
