const ACRO_LANG = require("../../language/acronyms.js");

var verbs = [
	{gerund: 'monitoring', infinitive: 'monitor'},
	{gerund: 'reading', infinitive: 'read'},
	{gerund: 'coupling', infinitive: 'couple'},
	{gerund: 'accessing', infinitive: 'access'},
	{gerund: 'entering', infinitive: 'enter'},
  {gerund: 'a', infinitive: 'a'},
  {gerund: 'an', infinitive: 'an'},
  {gerund: 'the', infinitive: 'the'},
  {gerund: 'at', infinitive: 'at'},
  {gerund: 'one', infinitive: 'one'},
  {gerund: 'another', infinitive: 'another'},
  {gerund: 'sensing', infinitive: 'sense'},
  {gerund: 'forcing', infinitive: 'force'},
  {gerund: 'developing', infinitive: 'develop'},
	{gerund: 'adding', infinitive: 'add'},
	{gerund: 'using', infinitive: 'use'},
	{gerund: 'processing', infinitive: 'process'},
  {gerund: 'receiving', infinitive: 'receive'},
  {gerund: 'identifying', infinitive: 'identify'},
  {gerund: 'decoding', infinitive: 'decode'},
  {gerund: 'multiplexing', infinitive: 'multiplex'},
  {gerund: 'performing', infinitive: 'perform'},
  {gerund: 'parsing', infinitive: 'parse'},
  {gerund: 'repeating', infinitive: 'repeat'},
  {gerund: 'grouping', infinitive: 'group'},
  {gerund: 'recoupling', infinitive: 'recouple'},
  {gerund: 'biasing', infinitive: 'bias'},
  {gerund: 'filtering', infinitive: 'filter'},
  {gerund: 'triggering', infinitive: 'trigger'},
  {gerund: 'issuing', infinitive: 'issue'},
  {gerund: 'exchanging', infinitive: 'exchange'}
];

var vowels = ['a', 'e', 'i', 'o', 'u', 'y'];

var simplification = {
  'based at least in part on': 'based on',
  'wherein': 'where',
  'plurality': 'set',
  'compris': 'includ',
}

exports.articles = ['a', 'the', 'an', 'each','one', 'at', 'another'];

// if type does not equal 'final', 'advisory' or 'non-final', return an empty string
exports.actionType = function(type = ''){
  type = type.toLowerCase();
	if(type == 'final' || type == 'advisory' || type == 'non-final'){
    return type;
  }else{
    return "";
  }
}

// removes "et al" and extra white space from string
exports.cleanAuthor = function(author = ''){
	return author.replace('et al.', '').replace('et al', '').trim();
}

exports.infinitiveSentence = function(sentence){
    var index = 0;
    sentence = sentence || '';
    sentence = sentence.replace(',', ' ,');
    var verb = sentence.split(' ')[0];

    if (verb && verb.slice(-2) == 'ly' && sentence.split(' ')[1]) {
        verb = sentence.split(' ')[1];
        var index = 1;
    }

    sentence = sentence.replace(' ,', ',');
    return sentence.replace(verb, exports.infinitive(verb));
}

exports.infinitive = function(verb){
  var original = verb.toLowerCase();
	verbs.forEach(function(v){
		if(original == v.gerund){
			verb = v.infinitive;
    }
	})
	if(verb.slice(-3).toLowerCase() == 'ing' && verb.length > 4){
		var char4 = verb.charAt(verb.length - 4).toLowerCase();
		var char5 = verb.charAt(verb.length - 5).toLowerCase();
		if (char4 == char5 && vowels.indexOf(char4) < 0 && ['l', 's'].indexOf(char4) < 0){
      verb = verb.slice(0, -4);
    }else if((vowels.indexOf(char4) < 0 && vowels.indexOf(char5) > -1 && verb.indexOf('aining') < 0) || (char5 == 'b' && char4 == 'l') || (char5 == 'r' && char4 == 'g')){
      verb = verb.slice(0, -3) + 'e';
    }else{
      verb = verb.slice(0, -3);
    }
	}
	return verb;
}

exports.gerundSentence = function(sentence){
    var index = 0;
    sentence = sentence || '';
    sentence = sentence.replace(',', ' ,');
    var verb = sentence.split(' ')[0];

    if (verb && verb.slice(-2) == 'ly' && sentence.split(' ')[1]) {
        verb = sentence.split(' ')[1];
        var index = 1;
    }

    sentence = sentence.replace(' ,', ',');
    return sentence.replace(verb, exports.gerund(verb));
}

exports.gerund = function(verb) {
    if (verb == 'means') { return verb }
    
    var last = verb.charAt(verb.length - 1).toLowerCase();
    var second_last = verb.charAt(verb.length - 2).toLowerCase();
    var third_last = verb.charAt(verb.length - 3).toLowerCase();
    var specified_gerund;

    //console.log(verb)

    verbs.forEach(function(verb_object){
        if (verb_object.gerund == verb || verb_object.infinitive == verb)
            specified_gerund = verb_object.gerund;
    })

    if (specified_gerund)
        verb = specified_gerund;
    else if (verb.slice(-3).toLowerCase() == 'ing' && verb.length > 4){
    }
    else if (second_last == 'i' && last == 'e') {
        verb = verb.slice(0, -2) + 'ying';
    }
    else if (last == 'e') {
        verb = verb.slice(0, -1) + 'ing';
    }
    else if (vowels.indexOf(third_last) < 0 && vowels.indexOf(last) < 0 && vowels.indexOf(second_last) > -1 && last != 'w' && last != 'x' && last != 'y'){
        verb += last + 'ing';
    }
    else {
        verb += 'ing';
    }

    return verb;
}

exports.capitalize = function(string = "") {
  var first_char = string.charAt(0) || '';
  return first_char.toUpperCase() + string.slice(1);
}

exports.unCapitalize = function(string = "") {
  var keys = ['UE,', 'I'];
  for(var k in ACRO_LANG.acronyms) keys.push(k);
  first_word_slash = string.split('/')[0];
  first_word_dash = string.split('-')[0];
  if (keys.indexOf(first_word_slash) >= 0 || keys.indexOf(first_word_dash) >= 0 || (string == string.toUpperCase())){
    return string;
  }
  var first_char = string.charAt(0) || '';
  return first_char.toLowerCase() + string.slice(1);
}


exports.simplifyWithAcroDefinitions = function(text){
	text = exports.simplify(text);
	text = exports.spellAcronyms(text);
	return text;
}

exports.simplify = function(text){
	text = text || '';
	for(var key in simplification){
		var find = new RegExp(key, 'g');
		text = text.replace(find, simplification[key])
	}
	return exports.unspellAcronyms(text);
}

exports.unspellAcronyms = function(text){
	for(var key in ACRO_LANG.acronyms){
		var find = new RegExp((ACRO_LANG.acronyms[key] + ' \\(' + key + '\\)'), 'g');
		text = text.replace(find, key);
	}
	return text
}

exports.spellAcronyms = function(text){
	for(var key in ACRO_LANG.acronyms){
    text = text.replace(key, ACRO_LANG.acronyms[key] + ' (' + key + ')');
    text = text.replace('(' + ACRO_LANG.acronyms[key] + ' (' + key + '))', '(' + key + ')');
    text = text.replace('(' + ACRO_LANG.acronyms[key] + ' (' + key + ')s)', '(' + key + 's)');
  }
  return text
}

exports.removeListSeparators = function(text){
    if (text.slice(-1) == ';' || text.slice(-1) == '.'){
        return text.slice(0, -1);
    } else if(text.slice(-5) == '; and') {
        return text.slice(0, -5);
    } else {
        return text;
    }
}

exports.capWords = function(string){
    return (typeof string == 'string') ? string.split(' ').map(function(word){return exports.capitalize(word)}).join(' ') : string;
}

exports.unCapWords = function(string){
    return (typeof string == 'string') ? string.split(' ').map(function(word){return exports.unCapitalize(word)}).join(' ') : string;
}
