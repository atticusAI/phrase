const G_TOOLS = require("../tools/genericTools.js");

exports.listClaims = function(claims){
  var claim_list = [];
  for(var c in claims){
    claim_list.push(claims[c]);
    if(claims[c].tchildren){
      claim_list = claim_list.concat(exports.listClaims(claims[c].tchildren));
    }
  }
  return claim_list;
}

exports.combineClaims = function(claim_list){
  var claims = claim_list[0];
  var amendments = claim_list[1];
  var new_list = [];
  for(var a in amendments){
    if(amendments[a].status == "Added"){
      new_list.push(amendments[a]);
      if(amendments[a].tchildren){
        var children_obj = [[], amendments[a].tchildren]
        new_list = new_list.concat(exports.combineClaims(children_obj));
      }
    }else{
      if(amendments[a].status == "Amended"){
        amendments[a].claim_preamble = "";
        if(amendments[a].preamble_edit){
          amendments[a].claim_preamble = exports.stringifyRuns(amendments[a].preamble_edit);
        }
        amendments[a].claim_features = [];
        if(amendments[a].feature_edits){
          for(var e in amendments[a].feature_edits){
            var feature = {feature_text: exports.stringifyRuns(amendments[a].feature_edits[e])}
            amendments[a].claim_features.push(feature);
          }
        }
        if(claims[a].claim_type){
          amendments[a].claim_type = claims[a].claim_type;
        }
        new_list.push(amendments[a]);
      }else{
        if(amendments[a].status == "Canceled" || amendments[a].status == "Withdrawn"){
          claims[a].status = amendments[a].status;
        }
        new_list.push(claims[a]);
      }
      if(amendments[a].tchildren){
        var children_obj = [claims[a].tchildren, amendments[a].tchildren]
        new_list = new_list.concat(exports.combineClaims(children_obj));
      }
    }
  }
  return new_list;
}

exports.stringifyRuns = function(runs = []){
  var run_string = "";
  for(var r in runs){
    if(runs[r].indexOf("Mtch:") == 0){
      run_string += runs[r].slice(5);
    }else if(runs[r].indexOf("Add:") == 0){
      run_string += runs[r].slice(4);
    }
  }
  return run_string;
}

exports.combineCanceledClaims = function(claim_list){
  var first_canceled = null;
  for(var c = 0; c < claim_list.length; c++){
    if(claim_list[c].status && claim_list[c].status.toLowerCase() == "canceled"){
      if(first_canceled != null){
        if(first_canceled.item_num.toString().indexOf("-") > -1){
          var nums = first_canceled.item_num.split("-");
          if(nums[1] == claim_list[c].item_num -1){
            first_canceled.item_num = nums[0]+"-"+claim_list[c].item_num;
            claim_list.splice(c, 1);
            c--;
          }else{
            first_canceled = claim_list[c];
          }
        }else{
          if(first_canceled.item_num == claim_list[c].item_num-1){
            first_canceled.item_num += "-"+claim_list[c].item_num;
            claim_list.splice(c, 1);
            c--;
          }else{
            first_canceled = claim_list[c];
          }
        }
      }else{
        first_canceled = claim_list[c];
      }
    }else{
      first_canceled = null;
    }
  }
}


exports.wordTable = function(data, headers, col_widths, super_header, shade_group, color, options){
    
    // Locally defined 'mark' function
    function mark(tag, content, attributes, leaf){
      var markup = '<' + tag;
      if (tag == 'input') leaf = true;
      for (var key in attributes){
          markup = markup + ' ' + key + '="' + attributes[key] + '"';
      }
      markup = (leaf) ? (markup + '/>') : (markup + '>' + content + '</' + tag + '>');
      return markup;
    }

    options = options || {};
    color = color || 'C8C8C8';
    var table_style = '<w:tblStyle w:val="TableGrid"/>';
    var table_width = '<w:tblW w:w="0" w:type="auto"/>';
    var table_look = '<w:tblLook w:noHBand="true" w:noVBand="true"/>';
    var inside = {'w:sz' : '6', 'w:val' : 'single'};
    var outside = {'w:sz' : '12', 'w:val' : 'single'};
    var table_borders = mark('w:tblBorders',  mark('w:top', '', outside, true) + mark('w:bottom', '', outside, true) 
      + mark('w:start', '', outside, true) + mark('w:end', '', outside, true) 
      + mark('w:insideH', '', inside, true) + mark('w:insideV', '', inside, true) );
    var table_properties = mark('w:tblPr', table_style + table_width + table_look + table_borders);

    var tbl_grid = mark('w:tblGrid', col_widths.map(function(width){return '<w:gridCol w:w="' + width + '"/>'}).join('') );
    var super_header_row = super_header ? tblRow([super_header], true, col_widths.length, false, color, options.super_header_justification) : '';
    var header_row = headers ? tblRow(headers, true, null, null, color) : '';
    var data_rows = data.map(function(row_data, index){
      var alt = (index - (index % shade_group))/shade_group;
      var shade = ( parseInt(shade_group) && (alt % 2 == 1) ) ? true : false;
      return tblRow(row_data, false, false, shade, color);
    }).join('');

    var word_table = mark('w:tbl', table_properties + tbl_grid + super_header_row + header_row + data_rows);
    
    function tblRow(row_data, row_header, row_span, row_shade, row_color, justification){
      if (!G_TOOLS.isArray(row_data)) row_data = [];
        var tbl_row = mark('w:tr', row_data.map(function(content, row_index){
          var width = (row_index < col_widths.length) ? col_widths[row_index] : 10;
          if (row_data.length == 1) {
            var total_width = 0;
            col_widths.forEach(function(w){total_width += parseInt(w)})
            width = total_width;
          }
            var tbl_col = tblColumn(content, width, row_header, row_span, row_shade, row_color, justification);
            return tbl_col;
        }).join('') )

        //console.log(tbl_row);
        return tbl_row;
    }
    
    function tblColumn(column_content, column_width, column_header, column_span, column_shade, column_color, justification) {
        var tcw = '<w:tcW w:w="' + column_width + '" w:type="dxa"/>';
        var align = '<w:vAlign w:val="center"/>';
        
        var fill = '';
        if (column_header) fill = '<w:shd w:val="clear" w:color="auto" w:fill="' + column_color + '"/>';
        else if (column_shade) fill = '<w:shd w:val="clear" w:color="auto" w:fill="D3D3D3"/>';

        column_span = column_span ? '<w:gridSpan w:val="' + column_span + '"/>' : '';
        var tc_prop = mark('w:tcPr', tcw + column_span + fill + align);

        var run_prop = column_header ? mark('w:rPr', '<w:b />') : '';
        var text = mark('w:t', column_content);
        var run = mark('w:r', run_prop + text);

        var par_style = '<w:pStyle w:val="BodyText"/>';
        var spacing = '<w:spacing w:after="0"/>';
        var indent = '<w:ind w:firstLine="0"/>';
        justification = justification || ((column_header || column_width < 2000) ? 'center' : 'left');
        var jc = '<w:jc w:val="' + justification + '"/>';
        var par_prop = mark('w:pPr', par_style + spacing + indent + jc);
        var par = mark('w:p', par_prop + run)

        var tbl_col = mark('w:tc', tc_prop + par);
        
        return tbl_col;
    }

    return word_table;
}

exports.extractSimplePreamble = function(long_preamble){
  if (long_preamble.indexOf('method, comprising') > 0){
    return '';
  }

  if (long_preamble.indexOf(' for ') > 0){
    var start = long_preamble.indexOf('for ') + 4;
  }
  else {
    var start = long_preamble.indexOf('of ') + 3;
  }
  
  var end = long_preamble.indexOf(', comprising');
  if (end < 0){
    end = long_preamble.indexOf(', wherein');
  }

  return long_preamble.slice(start, end);
}

exports.extractComplexPreamble = function(long_preamble){
  var start = long_preamble.indexOf('wherein ') + 8;
  var end = long_preamble.indexOf('comprises') + 9;
  return long_preamble.slice(start, end)+ ' ';
}