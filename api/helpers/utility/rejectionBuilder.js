const REJECTION_INFO = require("../language/rejectionInfo.js");
const REF_BUILDER = require("./referenceBuilder.js");
const G_TOOLS = require("./tools/genericTools.js");
const S_TOOLS = require("./tools/stringTools.js");
const CLAIM_TOOLS = require("./tools/claimTools.js");

exports.rejectionMarkup = function(task = {}, type) {
	var rejection_info = REJECTION_INFO.list[type] || {};
	var rejection_list = [];
	var rejections = (task.task_input && task.task_input.oa_rejections) ? task.task_input.oa_rejections : [];
  // gather rejections that are of the requested type
	rejections.forEach(function(rejection){
		if (rejection.reject_type == type){
      rejection_list.push(rejection);
    }
	})
  var claim_obj = [[],[]];
  if(task.task_input && task.task_input.oa_previous_claims){
    claim_obj[0] = task.task_input.oa_previous_claims;
  }
  if(task.task_output && task.task_output.oa_claim_updates){
    claim_obj[1] = task.task_output.oa_claim_updates;
  }
	var claims = CLAIM_TOOLS.combineClaims(claim_obj);
	if (rejection_list.length > 0) {
		var content = rejection_list.map(function(rejection, index){
			var claims = G_TOOLS.writeNumbers(rejection.reject_claims) || '{CLAIMS}';
      var primary_claim = G_TOOLS.breakDownNumbers(claims)[0];
      claims.replace('-', '–')
			var subheader_markup = '';
      if(rejection_list.length > 1 && rejection_info.subheader){
        subheader_markup = 
        `<w:p>
    			<w:pPr>
            <w:keepNext/>
    				<w:spacing w:before="0" w:after="0" w:line="360" w:lineRule="auto"></w:spacing>
    			</w:pPr>
    			<w:r>
            <w:rPr>
              <w:u w:val="single"/>
              <w:i/>
            </w:rPr>
    				<w:t xml:space="preserve">${rejection_info.subheader}.</w:t>
    			</w:r>
    		</w:p><w:p>
    			<w:pPr>
            <w:ind w:firstLine="720"></w:ind>
    				<w:spacing w:before="0" w:after="120" w:line="360" w:lineRule="auto"></w:spacing>
    			</w:pPr>
    			<w:r>
            <w:rPr></w:rPr>
    				<w:t xml:space="preserve">${rejection_info.text}</w:t>
    			</w:r>
    		</w:p>`;
      }
      var follower = rejection.reject_refs.map(function(reference){
        var author = S_TOOLS.cleanAuthor(reference.ref_short_name);
        var ref_citation = reference.ref_citation || '[ENTER HIGH LEVEL DESCRIPTION]';
        return `<w:p>
    			<w:pPr>
            <w:ind w:firstLine="720"></w:ind>
    				<w:spacing w:before="0" w:after="120" w:line="360" w:lineRule="auto"></w:spacing>
    			</w:pPr>
    			<w:r>
            <w:rPr></w:rPr>
    				<w:t xml:space="preserve">${author} generally discusses ${ref_citation}. ${author}, Abstract.'</w:t>
    			</w:r>
    		</w:p>`;
        // + ${referenceMarkup(reference, T.breakDownNumbers(claims), ref_cites, author)}
      }).join('');
			var rejection_text = subheader_markup + follower;
      
			var reference_list = rejection.reject_refs || [];
			var references = REF_BUILDER.referenceList(reference_list, []);
			if (claims.length < 4 && claims.indexOf(',') < 0 && claims.indexOf('–') < 0){
        rejection_text = rejection_text.replace(/aims {CLAIMS}/g, 'aim ' + claims);
      }
			rejection_text = rejection_text.replace(/{CLAIMS}/g, claims).replace(/{REF}/g, references);
			return rejection_text;
		}).join('');
		return content;
	}else{
    return '';
  }
}

// return an array of each OA rejection with its references in string format
exports.rejectionList = function(patent_office_action){
  var rejection_list = []; // list of rejections in text format
  var rejections = patent_office_action.oa_rejections || []; 
  rejections = rejections.filter(r=>{
    return r.reject_claims.length > 0;
  })
  var used_reference_list = []; // keeps list of each rejection that has been used
  rejections.forEach(function(rejection){
    var rejection_reference_list = rejection.reject_refs || [];
    var rejection_info = REJECTION_INFO.list[rejection.reject_type] || {};
    var references = REF_BUILDER.referenceList(rejection_reference_list, used_reference_list);
    used_reference_list = used_reference_list.concat(rejection_reference_list);
    var rejection_text = rejection_info.short || '';
    var claims = G_TOOLS.writeNumbers(rejection.reject_claims) || '';
    claims.replace('-', '–')
    if (claims.length < 4 && claims.indexOf(',') < 0 && claims.indexOf('–') < 0){ 
      rejection_text = rejection_text
        .replace(/aims {CLAIMS} were/g, 'aim ' + claims + ' was')
        .replace(/because they use a generic placeholder/g, 'because it uses a generic placeholder');
    }
    rejection_text = rejection_text
      .replace(/{CLAIMS}/g, claims)
      .replace('{REFS}', references);
    if(patent_office_action.pre_a_i_a){
      rejection_text = rejection_text.replace(/{CODE_CITE}/g, REJECTION_INFO.list[rejection.reject_type]["pre-AIA"]);
    }else{
      rejection_text = rejection_text.replace(/{CODE_CITE}/g, REJECTION_INFO.list[rejection.reject_type]["AIA"]);
    }
    rejection_list.push(rejection_text)
  })
  return rejection_list; 
};
