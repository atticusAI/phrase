const uuid = require('uuid/v3');
const XML_BUILDER = require('xmlbuilder');
const VIEWS = require('../views/word');
const config = require('../../config');
const TAGS = require('../tools/tagTools');

exports.WordTools = function () {
    var _gid = 1;

    class View {
        constructor(templateName) {
            this.templateName = templateName;
        }

        build(context) {
            return VIEWS[this.templateName].call(context);
        }
    }

    class WordObject {
        constructor(viewName) {
            this.view = new View(viewName);
            this.children = [];
            this.properties = null;
        }

        append(obj) {
            this.children.push(obj);
        }

        build() {
            return this.view.build(this);
        }

        toXML() {
            var xml = XML_BUILDER.create(this.build(),
                config.xmlbuilder.head || {
                    version: '1.0',
                    encoding: 'UTF-8'
                },
                config.xmlbuilder.options || {
                    pubID: null,
                    sysID: null,
                    headless: true
                },
                config.xmlbuilder.skips || {
                    skipNullNodes: true,
                    skipNullAttributes: true
                }
            );

            for (var c in this.children) {
                let el = this.children[c].build(xml);
                if (el) {
                    xml.ele(el);
                } 
            }

            return xml.end(config.xmlbuilder.end || {
                pretty: false
            });
        }
    }

    class Paragraph extends WordObject {
        constructor() {
            super('Paragraph');
        }
    }

    class Text extends WordObject {
        constructor(text) {
            super('Text');
            this.text = text;
            this.propertiesName = 'w:rPr';
            this.properties = [];
        }

        getProperty(name) {
            return this.properties.find(p => p.name === name);
        }

        setProperty(name, val) {
            this.deleteProperty(name);
            this.properties.push({
                "name": name,
                "val": val
            });
        }

        deleteProperty(name) {
            this.properties = this.properties.filter(p => p.name !== name);
        }

        get fontSize() {
            return this.getProperty('fontSize').val;
        }
        set fontSize(val) {
            this.setProperty('fontSize', val);
        }

        get bold() {
            return this.getProperty('bold').val;
        }
        set bold(val) {
            this.setProperty('bold', val);
        }

        /*
        build(xml) {
            if (this.properties.length > 0){
                this.children.append(
                    this.propertiesName: {

                    }
                );
            }
            var b = this.view.build(this);
        }
        */
    }

    class RefSource extends WordObject {
        constructor(hash, type) {
            super("RefSource");
            this.hash = hash;
            this.type = type;
            this.id = uuid(hash + type, config.app.id);
        }
    }

    class RefTarget extends WordObject {
        constructor(hash, type) {
            super('RefTarget');
            this.hash = hash;
            this.type = type;
            this.srcid = uuid(hash + type, config.app.id);
        }
    }

    class ComponentRefTarget extends RefTarget {
        constructor(device, component_name){
            super(device.text, 'Figure');
            this.cmp = TAGS.findComponent(device.components, component_name); 
        }

        build(xml) {
            xml.ele(super.build(xml));
            xml.ele(new Text(this.cmp ? this.cmp.text || '' : '').build(xml));
        }
    }

    class ComponentList extends WordObject {
        constructor(device) { 
            super('ComponentList');
            this.device = device;
        }

        build(xml) {
            if (this.device && this.device.components) {
                for (var i = 0; i < this.device.components.length; i++) {
                    let cmp = this.device.components[i];
                    this.append(new Text(cmp.component_name + ' '));
                    this.append(new RefTarget(this.device.text, 'Figure'));
                    var t;
                    if (i === this.device.components.length - 2) {
                        t = cmp.text + ', and ';
                    } else if (i === this.device.components.length - 1) {
                        t = cmp.text + '.';
                    } else {
                        t = cmp.text + ', ';
                    }
                    this.append(new Text(t));
                }
            }
            for (var c in this.children){
                xml.ele(this.children[c].build());
            }
        }
    }

    class ParagraphBuilder {
        constructor(view) {
            this.view = view;
            this.parent = new Paragraph();
        }

        build(context) {
            var elements = [];
            if (typeof this.view === 'function') {
                elements = this.view.call(context);
            } else if (Array.isArray(view)){
                elements = view;
            }

            for (var e in elements) {
                if (typeof elements[e] === 'string') {
                    this.parent.append(new Text(elements[e]));
                } else if (typeof elements[e] === 'object') { 
                    this.parent.append(elements[e]);
                }
            }
        }

        toXML(device) {
            this.build(device);
            return this.parent.toXML();
        }
    }

    // exports
    this.Paragraph = Paragraph;
    this.ParagraphBuilder = ParagraphBuilder;
    this.Text = Text;
    this.RefSource = RefSource;
    this.RefTarget = RefTarget;
    this.ComponentRefTarget = ComponentRefTarget;
    this.ComponentList = ComponentList;
};