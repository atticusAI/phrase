// returns an array of components
// recursively copy all tchildrend outer array
exports.flatten = function(components){
    let cmp = [];
    (components || []).map(c => {
        cmp.push(c);
        if (c.tchildren && c.tchildren.length > 0){
            cmp = cmp.concat(exports.flatten(c.tchildren));
        }
    });
    return cmp;
};

// find a component by component name
// recursively search all components and children
exports.findComponent = function(components, component_name){
    var cmp;
    (components || []).some((c) => {
        if (c.component_name === component_name) {
            cmp = c;
            return true;
        } else {
            if (!cmp && c.tchildren && c.tchildren.length > 0){
                cmp = exports.findComponent(c.tchildren, component_name);
                return typeof cmp !== 'undefined';
            } else {
                return false;
            }
        }
    });
    return cmp;
};

exports.softwareComponent = function(components) {
    var sft = exports.findComponent(components, "Software");
    if (!sft) { 
        sft = exports.findComponent(components, "Code");
    }
    return sft;
};
