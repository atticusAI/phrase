


{
    "device_name":"UE",
    "primary_module":{
        "tnode":{
            "module_name":"UE Coding Manager"
        }
    },
    "static_modules":[
        {"tnode":{
            "module_name":"I/O Controller",
            "module_description" : ["I/O controller {#I/O Controller} may manage input and output signals for device {#UE}. I/O controller {#I/O Controller} may also manage peripherals not integrated into device {#UE}. In some cases, I/O controller {#I/O Controller} may represent a physical connection or port to an external peripheral. In some cases, I/O controller {#I/O Controller} may utilize an operating system such as iOS®, ANDROID®, MS-DOS®, MS-WINDOWS®, OS/2®, UNIX®, LINUX®, or another known operating system. In other cases, I/O controller {#I/O Controller} may represent or interact with a modem, a keyboard, a mouse, a touchscreen, or a similar device. In some cases, I/O controller {#I/O Controller} may be implemented as part of a processor. In some cases, a user may interact with device {#UE} via I/O controller {#I/O Controller} or via hardware components controlled by I/O controller {#I/O Controller}."]
            }
        },
        {"tnode":{
            "module_name":"Transceiver",
            "module_description" : ["Transceiver {#Transceiver} may communicate bi-directionally, via one or more antennas, wired, or wireless links as described above. For example, the transceiver {#Transceiver} may represent a wireless transceiver and may communicate bi-directionally with another wireless transceiver. The transceiver {#Transceiver} may also include a modem to modulate the packets and provide the modulated packets to the antennas for transmission, and to demodulate packets received from the antennas."]
            }
        },
        {"tnode":{
            "module_name":"Antenna",
            "module_description" : ["In some cases, the wireless device may include a single antenna {#Antenna}. However, in some cases the device may have more than one antenna {#Antenna}, which may be capable of concurrently transmitting or receiving multiple wireless transmissions."]
            }
        },
        {"tnode":{
            "module_name":"Memory"
            "module_description" : ["Memory {#Memory} may include RAM and ROM. The memory {#Memory} may store computer-readable, computer-executable software {#Software} including instructions that, when executed, cause the processor to perform various functions described herein. In some cases, the memory {#Memory} may contain, among other things, a BIOS which may control basic hardware or software operation such as the interaction with peripheral components or devices."]
        	},
        	"tchildren": [{
                "tnode":{
                    	"module_name":"Software",
                    	"module_description" : ["Software {#Software} may include code to implement aspects of the present disclosure, including code to support {TITLE}. Software {#Software} may be stored in a non-transitory computer-readable medium such as system memory or other memory. In some cases, the software {#Software} may not be directly executable by the processor but may cause a computer (e.g., when compiled and executed) to perform functions described herein."]
                    }
                }]
        },
        {"tnode":{
            "module_name":"Processor",
            "module_description" : ["Processor {#Processor} may include an intelligent hardware device, (e.g., a general-purpose processor, a DSP, a CPU, a microcontroller, an ASIC, an FPGA, a programmable logic device, a discrete gate or transistor logic component, a discrete hardware component, or any combination thereof). In some cases, processor {#Processor} may be configured to operate a memory array using a memory controller. In other cases, a memory controller may be integrated into processor {#Processor}. Processor {#Processor} may be configured to execute computer-readable instructions stored in a memory to perform various functions (e.g., functions or tasks supporting {TITLE})."]
            }
        },
    	{"tnode":{
            "module_name":"Bus",
            "module_description" : [""]
            }
        }
    ],
    "input_module":{
        "tnode":{
            "module_name":"Receiver"
            "module_description" : ["Receiver {#Receiver} may receive information such as packets, user data, or control information associated with various information channels (e.g., control channels, data channels, and information related to {TITLE}, etc.). Information may be passed on to other components of the device. The receiver {#Receiver} may be an example of aspects of the transceiver {#Transceiver} described with reference to FIG. {F#Transceiver}. The receiver {#Receiver} may utilize a single antenna or a plurality of antennas."]
        }
    },
    "output_module":{
        "tnode":{
            "module_name":"Transmitter",
         	"module_description" : ["Transmitter {#Transmitter} may transmit signals generated by other components of the device. In some examples, the transmitter {#Transmitter} may be collocated with a receiver {#Receiver} in a transceiver module. For example, the transmitter {#Transmitter} may be an example of aspects of the transceiver {#Transceiver} described with reference to FIG. {F#Transceiver}. The transmitter {#Transmitter} may utilize a single antenna or a plurality of antennas."]
        }
    }
}







{
    "device_name":"BS",
    "primary_module":{
        "tnode":{
            "module_name":"Base Station Coding Manager"
        }
    },
    "static_modules":[
        {"tnode":{
            "module_name":"Network Communications Manager",
            "module_description" : ["Network communications manager {#Network Communications Manager} may manage communications with the core network (e.g., via one or more wired backhaul links). For example, the network communications manager {#Network Communications Manager} may manage the transfer of data communications for client devices, such as one or more UEs 115."]
            }
        },
        {"tnode":{
            "module_name":"Transceiver",
            "module_description" : ["Transceiver {#Transceiver} may communicate bi-directionally, via one or more antennas, wired, or wireless links as described above. For example, the transceiver {#Transceiver} may represent a wireless transceiver and may communicate bi-directionally with another wireless transceiver. The transceiver {#Transceiver} may also include a modem to modulate the packets and provide the modulated packets to the antennas for transmission, and to demodulate packets received from the antennas."]
            }
        },
        {"tnode":{
            "module_name":"Antenna",
            "module_description" : ["In some cases, the wireless device may include a single antenna {#Antenna}. However, in some cases the device may have more than one antenna {#Antenna}, which may be capable of concurrently transmitting or receiving multiple wireless transmissions."]
            }
        },
        {"tnode":{
            "module_name":"Memory",
            "module_description" : ["Memory {#Memory} may include RAM and ROM. The memory {#Memory} may store computer-readable, computer-executable software {#Software} including instructions that, when executed, cause the processor to perform various functions described herein. In some cases, the memory {#Memory} may contain, among other things, a BIOS which may control basic hardware or software operation such as the interaction with peripheral components or devices."]
        	},
        	"tchildren": [{
                "tnode":{
                    	"module_name":"Software",
                    	"module_description" : ["Software {#Software} may include code to implement aspects of the present disclosure, including code to support {TITLE}. Software {#Software} may be stored in a non-transitory computer-readable medium such as system memory or other memory. In some cases, the software {#Software} may not be directly executable by the processor but may cause a computer (e.g., when compiled and executed) to perform functions described herein."]
                    }
                }]
        },
        {"tnode":{
            "module_name":"Processor",
            "module_description" : ["Processor {#Processor} may include an intelligent hardware device, (e.g., a general-purpose processor, a DSP, a CPU, a microcontroller, an ASIC, an FPGA, a programmable logic device, a discrete gate or transistor logic component, a discrete hardware component, or any combination thereof). In some cases, processor {#Processor} may be configured to operate a memory array using a memory controller. In other cases, a memory controller may be integrated into processor {#Processor}. Processor {#Processor} may be configured to execute computer-readable instructions stored in a memory to perform various functions (e.g., functions or tasks supporting {TITLE})."]
            }
        },
        {"tnode":{
            "module_name":"Inter-station Communications Manager",
            "module_description" : ["Inter-station communications manager {#Inter-station Communications Manager} may manage communications with other base station 105, and may include a controller or scheduler for controlling communications with UEs 115 in cooperation with other base stations 105. For example, the inter-station communications manager {#Inter-station Communications Manager} may coordinate scheduling for transmissions to UEs 115 for various interference mitigation techniques such as beamforming or joint transmission. In some examples, inter-station communications manager {#Inter-station Communications Manager} may provide an X2 interface within an LTE/LTE-A wireless communication network technology to provide communication between base stations 105."]
            }
        }
    ],
    "input_module":{
        "tnode":{
            "module_name":"Receiver",
            "module_description" : ["Receiver {#Receiver} may receive information such as packets, user data, or control information associated with various information channels (e.g., control channels, data channels, and information related to {TITLE}, etc.). Information may be passed on to other components of the device. The receiver {#Receiver} may be an example of aspects of the transceiver {#Transceiver} described with reference to FIG. {F#Transceiver}. The receiver {#Receiver} may utilize a single antenna or a plurality of antennas."]
        }
    },
    "output_module":{
        "tnode":{
            "module_name":"Transmitter",
         	"module_description" : ["Transmitter {#Transmitter} may transmit signals generated by other components of the device. In some examples, the transmitter {#Transmitter} may be collocated with a receiver {#Receiver} in a transceiver module. For example, the transmitter {#Transmitter} may be an example of aspects of the transceiver {#Transceiver} described with reference to FIG. {F#Transceiver}. The transmitter {#Transmitter} may utilize a single antenna or a plurality of antennas."]
        }
    }
}

