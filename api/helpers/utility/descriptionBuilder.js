// const TECH_LANG = require("../language/technologies.json");
const X_TOOLS = require('./tools/xmlTools.js');
const G_TOOLS = require('./tools/genericTools.js');
const S_TOOLS = require('./tools/stringTools.js');
const CLAIM_BUILDER = require('./claimBuilder.js');
// const FIG_TOOLS = require("./tools/figureTools.js");
const B_TOOLS = require("./tools/blockDescriptionTools.js");
const W_TOOLS = require('./tools/wordTools');
// const TAGS = require('./tools/tagTools');
const config = require ('../config');

// Filtering happens based on salesforce device names (since we don't have a client), maintain
// that list here
const SF_DEV_NAMES = config.salesForce.deviceNames; // ['database server', 'data store', 'application server', 'user device', 'source database', 'target database', 'operational data store', 'analytical data store']

const NUM_PAR_PROP = {
  numbering: true,
  tabs: [1080],
  before: 160,
  line: 360,
  keep_w_next: false
}

exports.writeTechnologies = function(technologies){
  var techs = "";
  technologies.forEach(function(t){
    techs += exports.formatParagraphs(t.tech_description)
  })

  return S_TOOLS.simplifyWithAcroDefinitions(techs)
}

exports.formatParagraphs = function(input_text){

  var text = ''

  var pars = input_text.split('\n').filter(p=>{
    return p.length > 0;
  })
  pars.forEach(function(p){
    var repl_para = ''
    var words = p.split(' ')
    words.forEach(function(w){
      var key = (w.substring(w.indexOf('{')+1, w.indexOf('}')))
      var tail = w.substring(w.indexOf('}')+1)
      if (key.indexOf('NewFigNum') >= 0){ // ||
        var fig_label = B_TOOLS.hash(key.substring(key.indexOf(':')+1))
        w = X_TOOLS.injectRun(X_TOOLS.refSource('Figure', fig_label)) + tail
      } else if (key.indexOf('FigRef') >= 0){
        var fig_label = B_TOOLS.hash(key.substring(key.indexOf(':')+1))
        w = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_label)) + tail
      } else if (key.indexOf('BOLD') >= 0){
        var btext = key.substring(key.indexOf(':')+1)
        w = X_TOOLS.injectRun(X_TOOLS.r(btext, {bold: true}))
      }
      repl_para += w + ' '
    })
    text += X_TOOLS.p(X_TOOLS.r(repl_para), NUM_PAR_PROP)
  })

  return S_TOOLS.simplifyWithAcroDefinitions(text)
}

exports.backgroundDescription = function(application = {}){
  var background_descriptions = "";
  var technologies = application.technologies || [];
  for(var t in technologies){
    background_descriptions += X_TOOLS.p(X_TOOLS.r(technologies[t].tech_description), NUM_PAR_PROP);
  }
  return S_TOOLS.simplifyWithAcroDefinitions(background_descriptions);
}


exports.narrativeDescription = function(fig_label, client){

  var aspects = 'aspects of the present disclosure'
  if (client == 'Micron'){
    wc_implementation = '';
    aspects = 'examples as disclosed herein'
  } else {
    wc_implementation = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_label)) + '00 may implement aspects of wireless communication system 100.';
  }

  narratives = X_TOOLS.p(X_TOOLS.r('FIG. ', {bold: true}) + X_TOOLS.refSource('Figure', fig_label) + X_TOOLS.r(` illustrates an example of a **DESCRIPTION${fig_label}** ` + X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_label)) + `00 that supports {TITLE.LCASE} in accordance with ${aspects}. In some examples, **DESCRIPTION${fig_label}** ` + wc_implementation + ' **ADD DESCRIPTION OF HOW THE FIGURE RELATES TO THE INVENTION**'), NUM_PAR_PROP);

  return narratives;
}

// This "Standard" narrative description does not include any wireless comms text
exports.narrativeDescriptionSTD = function(fig_label){
  narratives = X_TOOLS.p(X_TOOLS.r('FIG. ', {bold: true}) + X_TOOLS.refSource('Figure', fig_label) + X_TOOLS.r(` illustrates an example of a **DESCRIPTION${fig_label}** ` + X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_label)) + `00 that supports {TITLE.LCASE} in accordance with aspects of the present disclosure. **ADD DESCRIPTION OF HOW THE FIGURE RELATES TO THE INVENTION**`), NUM_PAR_PROP);

  return narratives;
}


exports.flowDescription = function(flow_obj, client){

  if (flow_obj.length == 0){ return ''; }

  var device_name = flow_obj[0][0];
  var fig_num = flow_obj[1];
  var fc_info = flow_obj[2];
  var dev_name_fig1label = device_name;

  if (device_name == 'UE, base station'){
    device_name = 'UE or base station';
    dev_name_fig1label = 'UE 115 or base station 105';
    flow_obj[3][3][1] = flow_obj[3][4][1];
  } else if (device_name == 'UE'){
    dev_name_fig1label += ' 115';
  } else if (device_name == 'base station'){
    dev_name_fig1label += ' 105';
  }

  var flowchart_content = '';
  var fig_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_num));
  var fig_labels =  B_TOOLS.getLabelList(flow_obj[3]);

  var ref_text = 'FIGs. {BLOCK_A} through {BLOCK_H}.'
  if(fig_labels.block_ma == undefined && fig_labels.block_mb != undefined){
    fig_labels.block_ma = fig_labels.block_mb;
  } else if (fig_labels.block_ma == undefined && fig_labels.block_mb == undefined){
    var ref_text = 'FIG. {BLOCK_H}.';
  }

  // Here again it would be better to filter based on whether BLOCK_H is available rather than by client
  var method = 'method';
  if (client == 'Micron'){
    // ref_text = '[[FIGs. XX through YY - {BLOCK_C}]].'
    ref_text = 'FIG. {BLOCK_C}.';
    method = 'method or methods';
    // Use the device name as the primary module name
    flow_obj[0][1] = flow_obj[0][0];
  }

  var flow_text = X_TOOLS.p( X_TOOLS.r('FIG. ', {bold: true}) + X_TOOLS.refSource('Figure', fig_num) + X_TOOLS.r(` shows a flowchart illustrating a ${method} {FIG}00 that supports {TITLE.LCASE} in accordance with aspects of the present disclosure. The operations of method {FIG}00 may be implemented by a ${dev_name_fig1label} or its components as described herein. For example, the operations of method {FIG}00 may be performed by a {PRIMARY} as described with reference to ${ref_text} In some examples, a {DEVICE_NAME} may execute a set of instructions to control the functional elements of the {DEVICE_NAME} to perform the described functions. Additionally or alternatively, a {DEVICE_NAME} may perform aspects of the described functions using special-purpose hardware.`), NUM_PAR_PROP);

  for(var f in fc_info){
    flow_text += X_TOOLS.p( X_TOOLS.r(`At {STEP}, the {DEVICE_NAME} may {STEP_TEXT}. The operations of {STEP} may be performed according to the methods described herein. In some examples, aspects of the operations of {STEP} may be performed by {ARTICLE} {STEP_MODULE} as described with reference to ${ref_text}`), NUM_PAR_PROP);
    if(fc_info[f].feature_component != null && ['a', 'e', 'i', 'o', 'u'].indexOf(fc_info[f].feature_component.slice(0,1).toLowerCase()) >= 0){
      var article = 'an';
    } else {
      var article = 'a';
    }
    flow_text = flow_text.replace(/{STEP}/g, fig_label+fc_info[f].text)
                         .replace(/{STEP_TEXT}/g, S_TOOLS.infinitiveSentence(G_TOOLS.removeFeaturePunctuation(fc_info[f].feature_text)))
                         .replace(/{STEP_MODULE}/g, fc_info[f].feature_component)
                         .replace(/{ARTICLE}/g, article);
  }

  flowchart_content += flow_text.replace(/{FIG}/g, fig_label)
                                .replace(/{DEVICE_NAME}/g, device_name)
                                .replace(/{PRIMARY}/g, flow_obj[0][1])
                                .replace(/{BLOCK_A}/g, X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_labels.block_ma)))
                                .replace(/{BLOCK_C}/g, X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_labels.block_mc)))
                                .replace(/{BLOCK_H}/g, X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_labels.block_h)))

  // flowchart_content = S_TOOLS.simplifyWithAcroDefinitions(flowchart_content);
  return S_TOOLS.simplify(flowchart_content) ;
};

function sortFlowOrder(f0, f1){
  let result = 0;
  if (f0.feature_data.flowchart_order > f1.feature_data.flowchart_order){
    result = 1;
  } else {
    result = -1;
  }
  return result;
}

exports.flowDescMPF = function(claim_flow_obj, client){

  // First, make a flow_obj for processing at flowDescription.
  // First two fields are the same, third is a list of features
  var flow_obj = [claim_flow_obj[0], claim_flow_obj[1]]

  // Add features from the claims
  var feature_list = [];
  var is_independent = true;
  claim_flow_obj[2].forEach(function(c){
      if(c.claim_features !== undefined){
      var features = c.claim_features.filter(f=>{
        return (f.feature_data.flowchart_order > 0);
      })
      feature_list = feature_list.concat(features);

      // Are features coming from a dependent claim? if they are, no need to print MPF
      if (features.length > 0){
        is_independent = is_independent && c.independent;
      }
    }
  })

  // Sort the features by requested order
  feature_list.sort(sortFlowOrder);
  flow_obj.push(feature_list);

  // Add labelling values
  flow_obj.push(claim_flow_obj[3])

  // Generate the Flow Description
  var flow_desc = exports.flowDescription(flow_obj, client);

  if (is_independent == false){
    return flow_desc;
  }

  // If not, we need to construct MPF support from the claims.
  // The independent claim is the LAST in this list, make it the first
  ind_clm_first = [claim_flow_obj[2][claim_flow_obj[2].length-1]]
  ind_clm_first.push(...claim_flow_obj[2].slice(0, claim_flow_obj[2].length-1))

  var MPF_desc = CLAIM_BUILDER.appClaimSummary(ind_clm_first, true, ['mpf'], claim_flow_obj[1]);

  return flow_desc + MPF_desc;
}


exports.blockDescription = function(block_type = "", device_group_raw = {}, label_group = {}, app_name = '', client = ''){
  // console.log(JSON.stringify(device_group_raw))
  // This is the returned object.

  // key labels for other figures in key value format
  var labels = B_TOOLS.tuplesToKeyValuePairs(label_group);

  // Parts of the active device objs / type check
  var device_group = B_TOOLS.cookDeviceGroup(device_group_raw);

  var device = {};
  var fig_hash = "";
  switch(block_type){
    case "MA":
      return generateBlockText(device_group.blocka, labels.block_ma, device_group, labels, "MA", client);
      break;
    case "MB":
      return generateBlockText(device_group.blockb, labels.block_mb, device_group, labels, "MB", client);
      break;
    case "MC":
      return generateBlockText(device_group.blockc, labels.block_mc, device_group, labels, "MC", client);
      break;
    case "HA":
      var block_text = "";
      if(device_group.blockh2 != undefined){
        device = device_group.blockh;
        device.device_name = "UE";
        block_text += generateBlockText(device, labels.block_h, device_group, labels, "HA", client);
        device = device_group.blockh2;
        device.device_name = "base station";
        block_text += generateBlockText(device, labels.block_h2, device_group, labels, "HA", client);
      } else {
        block_text = generateBlockText(device_group.blockh, labels.block_h, device_group, labels, "HA", client);
      }

      return block_text;
      break;
  }

  return generateBlockText(device, fig_hash, device_group, labels, block_type);
};




function generateBlockText(device, fig_hash, device_group, labels, block_type, client){

  // specific device parts
  var block_text = "";

  var fig_label_source = X_TOOLS.refSource('Figure', fig_hash, {bold: true});
  var fig_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_hash));
  var fig1_ref = '';
  var device_label = fig_label + device.text || "";

  // v2modules blm 2018-05-21
  var input_component = B_TOOLS.getComponentByTag(device, 'input');
  input_component.component_description = B_TOOLS.getTagDescription(input_component, 'input');
  // var primary_component = B_TOOLS.getComponentByTag(device, 'primary');
  var primary_component = B_TOOLS.getComponentByTag(device, 'primary');
  var output_component = B_TOOLS.getComponentByTag(device, 'output');
  output_component.component_description = B_TOOLS.getTagDescription(output_component, 'output');
  var static_components = B_TOOLS.getAllComponentsByTag(device_group.blockh, 'static');
  var feature_components = []; // B_TOOLS.getAllComponentsByTag(device_group.blockc, 'feature');

    // If a primary component is not defined, let's just call it a 'device'
    if(primary_component.component_name == undefined){
      primary_component.component_name = 'device';
      primary_component.text = device.text;
    }

  // global device parts
  var ma_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_ma));
  var mb_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_mb));
  var mc_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_mc));
  var ha_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_h));
  var h2_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_h2));

  if (device_group.blocka != undefined) {
    var ma_device_label = (ma_label + device_group.blocka.text);
    var ma_component_label = ma_label + device_group.blocka.primary_component.text;
  } else if (device_group.blockb) {
    var ma_device_label = (mb_label + device_group.blockb.text);
    var ma_component_label = mb_label + device_group.blockb.primary_component.text;
  }

  var mb_device_label = mb_label + ((typeof device_group.blockb !== 'undefined') ? device_group.blockb.text : '');
  var mc_device_label = mc_label + device_group.blockc.text;
  var ha_device_label = ha_label + ((typeof device_group.blockh !== 'undefined') ? device_group.blockh.text : '');
  var mb_component_label = mb_label + ((typeof device_group.blockb !== 'undefined') ? device_group.blockb.primary_component.text : '');
  var mc_component_label = mc_label + device_group.blockc.primary_component.text;
  var ha_component_label = ha_label + ((typeof device_group.blockh !== 'undefined') ? device_group.blockh.primary_component.text : '');

  var h2_component_label = h2_label + ((typeof device_group.blockh2 !== 'undefined') ? h2_label + device_group.blockh2.primary_component.text : '');

  // Identify transceiver module
  var transceiver_label = "";
  var transceiver_label2 = ";"
  var bus_label = "";
  static_components.forEach(function(c){
    if (c.component_name == "Transceiver") {
      transceiver_label = ha_label + c.text;
      transceiver_label2 = h2_label + c.text;
    }
    if (c.component_name == "Bus") {
      bus_label = fig_label + c.text;
    }
  });

  // Special labelling for the "UE, base station" device
  var device_name = device.device_name || "";
  var hw_ref_text = `${primary_component.component_name} ${ha_component_label} described herein.`// with reference to FIG. {F#Transceiver}.`
  if (device_name == 'UE, base station'){
    device_name = 'wireless device';
    fig1_ref = 'UE 115 or base station 105';
    h2_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_h2));
    hw_ref_text = `${primary_component.component_name} ${ha_component_label} or ${h2_component_label} as described herein.` // with reference to FIGs. {F#Transceiver} and {F#Transceiver2}.`
  } else if (device_name == 'UE') {
    fig1_ref = 'UE 115';
  } else if (device_name == 'base station') {
    fig1_ref = 'base station 105';
  } else{
    fig1_ref = device_name;
  }

  // Special handling for language preferred by Micron attorneys
  var aspects = 'aspects of the present disclosure'
  if (client == 'Micron'){
    aspects = 'examples as disclosed herein'
  }

  // primary_module.module_name = device_name + ' ' + primary_module.module_name;
  var cap_primary_module_name = S_TOOLS.capitalize(primary_component.component_name);
  var cap_device_name = S_TOOLS.capitalize(device_name);
  // For the combined "UE, base station" device, filter away unneeded modules:
  if(device_name == 'UE' || device_name == 'base station'){
    these_components = []
    for(var m in static_components){
      if ((device_name == 'UE' && ['Inter-station Communications Manager', 'Network Communications Manager'].indexOf(static_components[m].component_name) < 0) ||
        (device_name == 'base station' && static_components[m].component_name != 'I/O Controller')){
        these_components.push(static_components[m]);
      }
    }
    static_components = these_components;
  }

  // block type specific operations
  switch(block_type){
    case "MA":
      feature_components = B_TOOLS.getAllComponentsByTag(device_group.blockc, 'feature');
      var partitioned_components = B_TOOLS.partitionComponents(feature_components);
      // Header
      device_name = "device"
      block_text += X_TOOLS.p(X_TOOLS.r(`FIG. `, {bold: true}) + fig_label_source + X_TOOLS.r(` shows a block diagram ${fig_label}00 of a ${device_name} ${device_label} that supports {TITLE.LCASE} in accordance with aspects of the present disclosure. The ${device_name} ${device_label} may be an example of aspects of a ${fig1_ref} as described herein. The ${device_name} ${device_label} may include ${B_TOOLS.getComponentsNameList([input_component, primary_component, output_component], fig_label)}. The ${device_name} ${device_label} may also include a processor. Each of these components may be in communication with one another (e.g., via one or more buses).`), NUM_PAR_PROP);

      // Module Specific
      if (input_component.component_description != undefined){
        block_text += X_TOOLS.p(X_TOOLS.r(input_component.component_description), NUM_PAR_PROP);
      }
      
      var component_description = `The ${primary_component.component_name} ${fig_label}${primary_component.text} may ${B_TOOLS.writeDescriptionsList(B_TOOLS.formComponentList(partitioned_components[0], true))}`;

      for(var m = 1; m < partitioned_components.length; m++) {
        component_description += `The ${primary_component.component_name} ${fig_label}${primary_component.text} may also ${B_TOOLS.writeDescriptionsList(B_TOOLS.formComponentList(partitioned_components[m], true))}`;
      }
      component_description += `The ${primary_component.component_name} ${fig_label}${primary_component.text} may be an example of aspects of the ` + hw_ref_text;

      block_text += X_TOOLS.p(X_TOOLS.r(component_description), NUM_PAR_PROP);

      // block_text += X_TOOLS.p(X_TOOLS.r(`The ${primary_module.module_name} ${fig_label}${primary_module.text} may be an example of aspects of the ` + hw_ref_text), NUM_PAR_PROP);

      block_text += X_TOOLS.p(X_TOOLS.r(`The ${primary_component.component_name} ${fig_label}${primary_component.text}, or its sub-components, may be implemented in hardware, code (e.g., software or firmware) executed by a processor, or any combination thereof. If implemented in code executed by a processor, the functions of the ${primary_component.component_name} ${fig_label}${primary_component.text}, or its sub-components may be executed by a general-purpose processor, a digital signal processor (DSP), an application-specific integrated circuit (ASIC), a field-programmable gate array (FPGA) or other programmable logic device, discrete gate or transistor logic, discrete hardware components, or any combination thereof designed to perform the functions described in the present disclosure.`), NUM_PAR_PROP);
      block_text += X_TOOLS.p(X_TOOLS.r(`The ${primary_component.component_name} ${fig_label}${primary_component.text}, or its sub-components, may be physically located at various positions, including being distributed such that portions of functions are implemented at different physical locations by one or more physical components. In some examples, the ${primary_component.component_name} ${fig_label}${primary_component.text}, or its sub-components, may be a separate and distinct component in accordance with various aspects of the present disclosure. In some examples, the ${primary_component.component_name} ${fig_label}${primary_component.text}, or its sub-components, may be combined with one or more other hardware components, including but not limited to an input/output (I/O) component, a transceiver, a network server, another computing device, one or more other components described in the present disclosure, or a combination thereof in accordance with various aspects of the present disclosure.`), NUM_PAR_PROP);

      if (output_component.component_description != undefined){
        block_text += X_TOOLS.p(X_TOOLS.r(output_component.component_description), NUM_PAR_PROP);
      }
      block_text = block_text.replace(/{#Transceiver}/g, transceiver_label)
                             .replace(/{F#Transceiver}/g, ha_label)
                             .replace(/{#Transceiver2}/g, transceiver_label2)
                             .replace(/{F#Transceiver2}/g, h2_label);
      
      block_text = S_TOOLS.simplify(block_text);
      break;

    case "MB":
      feature_components = B_TOOLS.getAllComponentsByTag(device_group.blockb, 'feature');
      // Header
      if (labels.block_ma != undefined){
        var ma_tieback = `The ${primary_component.component_name} ${fig_label}${primary_component.text} may be an example of aspects of the ${primary_component.component_name} ${ma_component_label} as described herein.` // with reference to FIG. ${ma_label}.`
        if(device.device_name != 'UE, base station'){ 
          var wrt = ` The device ${device_label} may be an example of aspects of a device ${ma_device_label}, or a ${device_name} 115 as described herein.`; // with reference to FIGs. 1 and ${ma_label}.`;
        } else {
          var wrt = ` The device ${device_label} may be an example of aspects of a device ${ma_device_label}, a UE 115, or a base station 105 as described herein.`; // with reference to FIGs. 1 and ${ma_label}.`;
        }
      } else {
        var ma_tieback= ''
        var wrt = '';
      }
      
      // For Salesforce Apps, we went an 'apparatus' here
      device_name = "device"
      if (client == 'Salesforce'){
        device_article = 'an'
        device_type = 'apparatus'
        sf_text = ` In some cases, the apparatus ${device_label} may be an example of a user terminal, a database server, or a system containing multiple computing devices.`
        sf_boilerplate = `The ${primary_component.component_name} ${fig_label}${primary_component.text} and/or at least some of its various sub-components may be implemented in hardware, software executed by a processor, firmware, or any combination thereof. If implemented in software executed by a processor, the functions of the ${primary_component.component_name} ${fig_label}${primary_component.text} and/or at least some of its various sub-components may be executed by a general-purpose processor, a digital signal processor (DSP), an application-specific integrated circuit (ASIC), a field-programmable gate array (FPGA) or other programmable logic device, discrete gate or transistor logic, discrete hardware components, or any combination thereof designed to perform the functions described in the present disclosure. The ${primary_component.component_name} ${fig_label}${primary_component.text} and/or at least some of its various sub-components may be physically located at various positions, including being distributed such that portions of functions are implemented at different physical locations by one or more physical devices. In some examples, the ${primary_component.component_name} ${fig_label}${primary_component.text} and/or at least some of its various sub-components may be a separate and distinct component in accordance with various aspects of the present disclosure. In other examples, the ${primary_component.component_name} ${fig_label}${primary_component.text} and/or at least some of its various sub-components may be combined with one or more other hardware components, including but not limited to an I/O component, a transceiver, a network server, another computing device, one or more other components described in the present disclosure, or a combination thereof in accordance with various aspects of the present disclosure. `
      } else {
        device_article = 'a'
        device_type = 'device'
        sf_text = ''
        sf_boilerplate = ''
      }

      block_text = X_TOOLS.p(X_TOOLS.r(`FIG. `, {bold: true})+fig_label_source+X_TOOLS.r(` shows a block diagram ${fig_label}00 of ${device_article} ${device_type} ${device_label} that supports {TITLE.LCASE} in accordance with aspects of the present disclosure.${wrt} The ${device_type} ${device_label} may include ${B_TOOLS.getComponentsNameList([input_component, primary_component, output_component], fig_label)}. The ${device_type} ${device_label} may also include a processor. Each of these components may be in communication with one another (e.g., via one or more buses).` + sf_text), NUM_PAR_PROP);
      
      // Module Specific
      if (input_component.component_description != undefined){
        block_text += X_TOOLS.p(X_TOOLS.r(input_component.component_description), NUM_PAR_PROP);
      }

      // Salesforce likes to reference the C and HA diagrams
      if(client == 'Salesforce'){
        hw_ref_text = `${primary_component.component_name} ${mc_device_label} or ${ha_component_label} described with reference to FIGs. ${mc_label} and ${ha_label}.` 
      }

      block_text += X_TOOLS.p(X_TOOLS.r(`${ma_tieback} The ${primary_component.component_name} ${fig_label}${primary_component.text} may include ${B_TOOLS.getComponentsNameList(feature_components, fig_label, true)}. The ${primary_component.component_name} ${fig_label}${primary_component.text} may be an example of aspects of the ` + hw_ref_text), NUM_PAR_PROP);

      if(sf_boilerplate.length > 0){
        block_text += X_TOOLS.p(X_TOOLS.r(sf_boilerplate), NUM_PAR_PROP)
      }

      block_text += B_TOOLS.writeDescriptionsParagraph(B_TOOLS.formComponentList(B_TOOLS.aggregateComponents(feature_components), true, false), fig_label);

      if (output_component.component_description != undefined){
        block_text += X_TOOLS.p(X_TOOLS.r(output_component.component_description), NUM_PAR_PROP);
      }

      block_text = block_text.replace(/{#Transceiver}/g, transceiver_label)
                             .replace(/{F#Transceiver}/g, ha_label)
                             .replace(/{#Transceiver2}/g, transceiver_label2)
                             .replace(/{F#Transceiver2}/g, h2_label);
      
      if (labels.block_ma != undefined){
        block_text = S_TOOLS.simplify(block_text)
      } else {
        block_text = S_TOOLS.simplifyWithAcroDefinitions(block_text);
      }
      break;

    case "MC":
      feature_components = B_TOOLS.getAllComponentsByTag(device_group.blockc, 'feature');
      // Header

      // Change the 'with reference to' text depending on what other figures are defined, and whether or not primary component is defined
      if (labels.block_ma != undefined){
        var wrt = `${ma_component_label}, a ${primary_component.component_name} ${mb_component_label}, or a ${primary_component.component_name} ${ha_component_label} described herein.`;
      } else if(labels.block_mb != undefined) {
        var wrt = `${mb_component_label} or a ${primary_component.component_name} ${ha_component_label} described herein.`;
      } else {
        if (primary_component.component_name != 'device'){
          var wrt = `or a ${primary_component.component_name} ${ha_component_label} described herein.`;
        } else {
          var wrt = `${ha_device_label} described herein.`;
        }
      }

      // Really we should be detecting if the H figure is defined, not the client type
      if (client == 'Micron'){
        wrt = 'as described with reference to FIGs [[XX through YY]].';
        primary_component.component_name = device_name;
      }

      block_text = X_TOOLS.p(X_TOOLS.r(`FIG. `, {bold: true})+fig_label_source+X_TOOLS.r(` shows a block diagram ${fig_label}00 of a ${primary_component.component_name} ${fig_label}${device.text} that supports {TITLE.LCASE} in accordance with ${aspects}. The ${primary_component.component_name} ${fig_label}${device.text} may be an example of aspects of a ${primary_component.component_name} ${wrt} The ${primary_component.component_name} ${fig_label}${device.text} may include ${B_TOOLS.getComponentsNameList(feature_components, fig_label)}. Each of these modules may communicate, directly or indirectly, with one another (e.g., via one or more buses).`), NUM_PAR_PROP);

      block_text += B_TOOLS.writeComponentParagraphs(B_TOOLS.formComponentList(B_TOOLS.aggregateComponentsOld(feature_components), false, true), fig_label);
      block_text = S_TOOLS.simplify(block_text);
      break;

    case "HA":
      feature_components = B_TOOLS.getAllComponentsByTag(device_group.blockc, 'feature');
      var partitioned_components = B_TOOLS.partitionComponents(feature_components);
      var component_description = `The ${primary_component.component_name} ${fig_label}${primary_component.text} may ${B_TOOLS.writeDescriptionsList(B_TOOLS.formComponentList(partitioned_components[0], true))}`;

      for(var m = 1; m < partitioned_components.length; m++) {
        component_description += `The ${primary_component.component_name} ${fig_label}${primary_component.text} may also ${B_TOOLS.writeDescriptionsList(B_TOOLS.formComponentList(partitioned_components[m], true))}`;
      }

      // Header
      device_name = "device"
      if (labels.block_ma != undefined){
        var wrt = `The device ${device_label} may be an example of or include the components of ${device_name} ${ma_device_label}, ${device_name} ${mb_device_label}, or a ${fig1_ref} as described herein.`;
      } else if (labels.block_mb != undefined) {
        var wrt = `The device ${device_label} may be an example of or include the components of ${device_name} ${mb_device_label} as described herein.`
      } else {
        var wrt = '';
      }
      var short_list = static_components.filter(m=>{
        return (m.component_name != 'Software' && m.component_name != 'software')
      })

      // If it's a salesforce app, only data is transmitted, and tehre might be an apparatus
      var data_comms_type= 'voice and data'
      if(client == 'Salesforce'){
        device_name = `a ${device.device_name} or an apparatus`
        data_comms_type = 'data'
      }

      // If this is not salesforce or QC, there is no transmitting.
      var comms_components = ''
      if(['Salesforce', 'Qualcomm'].indexOf(client) > -1){
        comms_components = `components for bi-directional ${data_comms_type} communications including components for transmitting and receiving communications, including`
      }

      block_text = X_TOOLS.p(X_TOOLS.r(`FIG. `, {bold: true})+fig_label_source+X_TOOLS.r(` shows a diagram of a system ${fig_label}00 including a device ${device_label} that supports {TITLE.LCASE} in accordance with aspects of the present disclosure. ${wrt} The device ${fig_label}05 may include ${comms_components} ${B_TOOLS.getComponentsNameList([primary_component, ...short_list], fig_label)}. These components may be in electronic communication via one or more buses (e.g., bus ${bus_label}).`), NUM_PAR_PROP);

      if(device.component_name == 'UE'){
        block_text += X_TOOLS.p(X_TOOLS.r(`Device ${device_label} may communicate wirelessly with one or more base stations 105.`), NUM_PAR_PROP);
      }else if(device.component_name == 'base station'){
        block_text += X_TOOLS.p(X_TOOLS.r(`Device ${device_label} may communicate wirelessly with one or more UEs 115.`), NUM_PAR_PROP);
      }
      
      // Primary module description: Salesforce form is different from QC
      if(client == 'Salesforce'){
        block_text += X_TOOLS.p(X_TOOLS.r(`The ${primary_component.component_name} ${ha_component_label} may be an example of a ${primary_component.component_name} ${mb_component_label} or ${mc_device_label} as described herein. For example, the ${primary_component.component_name} ${ha_component_label} may perform any of the methods or processes described above with reference to FIGs. ${mb_label} and ${mc_label}. In some cases, the ${primary_component.component_name} ${ha_component_label} may be implemented in hardware, software executed by a processor, firmware, or any combination thereof.`), NUM_PAR_PROP)
      } else {
        block_text += X_TOOLS.p(X_TOOLS.r(component_description), NUM_PAR_PROP)
      }

      function blockTxt(s){
        if (s.component_name !== "Bus"){
          static_text = [];
          s.component_tags.forEach(function(t){
            if(t.tag_name === 'static' && t.tag_description && t.tag_description.length > 0) {
              block_text += X_TOOLS.p(X_TOOLS.r(t.tag_description), NUM_PAR_PROP);
            }
          });
        }
        if (s.tchildren && s.tchildren.length){
          s.tchildren.forEach(blockTxt);
        }
      }

      // Static module commentary gets inserted here:
      static_components.forEach(blockTxt);

      block_text = S_TOOLS.simplify(block_text);
      break;
  }

  block_text = block_text.replace(/{#UE}/g, device_label)
                         .replace(/{#BS}/g, device_label)
                         .replace(/{#device}/g, device_label)
                         .replace(/{FigRef}/g, fig_label);
  return block_text;
} 


exports.figure1BriefDescription = function(fig_ref, client){
  var fig_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_ref));
  var aspects = 'aspects of the present disclosure.';
  if (client == 'Micron'){
    aspects = 'examples as disclosed herein.';
  }

  description = X_TOOLS.p(X_TOOLS.r(`FIG. ${fig_label} illustrates an example of a system for {PREAMBLE} that supports {TITLE.LCASE} in accordance with ${aspects}`), NUM_PAR_PROP);
  return description;
}

exports.narrativeBriefDescription = function(fig_ref, client){
  var fig_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fig_ref));
  var aspects = 'aspects of the present disclosure.';
  if (client == 'Micron'){
    aspects = 'examples as disclosed herein.';
  }

  description = X_TOOLS.p(X_TOOLS.r(`FIG. ${fig_label} illustrates an example of a **DESCRIPTION${fig_ref}** that supports {TITLE.LCASE} in accordance with ${aspects}`), NUM_PAR_PROP);
  return description;
}

exports.abchBriefDescription = function(ref_list, use_a=true, use_b=true, use_h=true){
  var labels = B_TOOLS.getLabelList(ref_list[1]);
  if (use_a == true){
    var ma_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_ma));        
  } else {
    var ma_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_mb));   
  }

  var mb_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_mb));
  var mc_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_mc));
  var ha_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_h));
  var h2_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', labels.block_h2));

  description = '';

  if (use_b == true){
    if (ma_label != mb_label){
      description += X_TOOLS.p(X_TOOLS.r(`FIGs. ${ma_label} and ${mb_label} show block diagrams of devices that support {TITLE.LCASE} in accordance with aspects of the present disclosure.`), NUM_PAR_PROP);
    } else {
      description += X_TOOLS.p(X_TOOLS.r(`FIG. ${ma_label} shows a block diagram of an apparatus that supports {TITLE.LCASE} in accordance with aspects of the present disclosure.`), NUM_PAR_PROP); 
    }
  }
// TODO! has PRIMARY_MODULE keyword changed to PRIMARY_COMPONENT ??

  description += X_TOOLS.p(X_TOOLS.r(`FIG. ${mc_label} shows a block diagram of a {PRIMARY_MODULE} that supports {TITLE.LCASE} in accordance with aspects of the present disclosure.`), NUM_PAR_PROP);

  if(use_h == true){
    if(ref_list[0] == 'UE, base station'){
      description += X_TOOLS.p(X_TOOLS.r(`FIG. ${ha_label} shows a diagram of a system including a UE that supports {TITLE.LCASE} in accordance with aspects of the present disclosure.`), NUM_PAR_PROP);
      description += X_TOOLS.p(X_TOOLS.r(`FIG. ${h2_label} shows a diagram of a system including a base station that supports {TITLE.LCASE} in accordance with aspects of the present disclosure.`), NUM_PAR_PROP);
    } else {
      description += X_TOOLS.p(X_TOOLS.r(`FIG. ${ha_label} shows a diagram of a system including a device that supports {TITLE.LCASE} in accordance with aspects of the present disclosure.`), NUM_PAR_PROP);
    }
  }

  // return description
  return S_TOOLS.simplifyWithAcroDefinitions(description);
}

exports.flowBriefDescription = function(fc_ref, client){
  if (fc_ref.length == 0){ return ''; }

  var aspects = 'aspects of the present disclosure.';
  if (client == 'Micron'){
    methods = 'a method or methods';
    aspects = 'examples as disclosed herein.';
  } else{
    methods = 'methods';
  }

  var description = '';
  var first_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fc_ref[0][0]));
  if (fc_ref.length == 1){
    description = X_TOOLS.p(X_TOOLS.r(`FIG. ${first_label} shows a flowchart illustrating ${methods} that support {TITLE.LCASE} in accordance with ${aspects}`), NUM_PAR_PROP);
  } else if (fc_ref.length == 2) {
    var second_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fc_ref[1][0]));
    description = X_TOOLS.p(X_TOOLS.r(`FIGs. ${first_label} and ${second_label} show flowcharts illustrating ${methods} that support {TITLE.LCASE} in accordance with ${aspects}`), NUM_PAR_PROP);
  } else {
    var second_label = X_TOOLS.injectRun(X_TOOLS.refTarget('Figure', fc_ref[fc_ref.length-1][0]));
    description = X_TOOLS.p(X_TOOLS.r(`FIGs. ${first_label} through ${second_label} show flowcharts illustrating ${methods} that support {TITLE.LCASE} in accordance with ${aspects}`), NUM_PAR_PROP);
  }

  return description;
}

exports.dynamicFigureDescription = function(device) {
  const wt = new W_TOOLS.WordTools();
  const pb = new wt.ParagraphBuilder(require('./views/micron').dynamic_figure_description);
  return pb.toXML(device);
};

// create a simple brief description for dynamic figures
exports.dynamicFigureBriefDescription = function(device) {
  const wt = new W_TOOLS.WordTools();
  const pb = new wt.ParagraphBuilder(require('./views/micron').dynamic_figure_brief_description);
  return pb.toXML(device);
};
