exports.parts = {
  'afcp': {
		header: 'After Final Consideration Pilot 2.0 (AFCP 2.0)',
		text: 	'Applicant submits that the above amendments to the claims place the application in condition for allowance.  Furthermore, the amendments can be determined to place the application in condition for allowance with only a limited amount of further searching or consideration.  Therefore, Applicant requests that the Examiner consider these amendments under the AFCP 2.0. If the Examiner declines the request to consider this response under the AFCP 2.0, Applicant requests that the Examiner notify Applicant of this decision.',
	},
	'interview': {
		header: 'Examiner Interview',
		text: 	'Applicant thanks Examiner {EXAMINER} for granting a telephonic interview to discuss this Application. On Mos. DD, YYYY, Applicant’s representatives conducted an interview of Examiner {EXAMINER}.  During the interview, (possible/the present) amendments to claim ____ were discussed; and: [[Example (1): an agreement was reached as to amendments that would overcome all rejections under 35 U.S.C. § 101 and § 112 made in the Office Action. The agreed-upon amendments are reflected herein The Examiner asked, and Applicant agreed to submit a response to the Office Action with a request to participate in AFCP 2.0, and this response is submitted accordingly.]] [[Example (2): No final agreement was reached.  The Examiner indicated that a search for new art would be conducted in light of the present amendments, but suggested that the present amendments overcome the cited art.]]',
	},
}
