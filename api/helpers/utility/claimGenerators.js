const S_TOOLS = require("./tools/stringTools.js");
const CLAIM_TOOLS = require('./tools/claimTools.js');
const CLAIM_LANG = require("../language/claimLanguage.js");

exports.generateClaimCollection = function(claims = [], types = [], preamble = ""){
  claim_collection = {};
  claim_sequence = ['method', 'APP', 'MPF', 'CRM', 'CTR', 'DEV']

  var claim_count = {count: 0};
  claim_sequence.forEach(function(type){
    if (types.indexOf(type) >= 0){
      type = type.toLowerCase();
      claim_collection[type] = exports.generateClaims(claims, claim_count, type, preamble, 0, true);
    }
  })

  return JSON.stringify(claim_collection);
}

exports.generateClaims = function(claims, claim_count, type, preamble, parent, independent){
  var claim_group = [];
  if(claims && claims.length > 0){
    claims.forEach(function(claim){
      if(claim.claim_type && claim.claim_type.toLowerCase() == type && !claim.claim_generated){
        claim_group.push(claim);
      }else{
        if(claim.claim_type && claim.claim_type.toLowerCase() == "method" && !claim.claim_generated){
          if(exports.claimHasType(claim, type)){
            claim_group.push(exports.generateClaim(claim, claim_count, type, preamble, parent, independent));
          }
        }
      }
    })
  }
  return claim_group;
}

exports.generateClaim = function(claim, claim_count, type, preamble, parent, independent){
  // Introduce a try-catch to search for errors in claim-gen
  try {


    if(claim.claim_features != undefined){
      claim.claim_features = claim.claim_features.filter(f=>{
        return f.feature_text != undefined
      })
    }
    // If it's a wherein claim, with there's only one feature and no "comprises"
    // in the feature, make this an extended preamble.
    if(claim.claim_features.length == 1 &&
       claim.claim_preamble.indexOf("wherein") >= 0 &&
       claim.claim_preamble.indexOf("comprises") < 0 &&
       claim.claim_features[0].feature_text.indexOf("comprises") < 0){
      if(claim.claim_preamble.slice(-1) == ':'){
        claim.claim_preamble = claim.claim_preamble.slice(0,-1);
      }
      claim.claim_preamble += ' ' + claim.claim_features[0].feature_text;
      claim.claim_features = [];
    }
    

    claim_count.count++;
    var claim_num = claim_count.count;
    var new_claim = {
      item_num: claim_count.count,
      claim_device: claim.claim_device || "",
      claim_type: type,
      claim_preamble: "",
      claim_features: [],
      independent: independent,
      status: "Original",
      claim_generated: true,
      tchildren: exports.generateClaims(claim.tchildren, claim_count, type, preamble, claim_num, false)
    };
    var claim_trans = "";
    if(!independent){

      var type_info = CLAIM_LANG.claim_type_list[type] || CLAIM_LANG.claim_type_list['default'];
      if(claim.claim_preamble && claim.claim_preamble.indexOf("comprises") > -1){
        // Dependent – Clarifying a Feature, and using clarified feature in a new operation
        if(claim.claim_preamble.indexOf("comprising") > -1){
          claim_trans = ","+claim.claim_preamble.split(",")[1]+", "+type_info.feature_trans;
        // Dependent Adding an Operation
        }else{
          var trans_start = type_info.complex_trans_start || '';
          var trans_end = type_info.complex_trans_end || '';
          var feature_part = claim.claim_preamble.split("comprises:")[0].split("wherein")[1].trim();
          if(type == "mpf"){
            if(exports.activeFeature(feature_part)){
              claim_trans = trans_start + feature_part + " comprises:";
            } else {
              claim_trans = ', wherein ' + feature_part + " comprises:";
            }
          }else if(type == "method" || type == "dev"){
            claim_trans = ", wherein " + feature_part + " comprises: ";
          }else{
            var first_word = feature_part.split(" ")[0];
            if(first_word.indexOf("ing") > -1){
              feature_part = feature_part.replace(/^\S+/g, S_TOOLS.infinitive(first_word));
            }
            claim_trans = trans_start + feature_part + " comprise " + trans_end;
            if(type == "app" || type == "crm" || type == "ctr"){
              if(exports.activeFeature(feature_part)){
                claim_trans = trans_start + feature_part + trans_end;
              } else {
                claim_trans = ', wherein ' + feature_part + ' comprises ';
              }
            }
          }
        }
      // Patch fix for special Salesforce claim generation
      }else if(claim.claim_preamble && claim.claim_preamble.indexOf(', the method further comprising:') > -1){
        var feature_part = claim.claim_preamble.split(', the method further comprising:')[0].split("wherein")[1].trim();
        var claim_trans = ', wherein ' + feature_part + ', ' + type_info.feature_trans
      // Dependent – Clarifying an Operation
      }else{
        if(claim.claim_features && claim.claim_features.length > 0){
          if(claim.claim_preamble.indexOf("wherein") >= 0 && ['mpf', 'app', 'method', 'crm', 'ctr', 'dev'].indexOf(type) >= 0){
            claim_trans = ", wherein:"
          } else{
            claim_trans = exports.claimTrans(type);
          }
        }else{
          claim_trans = ","+claim.claim_preamble.split(/,(.+)/)[1];
        }
      }
    }else{
      if(CLAIM_LANG.claim_type_list[type] && CLAIM_LANG.claim_type_list[type].standard_features){
        for(var f in CLAIM_LANG.claim_type_list[type].standard_features){
          new_claim.claim_features.push({feature_text: CLAIM_LANG.claim_type_list[type].standard_features[f]});
        }
      }
    }

    preamble = CLAIM_TOOLS.extractSimplePreamble(claim.claim_preamble)
    new_claim.claim_preamble = exports.claimBegin(type, preamble, parent) + (!independent ? parent + claim_trans : '');
    if(new_claim.claim_preamble.indexOf("wherein") >= 0){
      var wherein_claim = true;
    } else {
      var wherein_claim = false;
    }

    var features = claim.claim_features || [];
    features.forEach(function(feature, index){
      if(feature.feature_text.slice(0,1) == ' '){
        feature.feature_text = feature.feature_text.slice(1);
      }
      var text = feature.feature_text ? exports.conjugateType(feature.feature_text, type, wherein_claim) : '';
      var feature_type = (exports.activeFeature(text, type)) ? 'active': 'passive';
      var trans = "";
      var punct = exports.punctuation(index, features.length);
      var f_content = trans + text; //+ punct;
      new_claim.claim_features.push({feature_text: f_content});
    })
    return new_claim;
  }catch(err){
    var new_claim = {
      item_num: claim_count.count,
      claim_device: "",
      claim_type: type,
      claim_preamble: `CLAIM GENERATOR: LANGUAGE ERROR IN CLAIM ${claim.item_num}. Maybe missing a 'wherein', 'comprises' or both? (${err})`,
      claim_features: [],
      independent: independent,
      status: "Original",
      claim_generated: true,
      tchildren: []
    };
    return new_claim;
  }
}

////////////////////////////////////////////////////////////////////////////////

exports.claimTrans = function(type){
  var claim_trans = '';
  var type_info = CLAIM_LANG.claim_type_list[type] || CLAIM_LANG.claim_type_list['default'];
  claim_trans = type_info.trans || ', further comprising:';
  return claim_trans;
}

exports.claimBegin = function(type, preamble, parent) {
  var claim_begin = '';
  var claim_type_info = CLAIM_LANG.claim_type_list[type] || CLAIM_LANG.claim_type_list['default'];
  var claim_noun = claim_type_info.noun;
  if (parent){
    claim_begin = CLAIM_LANG.claim_type_list.dependent.begin[0];
  }else{
    var type_begin_list = claim_type_info.begin || [];
    // type_begin_list.forEach(function(component, index) {
    //  claim_begin += component;
    // })
    claim_begin += type_begin_list[0];
  }
  claim_begin = claim_begin
    .replace('{PREAMBLE}', (preamble ? '' + preamble : '') )
    // .replace('{PREAMBLE}', (preamble ? ' for ' + preamble : '') )
    .replace('{CLAIM_TYPE}', claim_noun)
  return claim_begin;
}

exports.activeClaim = function(claim, type){
  var first_feature = (claim && claim.claim_features) ? claim.claim_features[0] : {};
  if (first_feature && exports.activeFeature(first_feature.feature_text, type)){
    return true;
  }else{
    return false;
  }
}

exports.activeFeature = function(text, type) {
  var articles = ['a', 'the', 'an', 'each','one', 'at', 'another'];
  var words = text ? text.split(' ') : [''];
  var first_word = (typeof words[0] == 'string') ? words[0].toLowerCase() : '';
  if ( (type == 'dev' || type == 'form' || type == 'sys' || type == 'app' || type == 'ctr' || type == 'crm') && first_word != 'the' && first_word != 'each' && first_word != 'a' && first_word != 'an'){
    return true;
  }else if (first_word != '' && (S_TOOLS.articles.indexOf(first_word) < 0 || first_word.length > 5) && text.indexOf('es:') < 0 ){
    return true;
  }else{
    return false;
  }
}

exports.punctuation = function(index, length, separator){
  separator = separator ? separator : ';';
  var punctuation = '';
  if (index == length - 1){
    punctuation = '.';
  }else if(index == length - 2 ){
    punctuation = separator + ' and';
  }else{
    punctuation = separator;
  }
  return punctuation;
}

exports.conjugateType = function(text, type, wherein_claim){
  if(!exports.activeFeature(text, type)){
    return text;
  }else if(wherein_claim && exports.actionComprisingAction(text, false, type)){
    return exports.actionComprisingAction(text, true, type);
  }else if (type == 'method'){
    return S_TOOLS.gerundSentence(text)
  }else if (type == 'mpf'){
    return 'means for ' + S_TOOLS.gerundSentence(text)
  }else if (type == 'form'){
    return 'forming ' + text
  }else{
    return S_TOOLS.infinitiveSentence(text)
  }
}

exports.claimHasType = function(claim, type){
  if(claim.claim_data && claim.claim_data.generate_options){
    for(var opt in claim.claim_data.generate_options){
      if(claim.claim_data.generate_options[opt].gen_name.toLowerCase() == type.toLowerCase()){
        return claim.claim_data.generate_options[opt].gen_value;
      }
    }
  }
  return true;
}

exports.actionComprisingAction = function(text, generate_language, type){
  var first_part = text.split('comprises')[0];
  var second_part = text.split('comprises')[1];
  if(!generate_language){
    if(first_part == undefined || second_part == undefined){
      return false;
    }
    if(first_part.split(' ')[0].slice(-3) == 'ing' && second_part.trim().split(' ')[0].slice(-3) == 'ing'){
      return true;
    }else{
      return false;
    }
  }else if(type == 'mpf') {
    return "the means for " + first_part + "comprises means for " + second_part.trim();
  }else if(type == 'app' || type == 'crm') {
    return "the instructions to " + S_TOOLS.infinitiveSentence(first_part) +
         "are executable by the processor to cause the apparatus to " + S_TOOLS.infinitiveSentence(second_part.trim());
  }else if(type == 'ctr') {
    return "the instructions to " + S_TOOLS.infinitiveSentence(first_part) +
         "are executable by the processor to cause the controller to " + S_TOOLS.infinitiveSentence(second_part.trim());
  }else if(type == 'summary'){
    return S_TOOLS.gerundSentence(first_part) + "may include operations, features, means, or instructions for " +
           S_TOOLS.gerundSentence(second_part.trim());
  }else{
    return first_part + "comprises " + second_part.trim();
  }
}
