const AF_CONF = require("../../configData/applicationFigureConfig.js");

exports.generateFigureList = function(task){
  var figure_list = {};
  if(task.task_output){
    figure_list.naratives = exports.getNarativeFigures(task.task_output);
    figure_list.blocks = exports.getBlockFigures(task.task_output);
    figure_list.flowcharts = exports.getFlowFigures(task.task_output);
  }
  return figure_list;
}

exports.getNarrativeFigures = function(application){
  if(application && application.nexus_data){
    var narrative_count = application.nexus_data.num_narative_figures || 0;
    return Array(narrative_count).fill({});
  }else{
    return [];
  }
}

// combines figure data in way that is meaningful for creating and describing figures 
exports.getBlockFigures = function(application){
  if(application && application.nexus_data){
    var template_info = AF_CONF.template_list["DEFAULT"];
    if(application.template_type && AF_CONF.template_list[application.template_type.toUpperCase()]){
      template_info = AF_CONF.template_list[application.template_type.toUpperCase()];
    }
    var input_module = template_info.input_module || "";
    var primary_module = application.nexus_data.primary_module || template_info.primary_module;
    var output_module = template_info.output_module || "";
    var blocks = template_info.blocks;
    
    var block_figures = []; // contains objs that describe each block figure
    var devices = []; // keeps tack of which devices already have figures
    if(application.app_claims && application.app_claims.length > 0){
      for(var c in application.app_claims){
        var device = application.app_claims[c].claim_device;
        if(!devices.includes(device)){
          devices.push(device);
          // combine template and device hardware modules into one list
          var device_info = AF_CONF.device_list["DEFAULT"];
          if(device && AF_CONF.device_list[device.toUpperCase()]){
            device_info = AF_CONF.device_list[device.toUpperCase()];
          }
          var hardware_modules = template_info.hardware_modules.concat(device_info.filter(function (item) {
            return template_info.hardware_modules.indexOf(item) < 0;
          }));
          // add block diagrams based on template config
          for(var b in blocks){
            var block_figure_obj = {
              block_type: blocks[b],
              blocks,
              input_module,
              primary_module,
              output_module,
              hardware_modules,
              device,
            }
            switch(blocks[b].toUpperCase()){
              case "MB":
                // get modules of independent claims
                var modules = [];
                if(application.app_claims[c].claim_features && application.app_claims[c].claim_features.length > 0){
                  for(var f in application.app_claims[c].claim_features){
                    var feature_module = application.app_claims[c].claim_features[f].feature_module;
                    if(feature_module){
                      if(!module.includes(feature_module)){
                        modules.push(feature_module);
                      }
                    }
                  }
                }
                block_figure_obj.modules = modules;
                break;
              case "MC":
                // get modules of independent claims and their children
                var modules = [];
                getDepModules(application.app_claims[c]);
                function getDepModules(dep_claims){
                  for(var d in dep_claims){
                    for(var f in dep_claims[d].claim_features){
                      var feature_module = dep_claims[d].claim_features[f].feature_module;
                      if(feature_module){
                        if(!modules.includes(feature_module)){
                          modules.push(feature_module);
                        }
                      }
                    }
                    if(dep_claims.tchilden && dep_claims.tchilden.length > 0){
                      getModules(dep_claims.tchilden);
                    }
                  }
                }
                block_figure_obj.modules = modules;
                break;
              default:
                
            }
            block_figures.push(block_figure_obj);
          }
        }
      }
      return block_figures;
    }else{
      return [];
    }
  }else{
    return [];
  }
}

exports.getFlowFigures = function(application){
  console.log(JSON.stringify(application));
  var flowchart_figures = [];
  if(application){
    if(application.app_claims && application.app_claims.length > 0){
      console.log("has claims");
      for(var c in application.app_claims){
        console.log(c);
        if(application.app_claims[c].claim_data){
          console.log("has claim data");
          for(var n in application.app_claims[c].claim_data.flowchart_figures){
            var device = application.app_claims[c].claim_device.device_name ? application.app_claims[c].claim_device.device_name : ""; 
            var features = getFigureFeatures(application.app_claims[c]);
            function getFigureFeatures(claim){
              var flow_features_collection = [];
              if(claim.claim_features && claim.claim_features.length > 0){
                flow_features_collection = claim.claim_features;
              }
              if(claim.tchilden && claim.tchilden.length > 0){
                for(var t in claim.tchilden){
                  if(claim.tchilden[t].claim_data){
                    for(var i in claim.tchilden[t].claim_data.flowchart_figures){
                      if(application.app_claims[c].claim_data.flowchart_figures[n] == claim.tchilden[t].claim_data.flowchart_figures[i]){
                        var flow_features = getFigureFeatures(claim.tchilden[t])
                        for(var f in flow_features){
                          flow_features_collection.push(flow_features[f]);
                          //// prevent duplicate features from being added
                          // var unique = true;
                          // for(var c in flow_features_collection){
                          //   if(flow_features_collection[c] == flow_features[f]){
                          //     unique = false;
                          //     break;
                          //   }
                          // }
                          // if(unique){
                          //   flow_features_collection.push(flow_features[f]);
                          // }
                          // break;
                        }
                      }
                    }
                  }
                }
              }
              return flow_features_collection;
            }
            features = features.sort((a,b)=>{
              if(a.feature_data && b.feature_data){
                if(a.feature_data.flowchart_order > b.feature_data.flowchart_order){
                  return -1;
                }else if(a.feature_data.flowchart_order < b.feature_data.flowchart_order){
                  return 1;
                }
              }
              return 0;
            })
            flowchart_figures.push({ features, device });
          }
        }
      }
    }
  }
  return flowchart_figures
}
